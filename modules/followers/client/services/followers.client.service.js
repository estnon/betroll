'use strict';

//Followers service used to communicate Followers REST endpoints
angular.module('followers').factory('Followers', ['$resource',
	function($resource) {
		return $resource('api/followers/:followerId', { followerId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('followers').factory('followArrs', function () {
	return {
		FollowList: {
			userIds: []
		},
		isFollowed: {
			followed: ''
		}
	};
});
