'use strict';

// Followers controller
angular.module('followers').controller('FollowersController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Followers', 'notifyOps', 'currentUser', 'progressBar', 'followArrs',
	function($scope, $stateParams, $location, $http, Authentication, Followers, notifyOps, currentUser, progressBar, followArrs) {
		$scope.authentication = Authentication;

		$scope.currentUser = currentUser;
		$scope.followArrs = followArrs;
		// Create new Follower
		$scope.create = function(userid) {
			// Create new Follower object
			var follower = new Followers({
				followed: userid
			});
			progressBar.status = 1;
			// Redirect after save
			follower.$save(function(response) {
				if (userid === currentUser.user.id) {
					$scope.isFollowing = true;
				} else {
					followArrs.isFollowed[userid] = true;
				}
				notifyOps.create(undefined, userid, 'New follower', 'follow');
				progressBar.status = 0;
			}, function(errorResponse) {
				$scope.isFollowing = false;
				couponOps.pop('error', langs.lang_strings[137], langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

		$scope.unfollow = function(userid) {
			progressBar.status = 1;
			var followingCurrentPromise = $http.get('api/followers/unfollow/' + userid);
			followingCurrentPromise.success(function(result) {
				if (userid === currentUser.user.id) {
					$scope.isFollowing = false;
				} else {
					followArrs.isFollowed[userid] = false;
				}
				notifyOps.create(undefined, userid, 'Unfollowed', 'unfollow');
				progressBar.status = 0;
			});
			followingCurrentPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};



		// Find a list of Followers
		$scope.find = function() {
			$scope.followers = Followers.query();
		};

		// Find existing Follower
		$scope.findOne = function() {
			$scope.follower = Followers.get({
				followerId: $stateParams.followerId
			});
		};
	}
]);
