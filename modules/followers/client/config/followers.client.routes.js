'use strict';

//Setting up route
angular.module('followers').config(['$stateProvider',
	function($stateProvider) {
		// Followers state routing
		$stateProvider.
		state('followers', {
			abstract: true,
			url: '/followers',
			template: '<ui-view/>'
		}).
		state('followers.list', {
			url: '',
			templateUrl: 'modules/followers/views/list-followers.client.view.html'
		});
	}
]);
