'use strict';

module.exports = function(app) {
	var followers = require('../controllers/followers.server.controller');
	var followersPolicy = require('../policies/followers.server.policy');

	// Followers Routes
	app.route('/api/followers').post(followers.create);
	app.route('/api/followers/list/of/').post(followers.listOfFollowers);
	app.route('/api/followers/following/list/of/').post(followers.listOfFollowing);
	app.route('/api/followers/following/array/of/:userid').get(followers.arrayOfFollowing);
	app.route('/api/followers/of/:userid').get(followers.isFollowing);
	app.route('/api/followers/following/us/:userid').get(followers.isFollowingUs);
	app.route('/api/followers/unfollow/:userid').get(followers.unfollow);
	app.route('/api/followers/followers/count/:userid').get(followers.countFollowers); //count of ones followers
	app.route('/api/followers/following/count/:userid').get(followers.countFollowing); //count of ones following
	app.route('/api/followers/isfollowed/:userid').get(followers.isFollowed);
	app.route('/api/followers/timeline/').post(followers.timeline);
};
