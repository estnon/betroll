'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	Follower = mongoose.model('Follower'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a Follower
 */
exports.create = function(req, res) {
	var follower = new Follower(req.body);
	follower.user = req.user;

	follower.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(follower);
		}
	});
};

/**
 * Show the current Follower
 */
exports.read = function(req, res) {
	res.jsonp(req.follower);
};

/**
 * List of Followers
 */
exports.listOfFollowers = function(req, res) {
	Follower.find({
		followed: req.body.userid,
		created: {
			$lt: new Date(req.body.lastseen)
		}
	}).sort('-created').limit(20).populate('user', '_id displayName username profileImageURL tagLine created timezone').exec(function(err, followers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(followers);
		}
	});
};

exports.listOfFollowing = function(req, res) {
	Follower.find({
		user: req.body.userid,
		created: {
			$lt: new Date(req.body.lastseen)
		}
	}).sort('-created').limit(20).populate('followed', '_id displayName username profileImageURL tagLine created timezone').exec(function(err, followers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(followers);
		}
	});
};

exports.arrayOfFollowing = function(req, res) {
	Follower.find({
		user: req.params.userid
	}, {
		_id: 0,
		followed: 1
	}).sort('-created').exec(function(err, followers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.jsonp(followers);
		}
	});
};

/**
 * Follower middleware
 */
exports.followerByID = function(req, res, next, id) {
	Follower.findById(id).populate('user', 'displayName').exec(function(err, follower) {
		if (err) return next(err);
		if (!follower) return next(new Error('Failed to load Follower ' + id));
		req.follower = follower;
		next();
	});
};

exports.isFollowing = function(req, res) {
	Follower.count({
		user: req.user._id,
		followed: req.params.userid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (count > 0) res.jsonp(true);
			else res.jsonp(false);
		}
	});
};

exports.isFollowingUs = function(req, res) {
	Follower.count({
		user: req.params.userid,
		followed: req.user._id
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (count > 0) res.jsonp(true);
			else res.jsonp(false);
		}
	});
};


exports.unfollow = function(req, res) {

	Follower.findOne({
		user: req.user._id,
		followed: req.params.userid
	}).exec(function(err, follower) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			follower.remove(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.jsonp(true);
				}
			});
		}
	});

};


exports.countFollowers = function(req, res) {
	Follower.count({
		followed: req.params.userid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(count);
		}
	});
};

exports.countFollowing = function(req, res) {
	Follower.count({
		user: req.params.userid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(count);
		}
	});
};

exports.isFollowed = function(req, res) {
	Follower.count({
		user: req.user._id,
		followed: req.params.userid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (count > 0) res.jsonp(true);
			else res.jsonp(false);
		}
	});
};


exports.timeline = function(req, res) {

	Follower.find({
		followed: {
			$in: req.body.userIds
		},
		user: req.user._id
	}, 'followed').sort('-created').exec(function(err, posts) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(posts);
		}
	});
};
