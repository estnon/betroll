'use strict';

(function() {
	// Followers Controller Spec
	describe('Followers Controller Tests', function() {
		// Initialize global variables
		var FollowersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Followers controller.
			FollowersController = $controller('FollowersController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Follower object fetched from XHR', inject(function(Followers) {
			// Create sample Follower using the Followers service
			var sampleFollower = new Followers({
				name: 'New Follower'
			});

			// Create a sample Followers array that includes the new Follower
			var sampleFollowers = [sampleFollower];

			// Set GET response
			$httpBackend.expectGET('api/followers').respond(sampleFollowers);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.followers).toEqualData(sampleFollowers);
		}));

		it('$scope.findOne() should create an array with one Follower object fetched from XHR using a followerId URL parameter', inject(function(Followers) {
			// Define a sample Follower object
			var sampleFollower = new Followers({
				name: 'New Follower'
			});

			// Set the URL parameter
			$stateParams.followerId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/api\/followers\/([0-9a-fA-F]{24})$/).respond(sampleFollower);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.follower).toEqualData(sampleFollower);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Followers) {
			// Create a sample Follower object
			var sampleFollowerPostData = new Followers({
				name: 'New Follower'
			});

			// Create a sample Follower response
			var sampleFollowerResponse = new Followers({
				_id: '525cf20451979dea2c000001',
				name: 'New Follower'
			});

			// Fixture mock form input values
			scope.name = 'New Follower';

			// Set POST response
			$httpBackend.expectPOST('api/followers', sampleFollowerPostData).respond(sampleFollowerResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Follower was created
			expect($location.path()).toBe('/followers/' + sampleFollowerResponse._id);
		}));

		it('$scope.update() should update a valid Follower', inject(function(Followers) {
			// Define a sample Follower put data
			var sampleFollowerPutData = new Followers({
				_id: '525cf20451979dea2c000001',
				name: 'New Follower'
			});

			// Mock Follower in scope
			scope.follower = sampleFollowerPutData;

			// Set PUT response
			$httpBackend.expectPUT(/api\/followers\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/followers/' + sampleFollowerPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid followerId and remove the Follower from the scope', inject(function(Followers) {
			// Create new Follower object
			var sampleFollower = new Followers({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Followers array and include the Follower
			scope.followers = [sampleFollower];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/api\/followers\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleFollower);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.followers.length).toBe(0);
		}));
	});
}());