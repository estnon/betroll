'use strict';

var should = require('should'),
	request = require('supertest'),
	path = require('path'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Follower = mongoose.model('Follower'),
	express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, follower;

/**
 * Follower routes tests
 */
describe('Follower CRUD tests', function() {
	before(function(done) {
		// Get application
		app = express.init(mongoose);
		agent = request.agent(app);

		done();
	});

	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Follower
		user.save(function() {
			follower = {
				name: 'Follower Name'
			};

			done();
		});
	});

	it('should be able to save Follower instance if logged in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Follower
				agent.post('/api/followers')
					.send(follower)
					.expect(200)
					.end(function(followerSaveErr, followerSaveRes) {
						// Handle Follower save error
						if (followerSaveErr) done(followerSaveErr);

						// Get a list of Followers
						agent.get('/api/followers')
							.end(function(followersGetErr, followersGetRes) {
								// Handle Follower save error
								if (followersGetErr) done(followersGetErr);

								// Get Followers list
								var followers = followersGetRes.body;

								// Set assertions
								(followers[0].user._id).should.equal(userId);
								(followers[0].name).should.match('Follower Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Follower instance if not logged in', function(done) {
		agent.post('/api/followers')
			.send(follower)
			.expect(403)
			.end(function(followerSaveErr, followerSaveRes) {
				// Call the assertion callback
				done(followerSaveErr);
			});
	});

	it('should not be able to save Follower instance if no name is provided', function(done) {
		// Invalidate name field
		follower.name = '';

		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Follower
				agent.post('/api/followers')
					.send(follower)
					.expect(400)
					.end(function(followerSaveErr, followerSaveRes) {
						// Set message assertion
						(followerSaveRes.body.message).should.match('Please fill Follower name');
						
						// Handle Follower save error
						done(followerSaveErr);
					});
			});
	});

	it('should be able to update Follower instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Follower
				agent.post('/api/followers')
					.send(follower)
					.expect(200)
					.end(function(followerSaveErr, followerSaveRes) {
						// Handle Follower save error
						if (followerSaveErr) done(followerSaveErr);

						// Update Follower name
						follower.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Follower
						agent.put('/api/followers/' + followerSaveRes.body._id)
							.send(follower)
							.expect(200)
							.end(function(followerUpdateErr, followerUpdateRes) {
								// Handle Follower update error
								if (followerUpdateErr) done(followerUpdateErr);

								// Set assertions
								(followerUpdateRes.body._id).should.equal(followerSaveRes.body._id);
								(followerUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Followers if not signed in', function(done) {
		// Create new Follower model instance
		var followerObj = new Follower(follower);

		// Save the Follower
		followerObj.save(function() {
			// Request Followers
			request(app).get('/api/followers')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Follower if not signed in', function(done) {
		// Create new Follower model instance
		var followerObj = new Follower(follower);

		// Save the Follower
		followerObj.save(function() {
			request(app).get('/api/followers/' + followerObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', follower.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Follower instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Follower
				agent.post('/api/followers')
					.send(follower)
					.expect(200)
					.end(function(followerSaveErr, followerSaveRes) {
						// Handle Follower save error
						if (followerSaveErr) done(followerSaveErr);

						// Delete existing Follower
						agent.delete('/api/followers/' + followerSaveRes.body._id)
							.send(follower)
							.expect(200)
							.end(function(followerDeleteErr, followerDeleteRes) {
								// Handle Follower error error
								if (followerDeleteErr) done(followerDeleteErr);

								// Set assertions
								(followerDeleteRes.body._id).should.equal(followerSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Follower instance if not signed in', function(done) {
		// Set Follower user 
		follower.user = user;

		// Create new Follower model instance
		var followerObj = new Follower(follower);

		// Save the Follower
		followerObj.save(function() {
			// Try deleting Follower
			request(app).delete('/api/followers/' + followerObj._id)
			.expect(403)
			.end(function(followerDeleteErr, followerDeleteRes) {
				// Set message assertion
				(followerDeleteRes.body.message).should.match('User is not authorized');

				// Handle Follower error error
				done(followerDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Follower.remove().exec();
		done();
	});
});