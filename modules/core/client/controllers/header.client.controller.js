'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', 'Authentication', 'Menus', '$aside',
	function($scope, $state, Authentication, Menus, $aside) {
		// Expose view variables

		$scope.$state = $state;
		$scope.authentication = Authentication;

		// Get the topbar menu
		$scope.menu = Menus.getMenu('topbar');

		// Toggle the menu items
		$scope.isCollapsed = false;
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.asideState = {
			open: false
		};

		$scope.openAside = function(position, backdrop, partialURL) {
			$scope.asideState = {
				open: true,
				position: position
			};

			function postClose() {
				$scope.asideState.open = false;
			}

			$aside.open({
				templateUrl: partialURL,
				placement: position,
				size: 'lg',
				backdrop: backdrop,
				controller: function($scope, $modalInstance) {
					$scope.ok = function(e) {
						$modalInstance.close();
						e.stopPropagation();
					};
					$scope.cancel = function(e) {
						$modalInstance.dismiss();
						e.stopPropagation();
					};
				}
			}).result.then(postClose, postClose);
		};

	}
]);


