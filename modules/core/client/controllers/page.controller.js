'use strict';

angular.module('core').controller('PageController', ['$window', '$rootScope', '$scope', '$state', '$interval', 'live', 'livegames', 'langs', 'loadLangs', 'ticker',
    'tickerVars', 'progressBar', 'couponOps', 'dateOps',
    function($window, $rootScope, $scope, $state, $interval, live, livegames, langs, loadLangs, ticker, tickerVars, progressBar, couponOps, dateOps) {
        // Expose view variables
        $scope.$state = $state;
        $scope.live = live;
        $scope.langs = langs;
        $scope.tickerVars = tickerVars;
        $scope.progressBar = progressBar;
        $scope.langs = langs;
        $scope.loadLangs = loadLangs;
        $scope.couponOps = couponOps;
        $scope.dateOps = dateOps;

        $scope.jumptoTop = function(){
            $window.scrollTo(0, 0);
        };
        $rootScope.$on('$viewContentLoaded', function(){
            $window.scrollTo(0, 0);
            live.updated = Date.now();
        });

        ticker.check();
        livegames.check();

        $interval(function () {
            ticker.check();
            livegames.check();
        }, 20000);
    }
]);


