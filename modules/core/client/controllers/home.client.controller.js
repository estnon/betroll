'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$location',
	function($scope, Authentication, $location) {

		$scope.imageType='png';

		$scope.playReset = function () {
			if ($scope.imageType === 'png') { $scope.imageType='gif' } else { $scope.imageType='png'; }
		};
		// This provides Authentication context.
		$scope.authentication = Authentication;
		if ($scope.authentication.user) {
			$location.path('posts');
		}
	}
]);

