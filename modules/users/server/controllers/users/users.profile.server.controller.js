'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	fs = require('fs'),
	images = require('images'),
	path = require('path'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User');

/**
 * Update user details
 */
exports.update = function(req, res) {
	// Init Variables
	var user = req.user;

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		user.displayName = user.name;

		user.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};

/**
 * Update profile picture
 */
exports.changeProfilePicture = function(req, res) {
	var user = req.user;
	var message = null;

	if (user) {
		fs.writeFile('./modules/users/client/img/profile/uploads/' + req.files.file.name, req.files.file.buffer, function(uploadError) {
			if (uploadError) {
				return res.status(400).send({
					message: 'Error occurred while uploading profile picture'
				});
			} else {

				var tmp_path = './modules/users/client/img/profile/uploads/' + req.files.file.name,
					out_path = './modules/users/client/img/profile/uploads/res/' + req.files.file.name,
					photo;

				photo = images(tmp_path);
				photo.size(196)
					.save(out_path, {
						quality: 80
					});


				user.profileImageURL = 'modules/users/img/profile/uploads/res/' + req.files.file.name;

				user.save(function(saveError) {
					if (saveError) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(saveError)
						});
					} else {
						req.login(user, function(err) {
							if (err) {
								res.status(400).send(err);
							} else {
								res.json(user);
							}
						});
					}
				});
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};


exports.lastTick = function(req, res) {
	var user = req.user;
	var message = null;

	if (user) {
		user.lastTick = req.body.tick;

		user.save(function(saveError) {
			if (saveError) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(saveError)
				});
			} else {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};

/**
 * Send User
 */
exports.me = function(req, res) {
	res.json(req.user || null);
};

/*
 * read current user
 */

exports.read = function(req, res) {
	res.jsonp(req.user);
};

/*
 * user middleware
 * */
exports.userByUsername = function(req, res, next, username) {
	User.findOne({
		username: username
	}, '_id displayName username created profileImageURL tagLine').exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next(new Error('Failed to load User ' + username));
		req.user = user;
		next();
	});
};

exports.search = function(req, res) {
	var ls = new Date(req.body.lastseen);
	var peopleQuery;

	if (req.body.searchterm === 'newest users') {
		peopleQuery = {
			created: {
				$lt: ls
			}
		};
	} else {
		peopleQuery = {
			created: {
				$lt: ls
			},
			$or: [{
				username: req.body.searchterm
			}, {
				displayName: req.body.searchterm
			}, {
				$text: {
					$search: req.body.searchterm
				}
			}]
		};

	}


	User.find(peopleQuery, '_id displayName username profileImageURL tagLine created timezone').sort('-created').limit(50).exec(function(err, people) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(people);
		}
	});
};


exports.crawl = function(req, res) {

	var swig  = require('swig');
	var sTitle, sDescription, sUrl, sImage;
	sTitle = 'Betroll';
	sDescription = 'Betting Connection';
	sUrl= req.protocol + '://' + req.headers.host + req.originalUrl;
	sImage ='https://betroll.net/modules/core/img/brand/logo.png';
	if (req.originalUrl.indexOf('users/') > -1) {
		User.findOne({
			username: req.params.username
		}, '_id displayName username created profileImageURL tagLine').exec(function (err, user) {
			if (err)
			{
				return res.status(400).send(
					{
						message: errorHandler.getErrorMessage(err)
					});
			}
			else
			{
				sUrl= 'https://betroll.net/users/' + user.username;

				sImage = 'https://betroll.net/' + user.profileImageURL;

				sTitle = 'Betroll | ' + user.displayName + ' @' + user.username;

				sDescription = user.tagLine;
				res.send(swig.renderFile('modules/users/server/views/user.html', {
					title: sTitle,
					description: sDescription + ' Member Since:' + user.created,
					keywords: sTitle + ', ' + sDescription,
					url: sUrl,
					image: sImage
				}));
			}
		});
	}
};
