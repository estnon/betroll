'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../controllers/users.server.controller');

	// Setting up the users profile api
	app.route('/api/users/me').get(users.me);
	app.route('/api/users/crawl/:username').get(users.crawl);
	app.route('/api/users/:username').put(users.update);
	app.route('/api/users/:username').get(users.read);
	app.route('/api/users/search/').post(users.search);
	app.route('/api/users/accounts').delete(users.removeOAuthProvider);
	app.route('/api/users/password').post(users.changePassword);
	app.route('/api/users/tick/').post(users.lastTick);
	app.route('/api/users/picture').post(users.changeProfilePicture);

	// Finish by binding the user middleware
	app.param('userId', users.userByID);
	app.param('username', users.userByUsername);
};
