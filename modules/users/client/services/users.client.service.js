'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('api/users/:username', {
			username: '@username'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('users').factory('currentUser', function() {
	return {
		user: {
			id: ''
		}
	};
});


angular.module('users').factory('ourUser', function() {
	return {
		ourUser: ''
	};
});

angular.module('users').directive('person', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/users/views/person.html'
	};
});

