'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [

	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);

angular.module('users').factory('dateOps', ['Authentication', 'langs', function (authentication, langs) {

	return {
		convertDate: function (date) {
			var newMoment = moment.tz(date, 'Europe/Istanbul');
			if (authentication.user) {
				return moment(newMoment).locale(authentication.user.language).tz(authentication.user.timezone).format('DD MMM YYYY HH:mm');
			} else {
				return newMoment.locale(langs.current_lang).format('DD MMM YYYY HH:mm');
			}
		},
		convertDateRelative: function (date) {

			var newMoment = moment.tz(date, 'Europe/Istanbul');
			if (authentication.user) {
				return moment(newMoment).locale(authentication.user.language).tz(authentication.user.timezone).fromNow();
			} else {
				return newMoment.locale(langs.current_lang).fromNow();
			}
		}
	};
}]);


angular.module('posts').factory('couponOps', ['toaster', 'langs', '$modal', '$location', function (toaster, langs, $modal, $location){

	return {
		openThread : function (postId) {
			$location.path('posts/thread/' + postId);
		},
		postText : undefined,
		GlobalCoupon : {
			coupon: [],
			totalOdds: 1
		},
		GlobalQuotes : {
			quotes: []
		},
		ReplyingTo: {
			postIds: [],
			userIds: [],
			usernames: []
		},
		replyTo: function (postid, postids, username, userid, usernames, userids) {
			this.unReplyTo();
			var usernamesPrep = usernames.slice();
			usernamesPrep.push(username);
			var useridsPrep = userids.slice();
			useridsPrep.push(userid);

			this.ReplyingTo.usernames = usernamesPrep.filter(function(elem, pos) {
				return usernamesPrep.indexOf(elem) === pos;
			});
			this.ReplyingTo.userIds = useridsPrep.filter(function(elem, pos) {
				return useridsPrep.indexOf(elem) === pos;
			});
			if (typeof postids !== 'undefined') {
				this.ReplyingTo.postIds = postids.slice();
				this.ReplyingTo.postIds.push(postid);
			}
			this.openModal('modules/posts/views/create-post-xs.client.view.html');


		},
		openModal : function (templateUrl, size) {

		var modalInstance = $modal.open({
				animation: true,
				controller: function($scope, $modalInstance, couponOps){
					{$scope.ok = function () {
						//couponOps.unReplyTo();
						$modalInstance.close();
					};}
				},
				templateUrl: templateUrl,
				size: size
			});
		},
		unReplyTo: function () {
			this.ReplyingTo.userIds = [];
			this.ReplyingTo.usernames = [];
			this.ReplyingTo.postIds = [];
		},
		addFormField : function (gameID, dateTime, teamNames, betType, betTypeShort, odd) {

			for (var i = 0; i < this.GlobalCoupon.coupon.length; i++) {
				if (this.GlobalCoupon.coupon[i].gameID === gameID) {
					this.pop('warning', this.GlobalCoupon.coupon[i].teamNames, langs.lang_strings[175]);
					return;
				}
			}

			this.GlobalCoupon.coupon.push({
				'gameID': gameID,
				'dateTime': dateTime,
				'teamNames': teamNames,
				'betType': betType,
				'betTypeShort': betTypeShort,
				'odd': odd
			});

			this.GlobalCoupon.totalOdds = 1;
			for (var y = 0; y < this.GlobalCoupon.coupon.length; y++) {
				this.GlobalCoupon.totalOdds = this.GlobalCoupon.totalOdds * this.GlobalCoupon.coupon[y].odd;
			}
			this.pop('success', teamNames, langs.lang_strings[176]);
		}, // addFormField end

		removeFormField : function(sira ) {
			this.GlobalCoupon.totalOdds = this.GlobalCoupon.totalOdds / this.GlobalCoupon.coupon[sira].odd ;
			this.GlobalCoupon.coupon.splice(sira,1);
		},

		removeQuoteField : function(sira) {
			this.GlobalQuotes.quotes.splice(sira,1);
		},

		addQuoteField : function (gameID, countryLeague, dateTime, teamNames, odd1, odd2, odd3) {

			for (var z = 0; z < this.GlobalQuotes.quotes.length; z++) {
				if (this.GlobalQuotes.quotes[z].gameID === gameID) {
					this.pop('warning', this.GlobalQuotes.quotes[z].teamNames, langs.lang_strings[173]);
					return;
				}
			}

			this.GlobalQuotes.quotes.push({
				'gameID': gameID,
				'countryLeague': countryLeague,
				'dateTime': dateTime,
				'teamNames': teamNames,
				'odd1': odd1,
				'odd2': odd2,
				'odd3': odd3
			});
			this.pop('success', teamNames, langs.lang_strings[174]);
		},	// addQuoteField end
		pop : function(toasttype, toasttitle, toasttext){
			toaster.pop(toasttype, toasttitle, toasttext);
		}

	};

}]);



angular.module('users').factory('ticker', ['$http', 'tickerVars', 'Authentication', 'langs', function($http, tickerVars, authentication, langs) {
	return {
		check: function () {
			if (authentication.user) {
				var postsTime = moment.tz(tickerVars.Timeline.time || authentication.user.lastTick, authentication.user.timezone);
				var listsTime = moment.tz(tickerVars.Shared.time || authentication.user.lastTick, authentication.user.timezone);
				var notificationsTime = moment.tz(tickerVars.Notifications.time || authentication.user.lastTick, authentication.user.timezone);
				var tickTime = moment.tz(Date.now(), authentication.user.timezone);

				var postsPromise = $http.post('api/posts/new/', {lastseen: postsTime});
				postsPromise.success(function (data) {
					tickerVars.Timeline.latest = data;
				});

				var listsPromise = $http.post('api/lists/new/', {lastseen: listsTime});
				listsPromise.success(function (data) {
					tickerVars.Shared.latest = data;
				});

				//console.log(notificationsTime);
				var notificationsPromise = $http.post('api/notifications/new/', {lastseen: notificationsTime});
				notificationsPromise.success(function (data) {
					tickerVars.Notifications.latest = data;
					if (typeof Notification !== 'undefined' && tickerVars.Notifications.latest > 0) {
						if (Notification.permission === 'granted') {

							var notificationsContentPromise = $http.post('api/notifications/newcontent/', {lastseen: notificationsTime});
							notificationsContentPromise.success(function (dataContent) {

								for (var index = 0; index < dataContent.length; ++index) {
									var notifTypes = { 'follow' : 74, 'share' : 75, 'mention' : 76, 'unfollow' : 77, 'fav' : 78};
									var notifTitle = dataContent[index].user.displayName + ' @' + dataContent[index].user.username  + ' ' + langs.lang_strings[notifTypes[dataContent[index].type]];
									var notification = new Notification(notifTitle, {
										icon: dataContent[index].user.profileImageURL,
										body: dataContent[index].post.postText.substring(0, 100) + '...'
									});
									notification.onclick = function () {
										window.open('notifications');
									};
								}


								tickerVars.Notifications.time=Date.now();
								tickerVars.Notifications.latest = '';
							});

						}
					}
				});

				var lastTickPromise = $http.post('api/users/tick/', {tick: tickTime});
				lastTickPromise.success(function (data) {
					// nothing
				});
			}
		}
	};
}]);

angular.module('users').factory('tickerVars', [function() {
	return {
		Timeline: {
			latest: '',
			time: ''
		},
		Shared: {
			latest: '',
			time: ''
		},
		Notifications: {
			latest: '',
			time: ''
		}
	};
}]);

angular.module('users').service('langs',  function () {
	return {
		lang_strings : {},
		current_lang : navigator.language.substring(0,2) || navigator.userLanguage.substring(0,2)
	};
});

angular.module('users').service('loadLangs', ['$http', 'langs', function ($http, langs) {
	return {
		load : function (lang){
			if (['en', 'de', 'ru', 'tr'].indexOf(langs.current_lang) === -1 ){ lang = 'en';}
			var langsPromise = $http.post('/api/auth/langs', {'dil' : lang || langs.current_lang});
			langsPromise.success(function (langdata) {
				langs.current_lang = lang;
				langs.lang_strings =  langdata;
			});
		}
	};
}]);


angular.module('users').directive('signupForm', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/users/views/authentication/signup.client.view.html'
	};
});
