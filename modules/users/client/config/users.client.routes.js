'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function ($stateProvider) {
		// Users state routing
		$stateProvider.
			state('settings', {
				abstract: true,
				url: '/settings',
				templateUrl: 'modules/users/views/settings/settings.client.view.html'
			}).
			state('settings.profile', {
				url: '/profile',
				templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
			}).
			state('settings.password', {
				url: '/password',
				templateUrl: 'modules/users/views/settings/change-password.client.view.html'
			}).
			state('settings.accounts', {
				url: '/accounts',
				templateUrl: 'modules/users/views/settings/manage-social-accounts.client.view.html'
			}).
			state('settings.picture', {
				url: '/picture',
				templateUrl: 'modules/users/views/settings/change-profile-picture.client.view.html'
			}).
			state('settings.desktopnotifs', {
				url: '/picture',
				templateUrl: 'modules/users/views/settings/enable-desktop-notifs.client.view.html'
			}).
			state('authentication', {
				abstract: true,
				url: '/authentication',
				templateUrl: 'modules/users/views/authentication/authentication.client.view.html'
			}).
			state('authentication.signup', {
				url: '/signup',
				templateUrl: 'modules/users/views/authentication/signup.client.view.html'
			}).
			state('authentication.signin', {
				url: '/signin',
				templateUrl: 'modules/users/views/authentication/signin.client.view.html'
			}).
			state('authentication.tos', {
				url: '/tos',
				templateUrl: 'modules/users/views/authentication/tos.client.view.html'
			}).
			state('authentication.privacy', {
				url: '/privacy',
				templateUrl: 'modules/users/views/authentication/privacy.client.view.html'
			}).
			state('password', {
				abstract: true,
				url: '/password',
				template: '<ui-view/>'
			}).
			state('password.forgot', {
				url: '/forgot',
				templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
			}).
			state('password.reset', {
				abstract: true,
				url: '/reset',
				template: '<ui-view/>'
			}).
			state('password.reset.invalid', {
				url: '/invalid',
				templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
			}).
			state('password.reset.success', {
				url: '/success',
				templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
			}).
			state('password.reset.form', {
				url: '/:token',
				templateUrl: 'modules/users/views/password/reset-password.client.view.html'
			}).
			state('users', {
				abstract: true,
				url: '/users',
				templateUrl: 'modules/users/views/view-profile.client.view.html'
			}).
			state('users.user', {
				url: '/:username',
				templateUrl: 'modules/users/views/view-profile.posts.client.view.html'
			}).
			state('users.favs', {
				url: '/:username/favs',
				templateUrl: 'modules/users/views/view-profile-favs.client.view.html'
			}).
			state('users.shares', {
				url: '/:username/shares',
				templateUrl: 'modules/users/views/view-profile-shares.client.view.html'
			}).
			state('users.following', {
				url: '/:username/following',
				templateUrl: 'modules/users/views/view-profile-following.client.view.html'
			}).
			state('users.followers', {
				url: '/:username/followers',
				templateUrl: 'modules/users/views/view-profile-followers.client.view.html'
			}).
			state('people', {
				url: '/people',
				templateUrl: 'modules/users/views/people.client.view.html'
			});
	}
]);
