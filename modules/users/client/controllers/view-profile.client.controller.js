/**
 * Created by ugur on 20.02.2015.
 */
'use strict';

angular.module('users').controller('ViewProfileController', ['$scope', '$state', '$http', '$location', 'Users', 'Authentication', '$stateParams',  'favArrs', 'listArrs', 'followArrs', 'currentUser', 'ourUser', 'progressBar', 'langs',
    function($scope, $state, $http, $location, Users, Authentication, $stateParams, favArrs, listArrs, followArrs, currentUser, ourUser, progressBar, langs) {
        $scope.user = Authentication.user;

        $scope.currentUser = currentUser;
        $scope.ourUser = ourUser;
        $scope.langs = langs;

        $scope.findPosts = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    if (typeof $scope.userPosts !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userPosts[$scope.userPosts.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }
                    //posts of user

                    var postData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    var userPostsPromise = $http.post('/api/posts/of/', postData);
                    userPostsPromise.success(function(data) {
                        if (typeof $scope.userPosts !== 'undefined' && loadmore === true) {
                            $scope.userPosts = $scope.userPosts.concat(data);
                        } else {
                            $scope.userPosts = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };


                        favArrs.FavList.user = $scope.user._id;
                        favArrs.FavList.postIds = [];
                        favArrs.FavList.postIds = $scope.userPosts.map(function(e) {
                            var key;
                            for (key in e) // push postids
                                return e[key];
                        });

                        $scope.checkIfFaved();

                        listArrs.ListList.user = $scope.user._id;
                        listArrs.ListList.postIds = [];
                        listArrs.ListList.postIds = favArrs.FavList.postIds;

                        $scope.checkIfShared();
                        progressBar.status = 0;

                    });
                    userPostsPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });
                });

        };

        $scope.findFavs = function(loadmore) {

            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    progressBar.status = 1;
                    if (typeof $scope.userFavs !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userFavs[$scope.userFavs.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var favData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    //favs of user
                    var userFavsPromise = $http.post('api/favs/list/of/', favData);
                    userFavsPromise.success(function(data) {
                        if (typeof $scope.userFavs !== 'undefined' && loadmore === true) {
                            $scope.userFavs = $scope.userFavs.concat(data);
                        } else {
                            $scope.userFavs = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };

                        favArrs.FavList.user = $scope.user._id;
                        favArrs.FavList.postIds = [];
                        favArrs.FavList.postIds = $scope.userFavs.map(function(e) {
                            var key;
                            for (key in e.post) // push postids
                                return e.post[key];
                        });

                        $scope.checkIfFaved();


                        listArrs.ListList.user = $scope.user._id;
                        listArrs.ListList.postIds = [];
                        listArrs.ListList.postIds = favArrs.FavList.postIds;


                        $scope.checkIfShared();
                        progressBar.status = 0;

                    });
                    userFavsPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                    });

                });
        };

        $scope.findShares = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;
            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {


                    if (typeof $scope.userShares !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userShares[$scope.userShares.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var shareData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    //shares of user
                    var userSharesPromise = $http.post('api/lists/list/of/', shareData);
                    userSharesPromise.success(function(data) {
                        if (typeof $scope.userShares !== 'undefined' && loadmore === true) {
                            $scope.userShares = $scope.userShares.concat(data);
                        } else {
                            $scope.userShares = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };

                        favArrs.FavList.user = $scope.user._id;
                        favArrs.FavList.postIds = [];
                        favArrs.FavList.postIds = $scope.userShares.map(function(e) {
                            var key;
                            for (key in e.post) // push postids
                                return e.post[key];
                        });

                        $scope.checkIfFaved();


                        listArrs.ListList.user = $scope.user._id;
                        listArrs.ListList.postIds = [];
                        listArrs.ListList.postIds = favArrs.FavList.postIds;


                        $scope.checkIfShared();
                        progressBar.status = 0;
                    });
                    userSharesPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });

                });
        };


        $scope.findFollowing = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    if (typeof $scope.userFollowing !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userFollowing[$scope.userFollowing.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var followingData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };


                    //followings of user
                    var followingsPromise = $http.post('api/followers/following/list/of/', followingData);
                    followingsPromise.success(function(data) {
                        if (typeof $scope.userFollowing !== 'undefined' && loadmore === true) {
                            $scope.userFollowing = $scope.userFollowing.concat(data);
                        } else {
                            $scope.userFollowing = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };


                        followArrs.FollowList.userIds = [];
                        followArrs.FollowList.userIds = $scope.userFollowing.map(function(e) {
                            var key;
                            for (key in e) // push postids
                                return e.followed._id;
                        });
                        $scope.checkIfFollowed();
                        progressBar.status = 0;
                    });
                    followingsPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });

                });

        };

        $scope.findFollowers = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    if (typeof $scope.userFollowers !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userFollowers[$scope.userFollowers.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var followersData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    //followers of user

                    var followersPromise = $http.post('api/followers/list/of/', followersData);
                    followersPromise.success(function(data) {
                        if (typeof $scope.userFollowers !== 'undefined' && loadmore === true) {
                            $scope.userFollowers = $scope.userFollowers.concat(data);
                        } else {
                            $scope.userFollowers = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };
                        followArrs.FollowList.userIds = [];
                        followArrs.FollowList.userIds = $scope.userFollowers.map(function(e) {
                            var key;
                            for (key in e) // push postids
                                return e.user._id;
                        });
                        $scope.checkIfFollowed();

                        progressBar.status = 0;
                    });
                    followersPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[148] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });

                });
        };

        $scope.findUser = function() {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    $scope.ourUser = ourUser;

                    if ($scope.user) {
                        //is followed by user
                        var followingCurrentPromise = $http.get('api/followers/of/' + ourUser._id);
                        followingCurrentPromise.success(function(result) {
                            $scope.isFollowing = result;
                        });
                        followingCurrentPromise.error(function() {
                            couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                        });

                        //is following our user
                        var followingUsPromise = $http.get('api/followers/following/us/' + ourUser._id);
                        followingUsPromise.success(function(result) {
                            $scope.isFollowingUs = result;
                        });
                        followingUsPromise.error(function() {
                            couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                        });
                    }

                    //post count
                    var postCountCurrentPromise = $http.get('/api/posts/count/' + ourUser._id);
                    postCountCurrentPromise.success(function(result) {
                        $scope.postCount = result;
                    });
                    postCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //fav count
                    var favCountCurrentPromise = $http.get('/api/favs/count/' + ourUser._id);
                    favCountCurrentPromise.success(function(result) {
                        $scope.favCount = result;
                    });
                    favCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //share count
                    var shareCountCurrentPromise = $http.get('/api/lists/count/' + ourUser._id);
                    shareCountCurrentPromise.success(function(result) {
                        $scope.shareCount = result;
                    });
                    shareCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //follower count
                    var followersCountCurrentPromise = $http.get('/api/followers/followers/count/' + ourUser._id);
                    followersCountCurrentPromise.success(function(result) {
                        $scope.followersCount = result;
                    });
                    followersCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //following count
                    var followingCountCurrentPromise = $http.get('/api/followers/following/count/' + ourUser._id);
                    followingCountCurrentPromise.success(function(result) {
                        $scope.followingCount = result;
                    });
                    followingCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });
                    progressBar.status = 0;
                });


        };


        $scope.checkIfFaved = function() {
            var favListPromise = $http.post('/api/favs/timeline', favArrs.FavList);
            favListPromise.success(function(favListData) {
                var tempList = [];
                for (var i = 0; i < favListData.length; i++) {
                    tempList[favListData[i].post] = true;
                }
                favArrs.isFaved = tempList;

            });
            favListPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[142] + ' ' + langs.lang_strings[136]);
            });

        };

        $scope.checkIfShared = function() {
            var listListPromise = $http.post('/api/lists/timeline', listArrs.ListList);
            listListPromise.success(function(listListData) {
                var tempList = [];
                for (var i = 0; i < listListData.length; i++) {
                    tempList[listListData[i].post] = true;
                }
                listArrs.isShared = tempList;

            });
            listListPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[143] + ' ' + langs.lang_strings[136]);
            });

        };

        $scope.checkIfFollowed = function() {
            var followsPromise = $http.post('/api/followers/timeline/', followArrs.FollowList);
            followsPromise.success(function(listFollowData) {
                var tempList = [];
                for (var i = 0; i < listFollowData.length; i++) {
                    tempList[listFollowData[i].followed] = true;
                }
                followArrs.isFollowed = tempList;

            });
            followsPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[148] + ' ' + langs.lang_strings[136]);
            });

        };

    }
]);
