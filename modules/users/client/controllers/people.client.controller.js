/**
 * Created by ugur on 20.02.2015.
 */
'use strict';

angular.module('users').controller('PeopleController', ['$scope', '$state', '$http', '$location', 'Users', 'Authentication', '$stateParams', 'dateOps', 'couponOps', 'progressBar', 'followArrs', 'langs',
    function($scope, $state, $http, $location, Users, Authentication, $stateParams, dateOps, couponOps, progressBar, followArrs, langs) {
        $scope.user = Authentication.user;
        $scope.dateOps = dateOps;
        $scope.searchTerm = 'newest users';
        $scope.langs = langs;

        $scope.search = function(more) {
            if ($scope.searchTerm.length <3 ){
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[145] + ' ' + langs.lang_strings[146]);
                return;
            }
            progressBar.status = 1;
            if (more === true) {
                $scope.lastSeen = $scope.people[$scope.people.length - 1].created;
            } else {
                $scope.lastSeen = Date.now();
                $scope.people = '';
            }
            var peoplePromise = $http.post('/api/users/search/', {searchterm : $scope.searchTerm, lastseen: $scope.lastSeen});
            peoplePromise.success(function(result) {

                if (more === true) {
                    $scope.people = $scope.people.concat(result);
                } else {
                    $scope.people = result;
                }
                followArrs.FollowList.userIds = [];
                followArrs.FollowList.userIds = $scope.people.map(function(e) {
                    var key;

                    for (key in e) // push postids
                        return e[key];
                });

                $scope.checkIfFollowed();
                if (!result.length) { $scope.noResults = true;} else {$scope.noResults = false;}
                progressBar.status = 0;
            });
            peoplePromise.error(function() {
                progressBar.status = 0;
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
            });
        };
        $scope.checkIfFollowed = function() {
            var followsPromise = $http.post('/api/followers/timeline/', followArrs.FollowList);
            followsPromise.success(function(listFollowData) {
                var tempList = [];
                for (var i = 0; i < listFollowData.length; i++) {
                    tempList[listFollowData[i].followed] = true;
                }
                followArrs.isFollowed = tempList;

            });
            followsPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[147] + ' ' + langs.lang_strings[136]);
            });

        };

    }
]);
