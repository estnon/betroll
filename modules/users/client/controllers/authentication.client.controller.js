'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', 'ticker','tickerVars', 'progressBar', '$aside', 'langs', 'loadLangs', '$modalStack',
	function($scope, $http, $location, Authentication, ticker, tickerVars, progressBar, $aside, langs, loadLangs,  $modalStack) {
		$scope.authentication = Authentication;
		$scope.tickerVars = tickerVars;
		$scope.langs = langs;

		if ($scope.authentication.user) {
			loadLangs.load($scope.authentication.user.language);
		} else {
			loadLangs.load($scope.langs.current_lang || 'en');
		}


		$scope.openAside = function(position, backdrop, partialUrl) {
			$scope.asideState = {
				open: true,
				position: position
			};

			function postClose() {
				$scope.asideState.open = false;
			}

			$aside.open({
				templateUrl: partialUrl,
				placement: position,
				size: 'lg',
				backdrop: backdrop,
				controller: function($scope, $modalInstance) {
					$scope.ok = function(e) {
						$modalInstance.close();
						e.stopPropagation();
					};
					$scope.cancel = function(e) {
						$modalInstance.dismiss();
						e.stopPropagation();
					};
				}
			}).result.then(postClose, postClose);
		};





		$scope.tzdetect = {
			names: moment.tz.names(),
			matches: function(base){
				var results = [], now = Date.now(), makekey = function(id){
					return [0, 4, 8, -5*12, 4-5*12, 8-5*12, 4-2*12, 8-2*12].map(function(months){
						var m = moment(now + months*30*24*60*60*1000);
						if (id) m.tz(id);
						return m.format('DDHHmm');
					}).join(' ');
				}, lockey = makekey(base);
				$scope.tzdetect.names.forEach(function(id){
					if (makekey(id)===lockey) results.push(id);
				});
				return results;
			}
		};


		$scope.signup = function() {
			if (typeof $scope.credentials !== 'undefined') {
				progressBar.status = 1;
				$scope.credentials.timezone = $scope.tzdetect.matches()[0];
				$scope.credentials.language = langs.current_lang || 'en';
				$http.post('/api/auth/signup', $scope.credentials).success(function (response) {
					// If successful we assign the response to the global user model

					$scope.authentication.user = response;
					$modalStack.dismissAll();
					progressBar.status = 0;
					// And redirect to the index page

					$location.path('settings/profile');
				}).error(function (response) {
					$scope.error = response.message;

					progressBar.status = 0;
				});
			} else {
				$scope.error = 'Please fill out the sign up form.';
			}
		};

		$scope.signin = function() {
			progressBar.status = 1;
			$http.post('/api/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				loadLangs.load($scope.authentication.user.language);

				ticker.check();

				progressBar.status = 0;
				$modalStack.dismissAll();
				// And redirect to the index page
				$location.path('/posts');
			}).error(function(response) {
				$scope.error = response.message;

				progressBar.status = 0;
			});
		};



	}
]);
