'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'langs',
	function($scope, $http, $location, Users, Authentication, langs) {
		$scope.user = Authentication.user;
		$scope.langs = langs;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');
	}
]);
