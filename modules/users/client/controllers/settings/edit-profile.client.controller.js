'use strict';

angular.module('users').controller('EditProfileController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'progressBar', 'couponOps', 'loadLangs', 'langs',
	function($scope, $http, $location, Users, Authentication, progressBar, couponOps, loadLangs, langs) {
		$scope.user = Authentication.user;

		$scope.tzList = moment.tz.names();
		$scope.notifyMe= function() {
			if (!Notification) {
				couponOps.pop('error', '', langs.lang_strings[44]);
				return;
			}

			if (Notification.permission !== "granted")
				Notification.requestPermission();
			else {
				couponOps.pop('success', '', langs.lang_strings[181]);

			}

		};
		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
				progressBar.status = 1;
				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
					loadLangs.load(Authentication.user.language);
					couponOps.pop('success', '', langs.lang_strings[107]);
					progressBar.status = 0;
				}, function(response) {
					$scope.error = response.data.message;
					couponOps.pop('error', langs.lang_strings[135], $scope.error);
					progressBar.status = 0;
				});
			} else {
				$scope.submitted = true;
			}
		};
	}
]);
