'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'progressBar', 'langs',
	function($scope, $http, $location, Users, Authentication, progressBar, langs) {
		$scope.user = Authentication.user;

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;
			progressBar.status = 1;
			$http.post('/api/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.addAlert({
					type: 'success',
					msg: langs.lang_strings[179]
				});
				$scope.passwordDetails = null;
				progressBar.status = 0;
			}).error(function(response) {
				$scope.error = response.message;
				progressBar.status = 0;
			});
		};
	}
]);
