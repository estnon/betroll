'use strict';

//Setting up route
angular.module('lists').config(['$stateProvider',
	function($stateProvider) {
		// Lists state routing
		$stateProvider.
		state('lists', {
			abstract: true,
			url: '/shared',
			template: '<ui-view/>'
		}).
		state('lists.list', {
			url: '',
			templateUrl: 'modules/lists/views/list-lists.client.view.html'
		}).
		state('lists.create', {
			url: '/create',
			templateUrl: 'modules/lists/views/create-list.client.view.html'
		}).
		state('lists.view', {
			url: '/:listId',
			templateUrl: 'modules/lists/views/view-list.client.view.html'
		}).
		state('lists.edit', {
			url: '/:listId/edit',
			templateUrl: 'modules/lists/views/edit-list.client.view.html'
		});
	}
]);
