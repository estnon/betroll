'use strict';

// Configuring the Lists module
angular.module('lists').run(['Menus',
	function(Menus) {
		// Add the Lists dropdown item
		Menus.addMenuItem('topbar', {
			title: 23,
			state: 'lists.list',
			position: 3
		});
	}
]);
