'use strict';

// Lists controller
angular.module('lists').controller('ListsController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Lists',  'notifyOps', 'favArrs', 'listArrs', 'tickerVars', 'progressBar',
	function($scope, $http, $stateParams, $location, Authentication, Lists, notifyOps, favArrs, listArrs, tickerVars, progressBar) {
		$scope.authentication = Authentication;

		$scope.user = Authentication.user;

		$scope.listArrs = listArrs;

		$scope.hidden = [];

		$scope.addToLists = function (postid, userid) {
			progressBar.status = 1;
			//lists of user
			var userListsPromise = $http.get('api/lists/add/' + postid);
			userListsPromise.success(function(data) {
				listArrs.isShared[postid] = true;
				notifyOps.create(postid, userid, 'Shared', 'share');
				progressBar.status = 0;
			});
			userListsPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.removeFromLists = function (postid) {
			progressBar.status = 1;
			//lists of user
			var userListsPromise = $http.get('api/lists/remove/' + postid);
			userListsPromise.success(function(data) {
				listArrs.isShared[postid] = false;
				progressBar.status = 0;
			});
			userListsPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.hide = function(listid) {

			var list = new Lists ({
				_id: listid
			});
			progressBar.status = 1;
			var hidePromise = $http.post('api/lists/hide/', list);
			hidePromise.success(function(data) {
				$scope.hidden[listid]=true;
				progressBar.status = 0;
			});
			hidePromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				$scope.hidden[listid]= false;
				progressBar.status = 0;
			});

		};


		// Find a list of Lists
		$scope.find = function() {

			if (!$scope.user) {
				$location.path('/');
			} else {
				progressBar.status = 1;
				if (typeof $scope.posts !== 'undefined') {
					$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
				} else {
					$scope.lastSeen = Date.now();
				}

				var postdata = {
					lastseen: $scope.lastSeen
				};

				// get posts of followings
				var timelinePromise = $http.post('/api/lists/list/all', postdata);
				timelinePromise.success(function (timelinedata) {
					progressBar.status = 0;

					if (typeof $scope.posts !== 'undefined') {
						$scope.posts = $scope.posts.concat(timelinedata);

					} else {
						$scope.posts = timelinedata;
					}
					$scope.posts.$resolved = true;
					tickerVars.Shared.time=Date.now();
					tickerVars.Shared.latest = '';

					favArrs.FavList.user = $scope.user._id;
					favArrs.FavList.postIds = [];
					favArrs.FavList.postIds = $scope.posts.map(function (e) {
						var key;
						for (key in e) // push postids
							if (e.post) return e.post[key];
					});

					$scope.checkIfFaved();


					listArrs.ListList.user = $scope.user._id;
					listArrs.ListList.postIds = [];
					listArrs.ListList.postIds = favArrs.FavList.postIds;

					$scope.checkIfShared();


				});
				timelinePromise.error(function () {
					couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
					progressBar.status = 0;
				});
			}
		};

		// Find existing List
		$scope.findOne = function() {
			$scope.list = Lists.get({
				listId: $stateParams.listId
			});
		};

		$scope.checkIfFaved = function () {
			progressBar.status = 1;
			var favListPromise = $http.post('/api/favs/timeline', favArrs.FavList);
			favListPromise.success(function (favListData) {
				var tempList = [];
				for (var i = 0; i < favListData.length; i++) {
					tempList[favListData[i].post] = true;
				}
				favArrs.isFaved = tempList;
				progressBar.status = 0;
			});
			favListPromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[142] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.checkIfShared = function () {
			progressBar.status = 1;
			var listListPromise = $http.post('/api/lists/timeline', listArrs.ListList);
			listListPromise.success(function (listListData) {
				progressBar.status = 0;
				var tempList = [];
				for (var i = 0; i < listListData.length; i++) {
					tempList[listListData[i].post] = true;
				}
				listArrs.isShared = tempList;

			});
			listListPromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[143] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};
	}
]);
