'use strict';

//Lists service used to communicate Lists REST endpoints
angular.module('lists').factory('Lists', ['$resource',
	function($resource) {
		return $resource('api/lists/:listId', {
			listId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('lists').factory('listArrs', function() {
	return {
		ListList: {
			postIds: [],
			user: ''
		},
		isShared: {
			post: ''
		}
	};
});
