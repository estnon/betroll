'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	List = mongoose.model('List'),
	Follower = mongoose.model('Follower'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


/**
 * Create a List
 */
exports.create = function(req, res) {
	var list = new List(req.body);
	list.user = req.user;
	list.post = req.params.postid;

	list.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(list);
		}
	});
};

/**
 * Show the current List
 */
exports.read = function(req, res) {
	res.jsonp(req.list);
};


/**
 * Delete an List
 */
exports.delete = function(req, res) {
	List.remove({
		user: req.user._id,
		post: req.params.postid
	}).exec(function(err, list) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(true);
		}
	});

};

/**
 * Delete all Lists of a post when post is deleted
 */
exports.deleteall = function(req, res) {
	List.remove({
		post: req.params.postid
	}).exec(function(err, list) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(true);
		}
	});
};


/**
 * Delete an List
 */
exports.isShared = function(req, res) {
	List.count({
		user: req.user._id,
		post: req.params.postid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (count > 0) res.jsonp(true);
			else res.jsonp(false);
		}
	});
};

/**
 * Delete by list id
 */
exports.hide = function(req, res) {

	List.remove({
		_id: req.body._id
	}).exec(function(err, fav) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(true);
		}
	});

};

/**
 * List middleware
 */
exports.listByID = function(req, res, next, id) {
	List.findById(id).populate('user', 'displayName').exec(function(err, list) {
		if (err) return next(err);
		if (!list) return next(new Error('Failed to load post ' + id));
		req.list = list;
		next();
	});
};

exports.count = function(req, res) {
	List.count({
		user: req.params.userid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(count);
		}
	});
};


exports.countOfPost = function(req, res) {
	List.count({
		post: req.params.postid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(count);
		}
	});
};


exports.listOfShares = function(req, res) {
	List.find({
		user: req.body.userid,
		created: {
			$lt: new Date(req.body.lastseen)
		}
	}).deepPopulate('post post.user post.coupon.pickedGame post.quotedGames user').sort('-created').limit(12).exec(function(err, lists) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(lists);
		}
	});
};


exports.listListAll = function(req, res) {

	Follower.find({
		user: req.user._id
	}, {
		_id: 0,
		followed: 1
	}).sort('-created').exec(function (err, followers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var obIds = [];
			followers.forEach(function (element, index, array) {
				obIds.push(element.followed);
			});
			obIds.push(req.user._id);

			List.aggregate([{
				$match: {
					user: {
						$in: obIds
					},
					created: {
						$lt: new Date(req.body.lastseen)
					}
				}
			}, {
				$group: {
					_id: '$post',
					list: {
						$last: '$_id'
					}
				}
			}, {
				$sort: {
					created: -1
				}
			}, {
				$limit: 20
			}]).exec(function (err, lists) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					var qList = [];

					for (var x = 0; x < lists.length; x++) {
						qList.push(lists[x].list);
					}

					List.find({
						_id: {
							$in: qList
						}
					}).deepPopulate('post post.user post.coupon.pickedGame post.quotedGames user').sort({
						created: -1
					}).exec(function (err, finallist) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							res.jsonp(finallist);
						}
					});
				}
			});
		}

	});
};


exports.checkNew = function(req, res) {
if (req.user) {
	Follower.find({
		user: req.user._id
	}, {
		_id: 0,
		followed: 1
	}).sort('-created').exec(function (err, followers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var obIds = [];
			followers.forEach(function (element, index, array) {
				obIds.push(element.followed);
			});

			List.aggregate([{
				$match: {
					user: {
						$in: obIds
					},
					created: {
						$gt: new Date(req.body.lastseen)
					}
				}
			}, {
				$group: {
					_id: '$post',
					list: {
						$last: '$_id'
					}
				}
			}]).exec(function (err, lists) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.jsonp(lists.length);
				}
			});
		}

	});
}
};

/**
 * timeline
 */
exports.timeline = function(req, res) {
	List.find({
		post: {
			$in: req.body.postIds
		},
		user: req.body.user
	}, 'post').sort('-created').exec(function(err, posts) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(posts);
		}
	});
};
