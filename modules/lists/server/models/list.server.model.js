'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate');
/**
 * List Schema
 */
var ListSchema = new Schema({
	post: {
		type: Schema.ObjectId,
		ref: 'Post'
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});
ListSchema.plugin(deepPopulate, {
	populate: {
		'post.user': {
			select: '_id displayName username profileImageURL'
		},
		'user': {
			select: '_id displayName username profileImageURL'
		}
	},
	whitelist: [
		'post',
		'post.user',
		'post.coupon.pickedGame',
		'post.quotedGames',
		'user'
	]
});
mongoose.model('List', ListSchema);
