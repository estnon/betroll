'use strict';

module.exports = function(app) {
	var lists = require('../controllers/lists.server.controller');
	var listsPolicy = require('../policies/lists.server.policy');

	// Lists Routes
	app.route('/api/lists/add/:postid').get(lists.create);
	app.route('/api/lists/remove/:postid').get(lists.delete);
	app.route('/api/lists/hide').post(lists.hide);
	app.route('/api/lists/removeall/:postid').get(lists.deleteall);
	app.route('/api/lists/count/:userid').get(lists.count);
	app.route('/api/lists/count/of/post/:postid').get(lists.countOfPost);
	app.route('/api/lists/list/of/').post(lists.listOfShares);
	app.route('/api/lists/new/').post(lists.checkNew);
	app.route('/api/lists/list/all/').post(lists.listListAll);
	app.route('/api/lists/timeline/').post(lists.timeline);
	app.route('/api/lists/isshared/:postid').get(lists.isShared);
};
