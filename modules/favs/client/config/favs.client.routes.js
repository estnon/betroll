'use strict';

//Setting up route
angular.module('favs').config(['$stateProvider',
	function($stateProvider) {
		// Favs state routing
		$stateProvider.
		state('favs', {
			abstract: true,
			url: '/favs',
			template: '<ui-view/>'
		}).
		state('favs.list', {
			url: '',
			templateUrl: 'modules/favs/views/list-favs.client.view.html'
		}).
		state('favs.create', {
			url: '/create',
			templateUrl: 'modules/favs/views/create-fav.client.view.html'
		}).
		state('favs.view', {
			url: '/:favId',
			templateUrl: 'modules/favs/views/view-fav.client.view.html'
		}).
		state('favs.edit', {
			url: '/:favId/edit',
			templateUrl: 'modules/favs/views/edit-fav.client.view.html'
		});
	}
]);