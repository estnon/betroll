'use strict';

// Favs controller
angular.module('favs').controller('FavsController', ['$scope', '$http', '$interval', '$stateParams', '$location', 'Authentication', 'Favs',  'notifyOps', 'favArrs', 'progressBar', '$state',
	function($scope, $http, $interval, $stateParams, $location, Authentication, Favs, notifyOps, favArrs, progressBar, $state) {
		$scope.authentication = Authentication;

		$scope.favArrs = favArrs;
		$scope.hidden = [];

		$scope.addToFavs = function(postid, userid) {
			// Create new Notification object
			var fav = new Favs ({
				post: postid
			});
			progressBar.status = 1;
			// Redirect after save
			fav.$save(function(response) {
				favArrs.isFaved[postid] = true;
				notifyOps.create(postid, userid, 'Faved', 'fav');
				progressBar.status = 0;
			}, function(errorResponse) {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

		$scope.removeFromFavs = function(postid) {
			progressBar.status = 1;
			var fav = {	postid: postid };

			var userFavsPromise = $http.post('api/favs/remove/', fav);
			userFavsPromise.success(function(data) {
				favArrs.isFaved[postid] = false;
				progressBar.status = 0;
			});
			userFavsPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.hide = function(favid) {

			var fav = new Favs ({
				_id: favid
			});
			progressBar.status = 1;
			var hidePromise = $http.post('api/favs/hide/', fav);
			hidePromise.success(function(data) {
				progressBar.status = 0;
				$scope.hidden[favid]=true;
			});
			hidePromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
				$scope.hidden[favid]=false;
			});

		};

		$scope.featured = function() {
			var skip = 0;

			var featInterval = $interval(function hello() {
				if ($state.current.name !== 'home') $interval.cancel(featInterval);
				progressBar.status = 1;
				var userFavsPromise = $http.post('api/favs/list/featured', {skip: skip});
				userFavsPromise.success(function(data) {

					$scope.userFavs = data;
					skip = (skip + 3) % 20;
					progressBar.status = 0;

				});
				userFavsPromise.error(function() {
					couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
				});
				return hello;
			}(), 8000);


		};

	}
]);
