'use strict';

//Favs service used to communicate Favs REST endpoints
angular.module('favs').factory('Favs', ['$resource',
	function($resource) {
		return $resource('api/favs/:favId', { favId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('favs').factory('favArrs', function () {
	return {
		FavList: {
			postIds: [],
			user: ''
		},
		isFaved: {
			post: ''
		}
	};
});


angular.module('favs').directive('featuredPosts', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/favs/views/featuredPosts.html'
	};
});
