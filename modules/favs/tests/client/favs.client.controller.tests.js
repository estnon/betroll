'use strict';

(function() {
	// Favs Controller Spec
	describe('Favs Controller Tests', function() {
		// Initialize global variables
		var FavsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Favs controller.
			FavsController = $controller('FavsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Fav object fetched from XHR', inject(function(Favs) {
			// Create sample Fav using the Favs service
			var sampleFav = new Favs({
				name: 'New Fav'
			});

			// Create a sample Favs array that includes the new Fav
			var sampleFavs = [sampleFav];

			// Set GET response
			$httpBackend.expectGET('api/favs').respond(sampleFavs);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.favs).toEqualData(sampleFavs);
		}));

		it('$scope.findOne() should create an array with one Fav object fetched from XHR using a favId URL parameter', inject(function(Favs) {
			// Define a sample Fav object
			var sampleFav = new Favs({
				name: 'New Fav'
			});

			// Set the URL parameter
			$stateParams.favId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/api\/favs\/([0-9a-fA-F]{24})$/).respond(sampleFav);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.fav).toEqualData(sampleFav);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Favs) {
			// Create a sample Fav object
			var sampleFavPostData = new Favs({
				name: 'New Fav'
			});

			// Create a sample Fav response
			var sampleFavResponse = new Favs({
				_id: '525cf20451979dea2c000001',
				name: 'New Fav'
			});

			// Fixture mock form input values
			scope.name = 'New Fav';

			// Set POST response
			$httpBackend.expectPOST('api/favs', sampleFavPostData).respond(sampleFavResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Fav was created
			expect($location.path()).toBe('/favs/' + sampleFavResponse._id);
		}));

		it('$scope.update() should update a valid Fav', inject(function(Favs) {
			// Define a sample Fav put data
			var sampleFavPutData = new Favs({
				_id: '525cf20451979dea2c000001',
				name: 'New Fav'
			});

			// Mock Fav in scope
			scope.fav = sampleFavPutData;

			// Set PUT response
			$httpBackend.expectPUT(/api\/favs\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/favs/' + sampleFavPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid favId and remove the Fav from the scope', inject(function(Favs) {
			// Create new Fav object
			var sampleFav = new Favs({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Favs array and include the Fav
			scope.favs = [sampleFav];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/api\/favs\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleFav);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.favs.length).toBe(0);
		}));
	});
}());