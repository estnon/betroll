'use strict';

var should = require('should'),
	request = require('supertest'),
	path = require('path'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Fav = mongoose.model('Fav'),
	express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, fav;

/**
 * Fav routes tests
 */
describe('Fav CRUD tests', function() {
	before(function(done) {
		// Get application
		app = express.init(mongoose);
		agent = request.agent(app);

		done();
	});

	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Fav
		user.save(function() {
			fav = {
				name: 'Fav Name'
			};

			done();
		});
	});

	it('should be able to save Fav instance if logged in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fav
				agent.post('/api/favs')
					.send(fav)
					.expect(200)
					.end(function(favSaveErr, favSaveRes) {
						// Handle Fav save error
						if (favSaveErr) done(favSaveErr);

						// Get a list of Favs
						agent.get('/api/favs')
							.end(function(favsGetErr, favsGetRes) {
								// Handle Fav save error
								if (favsGetErr) done(favsGetErr);

								// Get Favs list
								var favs = favsGetRes.body;

								// Set assertions
								(favs[0].user._id).should.equal(userId);
								(favs[0].name).should.match('Fav Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Fav instance if not logged in', function(done) {
		agent.post('/api/favs')
			.send(fav)
			.expect(403)
			.end(function(favSaveErr, favSaveRes) {
				// Call the assertion callback
				done(favSaveErr);
			});
	});

	it('should not be able to save Fav instance if no name is provided', function(done) {
		// Invalidate name field
		fav.name = '';

		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fav
				agent.post('/api/favs')
					.send(fav)
					.expect(400)
					.end(function(favSaveErr, favSaveRes) {
						// Set message assertion
						(favSaveRes.body.message).should.match('Please fill Fav name');
						
						// Handle Fav save error
						done(favSaveErr);
					});
			});
	});

	it('should be able to update Fav instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fav
				agent.post('/api/favs')
					.send(fav)
					.expect(200)
					.end(function(favSaveErr, favSaveRes) {
						// Handle Fav save error
						if (favSaveErr) done(favSaveErr);

						// Update Fav name
						fav.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Fav
						agent.put('/api/favs/' + favSaveRes.body._id)
							.send(fav)
							.expect(200)
							.end(function(favUpdateErr, favUpdateRes) {
								// Handle Fav update error
								if (favUpdateErr) done(favUpdateErr);

								// Set assertions
								(favUpdateRes.body._id).should.equal(favSaveRes.body._id);
								(favUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Favs if not signed in', function(done) {
		// Create new Fav model instance
		var favObj = new Fav(fav);

		// Save the Fav
		favObj.save(function() {
			// Request Favs
			request(app).get('/api/favs')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Fav if not signed in', function(done) {
		// Create new Fav model instance
		var favObj = new Fav(fav);

		// Save the Fav
		favObj.save(function() {
			request(app).get('/api/favs/' + favObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', fav.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Fav instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Fav
				agent.post('/api/favs')
					.send(fav)
					.expect(200)
					.end(function(favSaveErr, favSaveRes) {
						// Handle Fav save error
						if (favSaveErr) done(favSaveErr);

						// Delete existing Fav
						agent.delete('/api/favs/' + favSaveRes.body._id)
							.send(fav)
							.expect(200)
							.end(function(favDeleteErr, favDeleteRes) {
								// Handle Fav error error
								if (favDeleteErr) done(favDeleteErr);

								// Set assertions
								(favDeleteRes.body._id).should.equal(favSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Fav instance if not signed in', function(done) {
		// Set Fav user 
		fav.user = user;

		// Create new Fav model instance
		var favObj = new Fav(fav);

		// Save the Fav
		favObj.save(function() {
			// Try deleting Fav
			request(app).delete('/api/favs/' + favObj._id)
			.expect(403)
			.end(function(favDeleteErr, favDeleteRes) {
				// Set message assertion
				(favDeleteRes.body.message).should.match('User is not authorized');

				// Handle Fav error error
				done(favDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Fav.remove().exec();
		done();
	});
});