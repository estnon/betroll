'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var deepPopulate = require('mongoose-deep-populate');

/**
 * Fav Schema
 */
var FavSchema = new Schema({
	post: {
		type: Schema.ObjectId,
		ref: 'Post'
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

FavSchema.plugin(deepPopulate, {
	populate: {
		'post.user': {
			select: '_id displayName username profileImageURL'
		}
	},
	whitelist: [
		'post',
		'post.user',
		'post.coupon.pickedGame',
		'post.quotedGames'
	]
});
mongoose.model('Fav', FavSchema);
