'use strict';

module.exports = function(app) {
	var favs = require('../controllers/favs.server.controller');
	var favsPolicy = require('../policies/favs.server.policy');

	// Favs Routes

	app.route('/api/favs/').post(favs.create);
	app.route('/api/favs/remove').post(favs.delete);
	app.route('/api/favs/hide').post(favs.hide);
	app.route('/api/favs/removeall/').post(favs.deleteall);
	app.route('/api/favs/count/:userid').get(favs.count);
	app.route('/api/favs/count/of/post/:postid').get(favs.countOfPost);
	app.route('/api/favs/list/of/').post(favs.listOfFavs);
	app.route('/api/favs/list/featured').post(favs.listOfFeatured);
	app.route('/api/favs/timeline/').post(favs.timeline);
	app.route('/api/favs/isfaved/:postid').get(favs.isFaved);

};
