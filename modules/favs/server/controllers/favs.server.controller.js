'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	Fav = mongoose.model('Fav'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));



/**
 * Create a Fav
 */
exports.create = function(req, res) {
	var fav = new Fav(req.body);
	fav.user = req.user;

	fav.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(fav);
		}
	});
};

/**
 * Show the current Fav
 */
exports.read = function(req, res) {
	res.jsonp(req.fav);
};


/**
 * Delete an Fav
 */
exports.delete = function(req, res) {

	Fav.remove({
		user: req.user._id,
		post: req.body.postid
	}).exec(function(err, fav) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(true);
		}
	});

};

/**
 * Delete by Fav id
 */
exports.hide = function(req, res) {

	Fav.remove({
		_id: req.body._id
	}).exec(function(err, fav) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(true);
		}
	});

};

/**
 * Delete all Favs of a post when post is deleted
 */
exports.deleteall = function(req, res) {
	Fav.remove({
		post: req.body.postid
	}).exec(function(err, fav) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(true);
		}
	});
};


/**
 * Delete an Fav
 */
exports.isFaved = function(req, res) {
	Fav.count({
		user: req.user._id,
		post: req.params.postid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (count > 0) res.jsonp(true);
			else res.jsonp(false);
		}
	});
};


/**
 * Fav middleware
 */
exports.favByID = function(req, res, next, id) {
	Fav.findById(id).populate('user', 'displayName').exec(function(err, fav) {
		if (err) return next(err);
		if (!fav) return next(new Error('Failed to load Fav ' + id));
		req.fav = fav;
		next();
	});
};

exports.count = function(req, res) {
	Fav.count({
		user: req.params.userid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(count);
		}
	});
};


exports.countOfPost = function(req, res) {
	Fav.count({
		post: req.params.postid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(count);
		}
	});
};



exports.listOfFavs = function(req, res) {
	Fav.find({
		user: req.body.userid,
		created: {
			$lt: new Date(req.body.lastseen)
		}
	}).deepPopulate('post post.user post.coupon.pickedGame post.quotedGames').sort('-created').limit(12).exec(function(err, favs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(favs);
		}
	});
};


exports.listOfFeatured = function(req, res) {
	Fav.find({
		user: '556ca409a042b25251e5f4bb'
	}).deepPopulate('post post.user post.coupon.pickedGame post.quotedGames').sort('-created').skip(req.body.skip).limit(3).exec(function(err, favs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(favs);
		}
	});
};

/**
 * timeline
 */
exports.timeline = function(req, res) {
	Fav.find({
		post: {
			$in: req.body.postIds
		},
		user: req.body.user
	}, 'post').sort('-created').exec(function(err, posts) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(posts);
		}
	});
};
