'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	Notification = mongoose.model('Notification'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a Notification
 */
exports.create = function(req, res) {
	var notification = new Notification(req.body);
	notification.user = req.user;

	notification.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(notification);
		}
	});
};

/**
 * Show the current Notification
 */
exports.read = function(req, res) {
	res.jsonp(req.notification);
};

/**
 * Update a Notification
 */
exports.update = function(req, res) {
	var notification = req.notification ;

	notification = _.extend(notification , req.body);

	notification.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(notification);
		}
	});
};

/**
 * Delete an Notification
 */
exports.delete = function(req, res) {
	var notification = req.notification ;

	notification.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(notification);
		}
	});
};

/**
 * List of Notifications
 */
exports.list = function(req, res) {
	if (req.user) {

		/*Notification.aggregate([
			{ "$match": {
				notifyUser: req.user._id }
			},
			// Group by the grouping key, but keep the valid values
			{ "$group": {
				"_id" : "$post",
				"notification" : { "$push" : {
					"_id" : "$_id",
					"user" : "$user",
					"notifyUser" : "$notifyUser",
					"post" : "$post",
					"created" : "$created",
					"type" : "$type",
					"text" : "$text"
				}
				}
			}},

			// Then sort
			{ "$sort": { "_id": -1 } }

		]).limit(30).populate('user', '_id displayName username profileImageURL timezone').deepPopulate('post post.user post.coupon.pickedGame post.quotedGames').exec(function (err, notifications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(notifications);
			}
		});*/

		Notification.find({
			notifyUser: req.user._id
		}).sort('-created').limit(30).populate('user', '_id displayName username profileImageURL timezone').deepPopulate('post post.user post.coupon.pickedGame post.quotedGames').exec(function (err, notifications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(notifications);
			}
		});
	}
};


exports.checkNew = function(req, res) {
	if (req.user) {
		Notification.count({
			notifyUser: req.user._id,
			created: {
				$gt: new Date(req.body.lastseen)
			}
		}).exec(function (err, notifications) {
			if (err) {
				console.log(err);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				//console.log(notifications);
				res.jsonp(notifications);
			}
		});
	}
};


exports.checkNewContent = function(req, res) {
	if (req.user) {
		Notification.find({
			notifyUser: req.user._id,
			created: {
				$gt: new Date(req.body.lastseen)
			}
		}).populate('user', '_id displayName username profileImageURL timezone').populate('post', '_id postText').exec(function (err, notifications) {
			if (err) {
				console.log(err);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				//console.log(notifications);
				res.jsonp(notifications);
			}
		});
	}
};

/**
 * Notification middleware
 */
exports.notificationByID = function(req, res, next, id) { Notification.findById(id).populate('user', 'displayName').exec(function(err, notification) {
		if (err) return next(err);
		if (! notification) return next(new Error('Failed to load Notification ' + id));
		req.notification = notification ;
		next();
	});
};
