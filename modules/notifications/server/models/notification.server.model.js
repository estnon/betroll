'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate');
/**
 * Notification Schema
 */
var NotificationSchema = new Schema({
	post: {
		type: Schema.ObjectId,
		ref: 'Post'
	},
	notifyUser: {
		type: Schema.ObjectId,
		ref: 'Post'
	},
	text: {
		type: String,
		default: '',
		trim: true
	},
	type: {
		type: String,
		default: '',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

NotificationSchema.plugin(deepPopulate, {
	populate: {
		'post.user': {
			select: '_id displayName username profileImageURL'
		}
	},
	whitelist: [
		'post',
		'post.user',
		'post.coupon.pickedGame',
		'post.quotedGames'
	]
});
mongoose.model('Notification', NotificationSchema);
