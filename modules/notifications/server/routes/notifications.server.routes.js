'use strict';

module.exports = function(app) {
	var notifications = require('../controllers/notifications.server.controller');
	var notificationsPolicy = require('../policies/notifications.server.policy');

	// Notifications Routes
	app.route('/api/notifications').all()
		.get(notifications.list).all(notificationsPolicy.isAllowed)
		.post(notifications.create);
	app.route('/api/notifications/new/').post(notifications.checkNew);
	app.route('/api/notifications/newcontent/').post(notifications.checkNewContent);
	app.route('/api/notifications/:notificationId').all(notificationsPolicy.isAllowed)
		.get(notifications.read)
		.put(notifications.update);

	// Finish by binding the Notification middleware
	app.param('notificationId', notifications.notificationByID);
};
