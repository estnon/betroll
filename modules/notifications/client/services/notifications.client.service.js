'use strict';

//Notifications service used to communicate Notifications REST endpoints
angular.module('notifications').factory('Notifications', ['$resource',
	function($resource) {
		return $resource('api/notifications/:notificationId', { notificationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('notifications').factory('notifyOps', ['Notifications', function (Notifications) {

	return {
		create:  function(postid, notifyUserId, notifText, notifType) {
		// Create new Notification object
		var notification = new Notifications ({
			post: postid,
			notifyUser : notifyUserId,
			text : notifText,
			type: notifType
		});

		// Redirect after save
		notification.$save(function(response) {
			//do nothing
		});
	}


	};
}]);
