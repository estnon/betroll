'use strict';

//Setting up route
angular.module('notifications').config(['$stateProvider',
	function($stateProvider) {
		// Notifications state routing
		$stateProvider.
		state('notifications', {
			abstract: true,
			url: '/notifications',
			template: '<ui-view/>'
		}).
		state('notifications.list', {
			url: '',
			templateUrl: 'modules/notifications/views/list-notifications.client.view.html'
		}).
		state('notifications.create', {
			url: '/create',
			templateUrl: 'modules/notifications/views/create-notification.client.view.html'
		});
	}
]);
