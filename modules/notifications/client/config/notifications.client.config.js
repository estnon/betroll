'use strict';

// Configuring the Notifications module
angular.module('notifications').run(['Menus',
	function(Menus) {
		// Add the Notifications dropdown item
		Menus.addMenuItem('topbar', {
			title: 24,
			state: 'notifications.list',
			position: 4
		});
	}
]);
