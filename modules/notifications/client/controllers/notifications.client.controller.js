'use strict';

// Notifications controller
angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications', 'tickerVars', 'progressBar', 'langs',
	function($scope, $stateParams, $location, Authentication, Notifications,  tickerVars, progressBar, langs) {
		$scope.authentication = Authentication;

		$scope.notifFilter = undefined;
		$scope.langs = langs;
		$scope.notifTypes = { 'follow' : 74, 'share' : 75, 'mention' : 76, 'unfollow' : 77, 'fav' : 78};
		$scope.setFilter = function(filter) {
			$scope.notifFilter = { 'type' : filter };
			switch(filter) {
				case 'mention':
					$scope.notifFilterName = langs.lang_strings[70];
					break;
				case 'follow':
					$scope.notifFilterName = langs.lang_strings[71];
					break;
				case 'share':
					$scope.notifFilterName = langs.lang_strings[72];
					break;
				case 'fav':
					$scope.notifFilterName = langs.lang_strings[73];
					break;
				default:
					$scope.notifFilterName = langs.lang_strings[69];
			}

		};
		// Find a list of Notifications
		$scope.find = function() {

			if (!$scope.authentication.user) {
				$location.path('/');
			} else {
				progressBar.status = 1;
				$scope.notifications = Notifications.query(function success() {
					progressBar.status = 0;
				}, function error() {
					progressBar.status = 0;
				});
				tickerVars.Notifications.time=Date.now();
				tickerVars.Notifications.latest = '';
				$scope.setFilter();
			}
		};

		// Find existing Notification
		$scope.findOne = function() {
			$scope.notification = Notifications.get({ 
				notificationId: $stateParams.notificationId
			});
		};
	}
]);
