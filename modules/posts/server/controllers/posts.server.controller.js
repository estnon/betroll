'use strict';
/**
 * Module dependencies.
 */
var _ = require('lodash'),
	fs = require('fs'),
	images = require('images'),
	path = require('path'),
	mongoose = require('mongoose'),
	Post = mongoose.model('Post'),
	Follower = mongoose.model('Follower'),
	config = require(path.resolve('./config/env/default.js')),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
/**
 * Create a Post
 */
exports.create = function(req, res)
{
	var post = new Post(req.body);
	post.user = req.user;
	post.save(function(err)
	{
		if (err)
		{
			console.log(err);
			return res.status(400).send(
				{
					message: errorHandler.getErrorMessage(err)
				});
		}
		else
		{
			res.jsonp(post);
		}
	});
};
/**
 * Show the current Post
 */
exports.read = function(req, res)
{
	res.jsonp(req.post);
};
/**
 * Update a Post
 */
exports.update = function(req, res)
{
	var post = req.post;
	post = _.extend(post, req.body);
	post.save(function(err)
	{
		if (err)
		{
			return res.status(400).send(
				{
					message: errorHandler.getErrorMessage(err)
				});
		}
		else
		{
			res.jsonp(post);
		}
	});
};
/**
 * Delete an Post
 */
exports.delete = function(req, res)
{
	var post = req.post;
	post.remove(function(err)
	{
		if (err)
		{
			return res.status(400).send(
				{
					message: errorHandler.getErrorMessage(err)
				});
		}
		else
		{
			res.jsonp(post);
		}
	});
};
/**
 * List of Posts
 */
exports.list = function(req, res)
{
	Post.find().sort('-created').populate('user', '_id displayName username profileImageURL').populate('coupon.pickedGame').populate('quotedGames').exec(function(err, posts)
	{
		if (err)
		{
			return res.status(400).send(
				{
					message: errorHandler.getErrorMessage(err)
				});
		}
		else
		{
			res.jsonp(posts);
		}
	});
};
/**
 * timeline
 */
exports.timeline = function(req, res)
{
	if (req.user) {
		Follower.find(
			{
				user: req.user._id
			},
			{
				_id: 0,
				followed: 1
			}).sort('-created').exec(function (err, followers) {
				if (err) {
					return res.status(400).send(
						{
							message: errorHandler.getErrorMessage(err)
						});
				}
				else {
					var arr = [];
					var followingCount =0;
					followers.forEach(function (element, index, array) {
						arr.push(element.followed);
						followingCount = followingCount +1;
					});
					arr.push(req.user._id);
					var postQuery;
					if (followingCount === 0) {
						postQuery ={
							created: {
								$lt: new Date(req.body.lastseen)
							}
						};
					} else {
						postQuery= {
							user: {
								$in: arr
							},
							created: {
								$lt: new Date(req.body.lastseen)
							}
						};
					}
					Post.find(
						postQuery).sort('-created').limit(20).populate('user', '_id displayName username profileImageURL').populate('coupon.pickedGame').populate('quotedGames').exec(function (err, posts) {
							if (err) {
								return res.status(400).send(
									{
										message: errorHandler.getErrorMessage(err)
									});
							}
							else {
								res.jsonp(posts);
							}
						});
				}
			});
	}
};

/**
 * slips only
 */
exports.slips = function(req, res)
{
	if (req.user) {
		Follower.find(
			{
				user: req.user._id
			},
			{
				_id: 0,
				followed: 1
			}).sort('-created').exec(function (err, followers) {
				if (err) {
					return res.status(400).send(
						{
							message: errorHandler.getErrorMessage(err)
						});
				}
				else {
					var arr = [];
					var followingCount =0;
					followers.forEach(function (element, index, array) {
						arr.push(element.followed);
						followingCount = followingCount +1;
					});
					arr.push(req.user._id);
					var postQuery;
					if (followingCount === 0) {
						postQuery ={
							'coupon.0' : {$exists : true},
							created: {
								$lt: new Date(req.body.lastseen)
							}
						};
					} else {
						postQuery= {
							user: {
								$in: arr
							},
							'coupon.0' : {$exists : true},
							created: {
								$lt: new Date(req.body.lastseen)
							}
						};
					}
					Post.find(
						postQuery).sort('-created').limit(20).populate('user', '_id displayName username profileImageURL').populate('coupon.pickedGame').populate('quotedGames').exec(function (err, posts) {
							if (err) {
								return res.status(400).send(
									{
										message: errorHandler.getErrorMessage(err)
									});
							}
							else {
								res.jsonp(posts);
							}
						});
				}
			});
	}
};

/**
 * search posts
 */
exports.search = function(req, res)
{
	var searchQuery;
	var ls=new Date(req.body.lastseen);

	if (req.body.searchterm === 'latest posts') {
		searchQuery = {
			created: {
				$lt: ls
			}
		};
	} else {
		searchQuery = {
			$text: { $search: req.body.searchterm },
			created: {
				$lt: ls
			}
		};

	}

	if (req.user) {
		Post.find(searchQuery).sort('-created').limit(12).populate('user', '_id displayName username profileImageURL').populate('coupon.pickedGame').populate('quotedGames').exec(function (err, posts) {
				if (err) {
					return res.status(400).send(
						{
							message: errorHandler.getErrorMessage(err)
						});
				}
				else {
					res.jsonp(posts);
				}
			});
	}
};

/**
 * search posts
 */
exports.gameposts = function(req, res)
{

	var ls=new Date(req.body.lastseen);
	if (1) {
		Post.find(
			{
				$or : [{'coupon.pickedGame': req.body.gameid },
					{ 'quotedGames.0': req.body.gameid },
					{ 'quotedGames.1': req.body.gameid },
					{ 'quotedGames.2': req.body.gameid }],
				created: {
					$lt: ls
				}
			}).sort('-created').limit(10).populate('user', '_id displayName username profileImageURL').populate('coupon.pickedGame').populate('quotedGames').exec(function (err, posts) {
				if (err) {

					return res.status(400).send(
						{
							message: errorHandler.getErrorMessage(err)
						});
				}
				else {
					res.jsonp(posts);
				}
			});
	}
};



exports.listOf = function(req, res)
{
	Post.find(
		{
			user: req.body.userid,
			created:
			{
				$lt: new Date(req.body.lastseen)
			}
		}).sort('-created').limit(20).populate('user', '_id displayName username profileImageURL').populate('coupon.pickedGame').populate('quotedGames').exec(function(err, posts)
		{
			if (err)
			{
				return res.status(400).send(
					{
						message: errorHandler.getErrorMessage(err)
					});
			}
			else
			{
				res.jsonp(posts);
			}
		});
};

exports.checkNew = function(req, res)
{

	if (req.user) {
		Follower.find(
			{
				user: req.user._id
			},
			{
				_id: 0,
				followed: 1
			}).sort('-created').exec(function (err, followers) {
				if (err) {
					return res.status(400).send(
						{
							message: errorHandler.getErrorMessage(err)
						});
				}
				else {
					var arr = [];
					followers.forEach(function (element, index, array) {
						arr.push(element.followed);
					});
					arr.push(req.user._id);
					Post.count(
						{
							user: {
								$in: arr
							},
							created: {
								$gt: new Date(req.body.lastseen)
							}
						}).exec(function (err, posts) {
							if (err) {
								return res.status(400).send(
									{
										message: errorHandler.getErrorMessage(err)
									});
							}
							else {
								res.jsonp(posts);
							}
						});
				}
			});
	}
};

exports.postThread = function(req, res)
{
	Post.findById(req.params.postId).exec(function(err, post)
	{
		if (err)
		{
			return res.status(400).send(
				{
					message: errorHandler.getErrorMessage(err)
				});
		}
		else
		{
			Post.find(
				{
					$or: [
						{
							postIds:
							{
								$elemMatch:
								{
									$eq: mongoose.Types.ObjectId(post._id)
								}
							}
						},
						{
							_id: mongoose.Types.ObjectId(post._id)
						},
						{
							_id:
							{
								$in: post.postIds
							}
						}]
				}).sort('created').populate('user', '_id displayName username profileImageURL timezone').populate('coupon.pickedGame').populate('quotedGames').exec(function(err, posts)
				{
					if (err)
					{
						return res.status(400).send(
							{
								message: errorHandler.getErrorMessage(err)
							});
					}
					else
					{
						res.jsonp(posts);
					}
				});
		}
	});
};
exports.count = function(req, res)
{
	Post.count(
		{
			user: req.params.userid
		}).exec(function(err, count)
		{
			if (err)
			{
				return res.status(400).send(
					{
						message: errorHandler.getErrorMessage(err)
					});
			}
			else
			{
				res.jsonp(count);
			}
		});
};
/**
 * Post middleware
 */
exports.postByID = function(req, res, next, id)
{
	Post.findById(id).populate('user', '_id displayName username profileImageURL timezone').populate('coupon.pickedGame').populate('quotedGames').exec(function(err, post)
	{
		if (err) return next(err);
		if (!post) return next(new Error('Failed to load Post ' + id));
		req.post = post;
		next();
	});
};


exports.crawl = function(req, res)
{

	var swig  = require('swig');
	var sTitle, sDescription, sUrl, sImage, cardType;
	sTitle = 'Betroll';
	sDescription = 'Betting Connection';
	sUrl= req.protocol + '://' + req.headers.host + req.originalUrl;
	sImage ='https://betroll.net/modules/core/img/brand/logo.png';

	if (req.originalUrl.indexOf('posts/') > -1){
		Post.findById(req.params.postId).populate('user', '_id displayName username profileImageURL timezone').populate('coupon.pickedGame').populate('quotedGames').exec(function(err, post)
		{
			if (err)
			{
				return res.status(400).send(
					{
						message: errorHandler.getErrorMessage(err)
					});
			}
			else
			{
				sUrl= 'https://betroll.net/posts/' + post._id;
				if (post.imageURL) {
					cardType='summary_large_image';
					sImage = 'https://betroll.net/uploads/res/' + post._id;
				} else {
					cardType='summary';
					sImage = 'https://betroll.net/' + post.user.profileImageURL;
				}
				sTitle = 'Betroll | ' + post.user.displayName + ' @' + post.user.username + ' posted';
				var coupon ='\n';
				for (var say =0; say < post.coupon.length; say++) {
					coupon += post.coupon[say].pickedGame.homeTeamName + ' - ' +  post.coupon[say].pickedGame.awayTeamName + ' ' + post.coupon[say].betTypes + ' ' + parseFloat(post.coupon[say].pickedGame.odds[post.coupon[say].betTypesShort.split('.')[0]][post.coupon[say].betTypesShort.split('.')[1]]).toFixed(2) + '\n';
				}
				sDescription = post.postText;
				res.send(swig.renderFile('modules/posts/server/views/crawl.html', {
					title: sTitle,
					description: sDescription + ' ' + coupon,
					keywords: coupon,
					url: sUrl,
					image: sImage,
					post: post,
					cardType: cardType
				}));
			}
		});
	}
};
/**
 * add an image to a post
 */
exports.addPostImage = function(req, res)
{
	var message = null;
	fs.writeFile('./uploads/' + req.files.file.name, req.files.file.buffer, function(uploadError)
	{
		if (uploadError)
		{
			return res.send(
				{
					message: 'Error occurred while uploading picture'
				});
		}
		else
		{
			var tmp_path = 'uploads/' + req.files.file.name,
				out_path = 'uploads/res/' + req.files.file.name,
				photo;
			photo = images(tmp_path);
			photo.size(800)
				.save(out_path,
				{
					quality: 70
				});
			return res.send(
				{
					message: 'Upload successful',
					imageURL: req.files.file.name
				});
		}
	});
};
