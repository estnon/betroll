'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Post Schema
 */
var PostSchema = new Schema({
	postText: {
		type: String,
		default: '',
		trim: true
	},
	imageURL: {
		type: String,
	},
	embedURL: {
		type: String,
	},
	quotedGames: [{
		type: Schema.ObjectId,
		ref: 'Game'
	}],
	coupon: [{
		betTypes: { type: String },
		betTypesShort : { type: String },
		pickedGame: { type: Schema.ObjectId, ref: 'Game' }
	}],
	totalOdds: {
		type: String,
		default: '1'
	},
	postIds: [{
		type: Schema.ObjectId,
		ref: 'Post'
	}],
	userIds: [{
		type: Schema.ObjectId,
		ref: 'User'
	}],
	usernames: [{
		type: String
	}],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});
PostSchema.index({ postText: 'text'});
mongoose.model('Post', PostSchema);

