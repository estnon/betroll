'use strict';

module.exports = function(app) {
	var posts = require('../controllers/posts.server.controller');
	var postsPolicy = require('../policies/posts.server.policy');

	// Posts Routes
	app.route('/api/posts').all()
		.get(posts.list).all(postsPolicy.isAllowed)
		.post(posts.create);
	app.route('/api/posts/crawl/:postId').get(posts.crawl);
	app.route('/api/posts/timeline').post(posts.timeline);
	app.route('/api/posts/slips').post(posts.slips);
	app.route('/api/posts/search').post(posts.search);
	app.route('/api/posts/gameposts').post(posts.gameposts);
	app.route('/api/posts/of/').post(posts.listOf);
	app.route('/api/posts/remove/').post(posts.delete);
	app.route('/api/posts/count/:userid').get(posts.count);
	app.route('/api/posts/image').post(posts.addPostImage);
	app.route('/api/posts/thread/:postId').get(posts.postThread);
	app.route('/api/posts/new/').post(posts.checkNew);
	app.route('/api/posts/:postId').all(postsPolicy.isAllowed)
		.get(posts.read)
		.delete(posts.delete);

	app.param('postId', posts.postByID);
};
