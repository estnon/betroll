'use strict';

// Posts controller
angular.module('posts').controller('PostsController', ['$rootScope', '$scope', '$sce', '$stateParams', '$http', '$location', '$anchorScroll', 'Authentication', 'Posts',
	'couponOps', 'notifyOps', 'favArrs', 'listArrs', 'FileUploader', '$window', '$timeout', 'tickerVars', '$rootElement', 'langs', '$modalStack', 'progressBar', 'dateOps',
	function($rootScope, $scope, $sce, $stateParams, $http, $location, $anchorScroll, Authentication, Posts, couponOps, notifyOps, favArrs, listArrs, FileUploader, $window, $timeout, tickerVars, $rootElement, langs, $modalStack, progressBar, dateOps) {

		$scope.authentication = Authentication;
		$scope.user = Authentication.user;
		$scope.dateOps = dateOps;
		$scope.searchTerm ='latest posts';
		$scope.langs = langs;
		$scope.modalInstance = undefined;
		// Create file uploader instance
		$scope.uploader = new FileUploader({
			url: 'api/posts/image'
		});
		$scope.postText = couponOps.postText;
		$scope.autoExpand = function(e) {
			//couponOps.postText = $scope.postText;
			var element = typeof e === 'object' ? e.target : document.getElementById(e);
			var scrollHeight = element.scrollHeight - 0;
			element.style.height = scrollHeight + 'px';
			if (element.value.length < 48) document.getElementById('TextArea').style.height = 60 + 'px';
		};
		$scope.tickerVars = tickerVars;
		function expand() {
			$scope.autoExpand('TextArea');
		}



		// Set file uploader image filter
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function(item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|'.indexOf(type) !== -1;
			}
		});

		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function(fileItem) {

			if ($window.FileReader) {
				progressBar.status = 1;
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function(fileReaderEvent) {
					$timeout(function() {
						$scope.imageURL = fileReaderEvent.target.result;
					}, 0);
					progressBar.status = 0;
				};

			}
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
			// Show success message
			$scope.success = true;

			$scope.finalize(response.imageURL);

			// Clear upload buttons
			$scope.cancelUpload();

			progressBar.status = 0;
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();
			// Show error message
			$scope.error = response.message;
			couponOps.pop('error', langs.lang_strings[135], $scope.error);
			progressBar.status = 0;
		};

		// ...
		$scope.uploadImage = function() {
			// Clear messages
			$scope.success = $scope.error = null;

			// Start upload
			progressBar.status = 1;
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function() {
			$scope.uploader.clearQueue();
			$scope.imageURL = '';
		};

		$scope.couponOps = couponOps;

		$scope.quoteIDs = [];
		$scope.couponBuild = [];

		// Create new Post

		$scope.create = function() {
			progressBar.status = 1;
			if (!couponOps.postText) {
				couponOps.pop('error', 'Hey!', langs.lang_strings[144]);
				progressBar.status = 0;
				return;
			}

			if ($scope.uploader.queue.length) {
				$scope.uploadImage();

			} else {
				$scope.finalize();

			}



			for (var y = 0; y < couponOps.GlobalQuotes.quotes.length; y++) {
				$scope.quoteIDs.push(couponOps.GlobalQuotes.quotes[y].gameID);
			}


			for (var t = 0; t < couponOps.GlobalCoupon.coupon.length; t++) {
				$scope.couponBuild.push({
					pickedGame: couponOps.GlobalCoupon.coupon[t].gameID,
					betTypes: couponOps.GlobalCoupon.coupon[t].betType,
					betTypesShort: couponOps.GlobalCoupon.coupon[t].betTypeShort
				});
			}


		};


		$scope.finalize = function(finalImageURL) {
			var regexp = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/i;
			var Url1 = couponOps.postText.match(regexp);

			// Create new Post object
			var post = new Posts({
				postText: couponOps.postText,
				imageURL: finalImageURL,
				quotedGames: this.quoteIDs,
				coupon: this.couponBuild,
				totalOdds: couponOps.GlobalCoupon.totalOdds,
				postIds: couponOps.ReplyingTo.postIds,
				usernames: couponOps.ReplyingTo.usernames,
				userIds:couponOps.ReplyingTo.userIds
			});

			if (Url1) { post.embedURL = Url1[0]; }

			// after save
			post.$save(function(response) {
				progressBar.status = 1;
				$scope.quoteIDs = [];
				$scope.couponBuild = [];
				couponOps.postText = '';
				$scope.imageURL = '';
				couponOps.GlobalCoupon.coupon = [];
				couponOps.GlobalCoupon.totalOdds = 1;
				couponOps.GlobalQuotes.quotes = [];

				//notif
				couponOps.ReplyingTo.userIds.forEach(function(value) {
					if (value !== $scope.user._id) notifyOps.create(response._id, value, 'Mentioned', 'mention');
				});


				couponOps.unReplyTo();

				document.getElementById('TextArea').style.height = 60 + 'px';


				progressBar.status = 0;
				$modalStack.dismissAll();
				$location.path('posts/' + response._id);

			}, function(errorResponse) {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		// Remove existing Post
		$scope.remove = function( post ) {
			if ( post ) { post.$remove();

				for (var i in $scope.posts ) {
					if ($scope.posts [i] === post ) {
						$scope.posts.splice(i, 1);
					}
				}
			} else {
				$scope.post.$remove(function() {
					$location.path('posts');
				});
			}

		};

		// Find a list of Posts
		$scope.find = function() {
			$scope.makeQuery('/api/posts/timeline');
		};

		$scope.findSlips = function() {
			$scope.makeQuery('/api/posts/slips');
		};

		$scope.makeQuery = function(endPoint) {


			if (!$scope.user) {
				$location.path('/');
			} else {
				progressBar.status = 1;
				if (typeof $scope.posts !== 'undefined') {
					$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
				} else {
					$scope.lastSeen = Date.now();
				}

				var postdata = {
					lastseen: $scope.lastSeen
				};

				var timelinePromise = $http.post(endPoint, postdata);
				timelinePromise.success(function (timelinedata) {

					if (typeof $scope.posts !== 'undefined') {
						$scope.posts = $scope.posts.concat(timelinedata);
					} else {
						$scope.posts = timelinedata;
					}

					tickerVars.Timeline.time=Date.now();
					tickerVars.Timeline.latest = '';

					favArrs.FavList.user = $scope.user._id;
					favArrs.FavList.postIds = [];
					favArrs.FavList.postIds = $scope.posts.map(function (e) {
						var key;
						for (key in e) // push postids
							return e[key];
					});

					$scope.checkIfFaved();


					listArrs.ListList.user = $scope.user._id;
					listArrs.ListList.postIds = [];
					listArrs.ListList.postIds = favArrs.FavList.postIds;

					$scope.checkIfShared();
					progressBar.status = 0;

				});
				timelinePromise.error(function () {
					couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
					progressBar.status = 0;
				});
			}
		};

		$scope.search = function(more) {
			if ($scope.searchTerm.length <3 ){
				couponOps.pop('error', langs.lang_strings[145], langs.lang_strings[146]);
				return;
			}
			progressBar.status = 1;
			if (more === true) {
				$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
			} else {
				$scope.lastSeen = Date.now();
				$scope.posts = '';
			}

			var postdata = {
				searchterm: $scope.searchTerm,
				lastseen: $scope.lastSeen
			};

			var timelinePromise = $http.post('/api/posts/search', postdata);
			timelinePromise.success(function (timelinedata) {

				if (more === true) {
					$scope.posts = $scope.posts.concat(timelinedata);
				} else {
					$scope.posts = timelinedata;
				}
				//$scope.embedly(timelinedata);

				if (!timelinedata.length) { $scope.noResults = true;} else {$scope.noResults = false;}
				progressBar.status = 0;
			});
			timelinePromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[147] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

		// Find existing Post
		$scope.findThread = function() {
			progressBar.status = 1;
			$scope.activePost = $stateParams.postId;
			var threadPromise = $http.get('/api/posts/thread/' + $scope.activePost);
			threadPromise.success(function(data) {
				$scope.posts = data;
				//$scope.embedly(data);
				favArrs.FavList.user = $scope.user._id;
				favArrs.FavList.postIds = [];
				favArrs.FavList.postIds = $scope.posts.map(function(e) {
					var key;
					for (key in e) // push postids
						return e[key];
				});


				$scope.checkIfFaved();


				listArrs.ListList.user = $scope.user._id;
				listArrs.ListList.postIds = [];
				listArrs.ListList.postIds = favArrs.FavList.postIds;


				$scope.checkIfShared();

				$location.hash($scope.activePost);
				$anchorScroll();
				progressBar.status = 0;

			});
			threadPromise.error(function() {
				progressBar.status = 0;
				$location.path('posts/missing');
			});

		};


		$scope.checkIfFaved = function() {
			var favListPromise = $http.post('/api/favs/timeline', favArrs.FavList);
			favListPromise.success(function(favListData) {
				var tempList = [];
				for (var i = 0; i < favListData.length; i++) {
					tempList[favListData[i].post] = true;
				}
				favArrs.isFaved = tempList;

			});
			favListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[142] + ' ' + langs.lang_strings[136]);
			});

		};

		$scope.checkIfShared = function() {
			var listListPromise = $http.post('/api/lists/timeline', listArrs.ListList);
			listListPromise.success(function(listListData) {
				var tempList = [];
				for (var i = 0; i < listListData.length; i++) {
					tempList[listListData[i].post] = true;
				}
				listArrs.isShared = tempList;

			});
			listListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[143] + ' ' + langs.lang_strings[136]);
			});

		};

		$scope.findOne = function() {
			progressBar.status = 1;
			$scope.post = Posts.get({
				postId: $stateParams.postId
			},
				undefined,
				function(){
					$scope.favCount();
					$scope.shareCount();

					favArrs.FavList.user = $scope.user._id;
					favArrs.FavList.postIds = [];
					favArrs.FavList.postIds.push($stateParams.postId);

					$scope.checkIfFaved();


					listArrs.ListList.user = $scope.user._id;
					listArrs.ListList.postIds = [];
					listArrs.ListList.postIds = favArrs.FavList.postIds;


					$scope.checkIfShared();
					progressBar.status = 0;
				},
				function() {
					progressBar.status = 0;
					$location.path('posts/missing');
				});


		};

		$scope.favCount = function() {
			//fav count
			var favCountCurrentPromise = $http.get('/api/favs/count/of/post/' + $stateParams.postId);
			favCountCurrentPromise.success(function(result) {
				$scope.favCountOfPost = result;
			});
			favCountCurrentPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
			});
		};

		$scope.shareCount = function() {
			//share count
			var shareCountCurrentPromise = $http.get('/api/lists/count/of/post/' + $stateParams.postId);
			shareCountCurrentPromise.success(function(result) {
				$scope.shareCountOfPost = result;
			});
			shareCountCurrentPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
			});
		};

		$scope.gameposts = function(more) {

			progressBar.status = 1;
			if (more === true) {
				$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
			} else {
				$scope.lastSeen = Date.now();
				$scope.posts = '';
			}

			var postdata = {
				gameid: $stateParams.gameId,
				lastseen: $scope.lastSeen
			};

			var timelinePromise = $http.post('/api/posts/gameposts', postdata);
			timelinePromise.success(function (timelinedata) {

				if (more === true) {
					$scope.posts = $scope.posts.concat(timelinedata);
				} else {
					$scope.posts = timelinedata;
				}
				//$scope.embedly(timelinedata);

				if (!timelinedata.length) { $scope.noResults = true;} else {$scope.noResults = false;}
				progressBar.status = 0;
			});
			timelinePromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

	}
]);
