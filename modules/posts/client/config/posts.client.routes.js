'use strict';

//Setting up route
angular.module('posts').config(['$stateProvider',
	function($stateProvider) {
		// Posts state routing
		$stateProvider.
			state('posts', {
				abstract: true,
				url: '/posts',
				template: '<ui-view/>'
			}).
			state('posts.list', {
				url: '',
				templateUrl: 'modules/posts/views/list-posts.client.view.html'
			}).
			state('posts.slips', {
				url: '',
				templateUrl: 'modules/posts/views/list-posts-slip-only.client.view.html'
			}).
			state('posts.latest', {
				url: '/latest',
				templateUrl: 'modules/posts/views/latest-post.client.view.html'
			}).
			state('posts.search', {
				url: '/search',
				templateUrl: 'modules/posts/views/search-post.client.view.html'
			}).
			state('posts.create', {
				url: '/create',
				templateUrl: 'modules/posts/views/create-post.client.view.html'
			}).
			state('posts.missing', {
				url: '/missing',
				templateUrl: 'modules/posts/views/post-not-found.client.view.html'
			}).
			state('posts.view', {
				url: '/:postId',
				templateUrl: 'modules/posts/views/view-post.post.client.view.html'
			}).
			state('posts.thread', {
				url: '/thread/:postId',
				templateUrl: 'modules/posts/views/view-post.thread.client.view.html'
			});
	}
]);
