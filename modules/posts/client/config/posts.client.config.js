'use strict';

// Configuring the Posts module
angular.module('posts').run(['Menus',
	function(Menus) {
		// Add the Posts dropdown item
		Menus.addMenuItem('topbar', {
			title: 22,
			state: 'posts.list',
			position: 2
		});

	}
]);
