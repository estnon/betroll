'use strict';

//Posts service used to communicate Posts REST endpoints
angular.module('posts').factory('Posts', ['$resource',
	function($resource) {
		return $resource('api/posts/:postId', { postId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);


angular.module('posts')
	.filter('highlightWords', function () {
		return function (text, search) {
			if (text && (search || angular.isNumber(search))) {
				text = text.toString();
				search = search.toString();

				angular.forEach(search.split(/\s+/), function(word) {

					if (['span', 'class', 'ui-match'].indexOf(word) < 0) {

						var pattern = new RegExp('\\b' + word + '\\b', 'gi');
						text = text.replace(pattern, '<strong class="highlight ui-match">$&</strong>');
					}
				});
			}
			return text;
		};
	});

angular.module('posts').directive('showEmbed', ['$http', '$sce', function($http, $sce)
{
	return {
		restrict: 'E',
		scope:
		{
			embedUrl: '@'
		},
		link: function(scope, element, attrs)
		{
			scope.$watch('embedUrl', function(value)
			{
				if (value)
				{
					var threadPromise = $http.get('https://api.embed.ly/1/oembed?url=' + attrs.embedUrl + '&key=314af94fce4149d384ed211ac1cce22b&?secure=true');
					threadPromise.success(function(data)
					{
						var embedWrapper = '';

						if (data.type === 'link') {
							embedWrapper += '<a href="' + data.url + '" target="_blank">';
						}

						embedWrapper += '<br /><div class="embedded">';

						if (data.title) {
							embedWrapper += '<strong class="text-info">' + data.title + '</strong>';
						}
						if (data.description) {
							embedWrapper += '<div class="text-muted">' + data.description + '</div>';
						}

						if (data.type !== 'link' && data.html) {
							embedWrapper += '<div class="responsive-object">' + data.html + '</div>';
						}
						if (data.type === 'photo') {
							embedWrapper += '<div class="responsive-object"><a href="' + data.url + '" target="_blank"><img class="img-thumbnail post-image" src="' + data.url + '"></a></div>';
						}

						embedWrapper += '</div>';

						if (data.type === 'link') {
							embedWrapper += '</a>';
						}

						element.html(embedWrapper);

					});
					threadPromise.error(function()
					{
						element.html = '';
					});
				}
			});
		}
	};
}]);


angular.module('posts').directive('aPost', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/posts/views/apost.html'
	};
});

angular.module('posts').directive('postButtons', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/posts/views/postbuttons.html'
	};
});

angular.module('posts').directive('createPost', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/posts/views/create-post-template.client.view.html'
	};
});
