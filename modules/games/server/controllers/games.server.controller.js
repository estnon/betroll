'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	Game = mongoose.model('Game'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a Game
 */
exports.create = function(req, res) {
	var game = new Game(req.body);
	if (game.vnumber ==='AskjhskD324ASklk43_kjhdT%') {
		game.user = req.user;

		game.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(game);
			}
		});
	} else {
		return res.status(400).send({
			message: 'Something wrong.'
		});
	}
};

/**
 * Show the current Game
 */
exports.read = function(req, res) {
	res.jsonp(req.game);
};

/**
 * Update a Game
 */
exports.update = function(req, res) {
	if (req.user.username === 'betroll') {
		var home, away;
		var fhome, faway;


		if (req.body.score.totalHomeScore) {
			home = parseInt(req.body.score.totalHomeScore);
		} else {
			home = parseInt(req.body.score.totalHome);
		}

		if (req.body.score.totalAwayScore) {
			away = parseInt(req.body.score.totalAwayScore);
		} else {
			away = parseInt(req.body.score.totalAway);
		}

		var goalCount = 0;


		var game = {
			status: req.body.status,
			awayTeamName: req.body.awayTeamName,
			homeTeamName: req.body.homeTeamName,
			startTime: req.body.startTime,
			score: {
				minute: req.body.score.minute,
				firstHalfHome: req.body.score.firstHalfHome | '',
				firstHalfAway: req.body.score.firstHalfAway | '',
				totalHome: home,
				totalAway: away
			}

		};
		if (req.body.score.firstHalfHome) {
			game.score.firstHalfHome = req.body.score.firstHalfHome;
		}
		if (req.body.score.firstHalfAway) {
			game.score.firstHalfAway = req.body.score.firstHalfAway;
		}
		if (req.body.score.secondHalfHome) {
			game.score.secondHalfHome = parseInt(req.body.score.secondHalfHome);
		}
		if (req.body.score.secondHalfAway) {
			game.score.secondHalfAway = parseInt(req.body.score.secondHalfAway);
		}
		if (req.body.score.extraTimeHome) {
			game.score.extraTimeHome = parseInt(req.body.score.extraTimeHome);
		}
		if (req.body.score.extraTimeAway) {
			game.score.extraTimeAway = parseInt(req.body.score.extraTimeAway);
		}
		if (req.body.score.penaltiesHome) {
			game.score.penaltiesHome = parseInt(req.body.score.penaltiesHome);
		}
		if (req.body.score.penaltiesAway) {
			game.score.penaltiesAway = parseInt(req.body.score.penaltiesAway);
		}
		if (game.score.firstHalfHome !== '') {
			faway = parseInt(game.score.firstHalfAway);
		}

		if (game.score.firstHalfHome !== '') {
			faway = parseInt(game.score.firstHalfAway);
		}


		goalCount = parseInt(home) + parseInt(away);

		game.result = {
			main: {
				home: home > away,
				draw: home === away,
				away: home < away
			},
			doubleChance: {
				homeDraw: (home > away) || (home === away),
				homeAway: (home > away) || (home < away),
				drawAway: (home === away) || (home < away)
			},
			overUnder: {
				over05: goalCount > 0.5,
				over15: goalCount > 1.5,
				over25: goalCount > 2.5,
				over35: goalCount > 3.5,
				over45: goalCount > 4.5,
				under05: goalCount < 0.5,
				under15: goalCount < 1.5,
				under25: goalCount < 2.5,
				under35: goalCount < 3.5,
				under45: goalCount < 4.5
			},
			halfTimeFullTime: {
				homeHome: fhome !== '' && (home > away) && (fhome > faway),
				homeDraw: fhome !== '' && (home === away) && (fhome > faway),
				homeAway: fhome !== '' && (home < away) && (fhome > faway),
				drawHome: fhome !== '' && (home > away) && (fhome === faway),
				drawDraw: fhome !== '' && (home === away) && (fhome === faway),
				drawAway: fhome !== '' && (home < away) && (fhome === faway),
				awayHome: fhome !== '' && (home > away) && (fhome < faway),
				awayDraw: fhome !== '' && (home === away) && (fhome < faway),
				awayAway: fhome !== '' && (home < away) && (fhome < faway)
			},
			correctScore: {
				cs00: (home === 0 && away === 0 ),
				cs10: (home === 1 && away === 0 ),
				cs11: (home === 1 && away === 1 ),
				cs01: (home === 0 && away === 1 ),
				cs20: (home === 2 && away === 0 ),
				cs21: (home === 2 && away === 1 ),
				cs22: (home === 2 && away === 2 ),
				cs02: (home === 0 && away === 2 ),
				cs12: (home === 1 && away === 2 ),
				cs30: (home === 3 && away === 0 ),
				cs31: (home === 3 && away === 1 ),
				cs32: (home === 3 && away === 2 ),
				cs33: (home === 3 && away === 3 ),
				cs03: (home === 0 && away === 3 ),
				cs13: (home === 1 && away === 3 ),
				cs23: (home === 2 && away === 3 ),
				cs40: (home === 4 && away === 0 ),
				cs41: (home === 4 && away === 1 ),
				cs42: (home === 4 && away === 2 ),
				cs43: (home === 4 && away === 3 ),
				cs44: (home === 4 && away === 4 ),
				cs04: (home === 0 && away === 4 ),
				cs14: (home === 1 && away === 4 ),
				cs24: (home === 2 && away === 4 ),
				cs34: (home === 3 && away === 4 )
			},
			handicap10: {
				home: (home - away) === 0,
				draw: (away - home) === 1,
				away: (away - home) >= 2
			},
			handicap01: {
				home: (home - away) >= 2,
				draw: (home - away) === 1,
				away: (home - away) === 0
			},
			totalGoals: {
				tg1orMore: goalCount > 0,
				tg2orMore: goalCount > 1,
				tg3orMore: goalCount > 2,
				tg4orMore: goalCount > 3,
				tg5orMore: goalCount > 4,
				tg6orMore: goalCount > 5,
				tg7orMore: goalCount > 6
			},
			bothScores: {
				yes: (home > 0.5 && away > 0.5 ),
				no: (home < 0.5 || away < 0.5 )
			}
		};
		game.updated = new Date();
		Game.update({_id: req.body._id}, game, {multi: false}).exec(function (err, result) {
			if (err) {

				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {

				res.jsonp(game);

			}
		}); //change


	}

};

exports.scores = function (req, res) {
	var home, away;
	var fhome, faway;

	if (req.body.totalHomeScore !== '') {
		home = parseInt(req.body.totalHomeScore);
	} else {
		home = parseInt(req.body.totalHome);
	}

	if (req.body.totalAwayScore !== '') {
		away = parseInt(req.body.totalAwayScore);
	} else {
		away = parseInt(req.body.totalAway);
	}


	var game= {
		status: req.body.status,
		seed : req.body.seed,
		flag: req.body.flag,
		awayTeamName : req.body.awayTeamName,
		homeTeamName : req.body.homeTeamName,
		startTime : req.body.startTime,
		vnumber : req.body.vnumber,
		score : {
			minute : req.body.minute,
			totalHome : home,
			totalAway : away
		}

	};

	if (req.body.firstHalfHome) {
		game.score.firstHalfHome = req.body.firstHalfHome;
	}
	if (req.body.firstHalfAway) {
		game.score.firstHalfAway = req.body.firstHalfAway;
	}
	/*if (req.body.secondHalfHome) {
		game.score.secondHalfHome = req.body.secondHalfHome;
	}
	if (req.body.secondHalfAway) {
		game.score.secondHalfAway = req.body.secondHalfAway;
	}
	if (req.body.extraTimeHome) {
		game.score.extraTimeHome = req.body.extraTimeHome;
	}
	if (req.body.extraTimeAway) {
		game.score.extraTimeAway = req.body.extraTimeAway;
	}
	if (req.body.penaltiesHome) {
		game.score.penaltiesHome = req.body.penaltiesHome;
	}
	if (req.body.penaltiesAway) {
		game.score.penaltiesAway = req.body.penaltiesAway;
	}*/

	var goalCount = 0;

	if (game.score.firstHalfHome !== '') {
		fhome = parseInt(game.score.firstHalfHome);
	}
	if (game.score.firstHalfHome !== '') {
		faway = parseInt(game.score.firstHalfAway);
	}

	goalCount = parseInt(home) + parseInt(away);

	game.result = {
		main: {
			home:  home >  away ,
			draw: home === away ,
			away: home <  away
		},
		doubleChance: {
			homeDraw: (home >  away) || (home ===  away),
			homeAway: (home >  away) || (home <  away),
			drawAway: (home ===  away) || (home <  away)
		},
		overUnder: {
			over05: goalCount > 0.5,
			over15: goalCount > 1.5,
			over25: goalCount > 2.5,
			over35: goalCount > 3.5,
			over45: goalCount > 4.5,
			under05: goalCount < 0.5,
			under15: goalCount < 1.5,
			under25: goalCount < 2.5,
			under35: goalCount < 3.5,
			under45: goalCount < 4.5
		},
		halfTimeFullTime: {
			homeHome: fhome !== '' &&  (home > away) && (fhome > faway),
			homeDraw: fhome !== '' &&  (home === away) && (fhome > faway),
			homeAway: fhome !== '' &&  (home < away) && (fhome > faway),
			drawHome: fhome !== '' &&  (home > away) && (fhome === faway),
			drawDraw: fhome !== '' &&  (home === away) && (fhome === faway),
			drawAway: fhome !== '' &&  (home < away) && (fhome === faway),
			awayHome: fhome !== '' &&  (home > away) && (fhome < faway),
			awayDraw: fhome !== '' &&  (home === away) && (fhome < faway),
			awayAway: fhome !== '' &&  (home < away) && (fhome < faway)
		},
		correctScore: {
			cs00:  (home === 0 && away === 0 ),
			cs10:  (home === 1 && away === 0 ),
			cs11:  (home === 1 && away === 1 ),
			cs01:  (home === 0 && away === 1 ),
			cs20:  (home === 2 && away === 0 ),
			cs21:  (home === 2 && away === 1 ),
			cs22:  (home === 2 && away === 2 ),
			cs02:  (home === 0 && away === 2 ),
			cs12:  (home === 1 && away === 2 ),
			cs30:  (home === 3 && away === 0 ),
			cs31:  (home === 3 && away === 1 ),
			cs32:  (home === 3 && away === 2 ),
			cs33:  (home === 3 && away === 3 ),
			cs03:  (home === 0 && away === 3 ),
			cs13:  (home === 1 && away === 3 ),
			cs23:  (home === 2 && away === 3 ),
			cs40:  (home === 4 && away === 0 ),
			cs41:  (home === 4 && away === 1 ),
			cs42:  (home === 4 && away === 2 ),
			cs43:  (home === 4 && away === 3 ),
			cs44:  (home === 4 && away === 4 ),
			cs04:  (home === 0 && away === 4 ),
			cs14:  (home === 1 && away === 4 ),
			cs24:  (home === 2 && away === 4 ),
			cs34:  (home === 3 && away === 4 )
		},
		handicap10: {
			home:  (home - away) === 0,
			draw:  (away - home) === 1,
			away:  (away - home) >= 2
		},
		handicap01: {
			home:  (home - away) >= 2,
			draw:  (home - away) === 1,
			away:  (home - away) === 0
		},
		totalGoals: {
			tg1orMore:  goalCount > 0,
			tg2orMore:  goalCount > 1,
			tg3orMore:  goalCount > 2,
			tg4orMore:  goalCount > 3,
			tg5orMore:  goalCount > 4,
			tg6orMore:  goalCount > 5,
			tg7orMore:  goalCount > 6
		},
		bothScores: {
			yes:  (home > 0.5 && away > 0.5 ),
			no:   (home < 0.5 || away < 0.5 )
		}
	};

	if (game.vnumber ==='AskjhskD324ASklk43_kjhdT%') {
		game.user = req.user;

		var simdi = new Date();
		var altiSaatOnce = new Date(simdi.getTime() - 6 * 60 * 60 * 1000); // 6 saat onceden baslayip simdiye kadar olanlar

		game.updated = simdi;

		Game.update({ startTime: {$gte: altiSaatOnce, $lte: simdi}, homeTeamName: game.homeTeamName, awayTeamName: game.awayTeamName}, game, { multi: false }).exec(function(err, result){
			if (err) {

				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {

				if (req.body.flag === '1') {

					Game.update({
						seed: {$lt: req.body.seed},
						status: 2
					}, {status: 3}, {multi: true}).exec(function (err, result) {
						if (err) {
							console.log(err);
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {

							res.jsonp('OK');
						}
					}); //set final ended games
				} else {

					res.jsonp('OK');
				}
			}
		}); //change



	} else {
		return res.status(400).send({
			message: 'Something wrong.'
		});
	}

};

/**
 * Delete an Game
 */
exports.delete = function(req, res) {
	var game = req.game;
	if (req.user.username === 'betroll') {
		game.remove(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(game);
			}
		});
	}
};

/**
 * List of Games
 */
exports.list = function(req, res) {
		var query = {};
		var limit = 50;
		if (typeof req.query.sport !== 'undefined') {
			query.sport = req.query.sport;
		}
		if (typeof req.query.country !== 'undefined') {
			query.country = req.query.country;
		}
		if (typeof req.query.league !== 'undefined') {
			query.league = req.query.league;
		}
		if (typeof req.query.status !== 'undefined') {
			/*switch(req.query.status) {
				case 'Upcoming':
					query.status = 1;
					break;
				case 'Live':
					query.status = 2;
					break;
				case 'Ended':
					query.status = 3;
					break;
			}*/
			query.status = req.query.status;
		}
		if (typeof req.query.startTime !== 'undefined') {
			if (typeof req.query.country !== 'undefined') limit = 0;

			var dd = req.query.startTime.split('-')[2];
			var mm = req.query.startTime.split('-')[1] - 1;
			var yyyy = req.query.startTime.split('-')[0];

			var firstDay = new Date(yyyy, mm, dd, 0, 0, 0);
			var nextDay = new Date(firstDay.getTime() + 24 * 60 * 60 * 1000);

			query.startTime = {$gte: firstDay, $lte: nextDay};
		}

		Game.find(query).select('-vnumber -rb_id').sort({startTime: -1}).limit(limit).exec(function (err, games) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(games);
			}
		});
};

/**
 * live  Games
 */
exports.live = function(req, res) {
	//var updated = new Date(req.query.updated);

	var simdi = new Date();
	var ucSaatOnce = new Date(simdi.getTime() - 2 * 60 * 60 * 1000); // 2 saat onceden baslayip simdiye kadar olanlar

	Game.find({
		$or: [{
			status: 2
		}, {
			status: 3,
			updated: {$gte: ucSaatOnce}
		}]
	}).select('-vnumber -rb_id').sort({'status' : '1'}).exec(function (err, games) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var gamesArray = {};
			for (var i =0; i < games.length; i++) {

				gamesArray[games[i]._id] = games[i];
			}
			res.jsonp(gamesArray);
		}
	});
};
/**
 * List of countries
 */
exports.countries = function(req, res) {
	var query = {};
	if (typeof req.body.startTime !== 'undefined') {

		var dd = req.body.startTime.split('-')[2];
		var mm = req.body.startTime.split('-')[1] - 1;
		var yyyy = req.body.startTime.split('-')[0];

		var firstDay = new Date(yyyy, mm, dd, 0, 0, 0);
		var nextDay = new Date(firstDay.getTime() + 24 * 60 * 60 * 1000);

		query.startTime = {$gte: firstDay, $lte: nextDay};
	}
	if (typeof req.body.sport !== 'undefined') {
		query.sport = req.body.sport;
	}

	Game.distinct('country', query).exec(function(err, countries) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(countries);
		}
	});
};

/**
 * List of leagues
 */
exports.leagues = function(req, res) {
	var query = {};
	if (typeof req.body.startTime !== 'undefined') {

		var dd = req.body.startTime.split('-')[2];
		var mm = req.body.startTime.split('-')[1] - 1;
		var yyyy = req.body.startTime.split('-')[0];

		var firstDay = new Date(yyyy, mm, dd, 0, 0, 0);
		var nextDay = new Date(firstDay.getTime() + 24 * 60 * 60 * 1000);

		query.startTime = {$gte: firstDay, $lte: nextDay};
	}
	if (typeof req.body.sport !== 'undefined') {
		query.sport = req.body.sport;
	}
	if (typeof req.body.country !== 'undefined') {
		query.country = req.body.country;
	}

	Game.distinct('league', query ).exec(function(err, countries) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(countries);
		}
	});
};

exports.findgame_rbid = function(req, res) {
	Game.count({
		rb_id: req.params.rbid
	}).exec(function(err, count) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if (count > 0) res.jsonp(true);
			else res.jsonp(false);
		}
	});
};

/**
 * Game middleware
 */
exports.gameByID = function(req, res, next, id) {

	Game.findById(id).select('-vnumber -rb_id').exec(function(err, game) {
		if (err) return next(err);
		if (!game) return next(new Error('Failed to load Game ' + id));
		req.game = game;
		next();
	});
};

exports.crawl = function(req, res)
{

	var swig  = require('swig');
	var sTitle, sDescription, sUrl, sImage;
	sTitle = 'Betroll';
	sDescription = 'Betting Connection';
	sUrl= req.protocol + '://' + req.headers.host + req.originalUrl;
	sImage ='https://betroll.net/modules/core/img/brand/logo.png';

	if (req.originalUrl.indexOf('games/') > -1){
		Game.findById(req.params.gameId).exec(function(err, game)
		{
			if (err)
			{
				return res.status(400).send(
					{
						message: errorHandler.getErrorMessage(err)
					});
			}
			else
			{
				sUrl= 'https://betroll.net/games/' + game._id;
				sImage ='https://betroll.net/modules/core/img/brand/logo.png';
				sTitle = 'Betroll | ' + game.homeTeamName + ' - ' + game.awayTeamName + ' ' + game.startTime;
				res.send(swig.renderFile('modules/games/server/views/game.html', {
					title: sTitle,
					description: sTitle + ' ' + game.sport + ' - ' + game.country,
					url: sUrl,
					image: sImage,
					keywords:  sTitle + ' ' + game.sport + ' - ' + game.country,
					game: game
				}));
			}
		});
	}
};
