'use strict';

module.exports = function(app) {
	var games = require('../controllers/games.server.controller');
	var gamesPolicy = require('../policies/games.server.policy');

	// Games Routes
	app.route('/api/games').all()
		.get(games.list).all(gamesPolicy.isAllowed)
		.post(games.create).all(gamesPolicy.isAllowed);
	app.route('/api/games/crawl/:gameId').get(games.crawl);
	app.route('/api/games/live').get(games.live);
	app.route('/api/games/rbid/:rbid').all()
		.get(games.findgame_rbid).all(gamesPolicy.isAllowed);
	app.route('/api/games/countries/').all()
		.post(games.countries).all(gamesPolicy.isAllowed);
	app.route('/api/games/leagues/').all()
		.post(games.leagues).all(gamesPolicy.isAllowed);
	app.route('/api/games/scores/').all()
		.post(games.scores).all(gamesPolicy.isAllowed);
	app.route('/api/games/:gameId').all()
		.get(games.read)
		.put(games.update)
		.delete(games.delete);

	// Finish by binding the Game middleware
	app.param('gameId', games.gameByID);
};
