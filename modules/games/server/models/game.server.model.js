'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Game Schema
 */
var GameSchema = new Schema({
	rb_id: {
		type: String,
		default: '',
		trim: true
	},
	vnumber: {
		type: String,
		default: '',
		trim: true
	},
	sport: {
		type: String,
		default: 'Football',
		required: 'Please fill in Sport',
		trim: true
	},
	country: {
		type: String,
		default: '',
		required: 'Please fill in Country',
		trim: true
	},
	league: {
		type: String,
		default: '',
		required: 'Please fill in League',
		trim: true
	},
	startTime: {
		type: Date,
		default: Date.now,
		trim: true
	},
	updated: {
		type: Date,
		default: Date.now,
		trim: true
	},
	homeTeamName: {
		type: String,
		default: '',
		required: 'Please fill in Home Team',
		trim: true
	},
	awayTeamName: {
		type: String,
		default: '',
		required: 'Please fill in Away Team',
		trim: true
	},
	score: {
		minute: {
			type: Number
		},
		firstHalfHome: {
			type: Number
		},
		firstHalfAway: {
			type: Number
		},
		secondHalfHome: {
			type: Number
		},
		secondHalfAway: {
			type: Number
		},
		extraTimeHome: {
			type: Number
		},
		extraTimeAway: {
			type: Number
		},
		penaltiesHome: {
			type: Number
		},
		penaltiesAway: {
			type: Number
		},
		totalHome: {
			type: Number
		},
		totalAway: {
			type: Number
		}
	},
	seed: {
		type: Number
	},
	result: {
		main: {
			home: { type: Boolean },
			draw: {	type: Boolean },
			away: { type: Boolean }
		},
		doubleChance: {
			homeDraw: { type: Boolean },
			homeAway: {	type: Boolean },
			drawAway: { type: Boolean }
		},
		overUnder: {
			over05: { type: Boolean },
			over15: { type: Boolean },
			over25: { type: Boolean },
			over35: { type: Boolean },
			over45: { type: Boolean },
			under05: { type: Boolean },
			under15: { type: Boolean },
			under25: { type: Boolean },
			under35: { type: Boolean },
			under45: { type: Boolean }
		},
		halfTimeFullTime: {
			homeHome: { type: Boolean },
			homeDraw: { type: Boolean },
			homeAway: { type: Boolean },
			drawHome: { type: Boolean },
			drawDraw: { type: Boolean },
			drawAway: { type: Boolean },
			awayHome: { type: Boolean },
			awayDraw: { type: Boolean },
			awayAway: { type: Boolean }
		},
		correctScore: {
			cs00: { type: Boolean },
			cs10: { type: Boolean },
			cs11: { type: Boolean },
			cs01: { type: Boolean },
			cs20: { type: Boolean },
			cs21: { type: Boolean },
			cs22: { type: Boolean },
			cs02: { type: Boolean },
			cs12: { type: Boolean },
			cs30: { type: Boolean },
			cs31: { type: Boolean },
			cs32: { type: Boolean },
			cs33: { type: Boolean },
			cs03: { type: Boolean },
			cs13: { type: Boolean },
			cs23: { type: Boolean },
			cs40: { type: Boolean },
			cs41: { type: Boolean },
			cs42: { type: Boolean },
			cs43: { type: Boolean },
			cs44: { type: Boolean },
			cs04: { type: Boolean },
			cs14: { type: Boolean },
			cs24: { type: Boolean },
			cs34: { type: Boolean }
		},
		handicap10: {
			home: { type: Boolean },
			draw: {	type: Boolean },
			away: { type: Boolean }
		},
		handicap01: {
			home: { type: Boolean },
			draw: {	type: Boolean },
			away: { type: Boolean }
		},
		totalGoals: {
			tg1orMore: { type: Boolean },
			tg2orMore: { type: Boolean },
			tg3orMore: { type: Boolean },
			tg4orMore: { type: Boolean },
			tg5orMore: { type: Boolean },
			tg6orMore: { type: Boolean },
			tg7orMore: { type: Boolean }
		},
		bothScores: {
			yes: { type: Boolean },
			no: { type: Boolean }
		}
	},
	status: {
		type: String,
		default: 'Normal',
		trim: true
	},
	odds: {
		main: {
			home: { type: Number },
			draw: {	type: Number },
			away: { type: Number }
		},
		doubleChance: {
			homeDraw: { type: Number },
			homeAway: {	type: Number },
			drawAway: { type: Number }
		},
		overUnder: {
			over05: { type: Number },
			over15: { type: Number },
			over25: { type: Number },
			over35: { type: Number },
			over45: { type: Number },
			under05: { type: Number },
			under15: { type: Number },
			under25: { type: Number },
			under35: { type: Number },
			under45: { type: Number }
		},
		halfTimeFullTime: {
			homeHome: { type: Number },
			homeDraw: { type: Number },
			homeAway: { type: Number },
			drawHome: { type: Number },
			drawDraw: { type: Number },
			drawAway: { type: Number },
			awayHome: { type: Number },
			awayDraw: { type: Number },
			awayAway: { type: Number }
		},
		correctScore: {
			cs00: { type: Number },
			cs10: { type: Number },
			cs11: { type: Number },
			cs01: { type: Number },
			cs20: { type: Number },
			cs21: { type: Number },
			cs22: { type: Number },
			cs02: { type: Number },
			cs12: { type: Number },
			cs30: { type: Number },
			cs31: { type: Number },
			cs32: { type: Number },
			cs33: { type: Number },
			cs03: { type: Number },
			cs13: { type: Number },
			cs23: { type: Number },
			cs40: { type: Number },
			cs41: { type: Number },
			cs42: { type: Number },
			cs43: { type: Number },
			cs44: { type: Number },
			cs04: { type: Number },
			cs14: { type: Number },
			cs24: { type: Number },
			cs34: { type: Number }
		},
		handicap10: {
			home: { type: Number },
			draw: {	type: Number },
			away: { type: Number }
		},
		handicap01: {
			home: { type: Number },
			draw: {	type: Number },
			away: { type: Number }
		},
		totalGoals: {
			tg1orMore: { type: Number },
			tg2orMore: { type: Number },
			tg3orMore: { type: Number },
			tg4orMore: { type: Number },
			tg5orMore: { type: Number },
			tg6orMore: { type: Number },
			tg7orMore: { type: Number }
		},
		bothScores: {
			yes: { type: Number },
			no: { type: Number }
		}
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

GameSchema.index({ sport: 1});
GameSchema.index({ country: 1});
GameSchema.index({ league: 1});
GameSchema.index({ startTime: -1});
GameSchema.index({ updated: -1});
GameSchema.index({ homeTeamName: 1});
GameSchema.index({ awayTeamName: 1});
GameSchema.index({ seed: -1});
GameSchema.index({ status: 1});
mongoose.model('Game', GameSchema);
