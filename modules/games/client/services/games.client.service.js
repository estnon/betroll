'use strict';

//Games service used to communicate Games REST endpoints
angular.module('games').factory('Games',  ['$resource',
	function($resource) {
		return $resource('api/games/:gameId', { gameId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('games').directive('aGame', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/games/views/agame.html'
	};
});

angular.module('games').directive('aGameSide', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/games/views/agameside.html'
	};
});
angular.module('games').directive('bettedGame', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/games/views/bettedgame.html'
	};
});
angular.module('games').directive('quotedGame', function() {
	return {
		restrict: 'AE',
		templateUrl: 'modules/games/views/quotedgame.html'
	};
});

angular.module('games').service('live',  function () {
	return {
		updated : undefined,
		games : []
	};
});

angular.module('games').factory('livegames', ['$http', 'live', function($http, live) {
	return {
		check: function () {
			var livePromise = $http({
				url: 'api/games/live',
				method: 'GET',
				params: {updated: moment.tz(live.updated, 'Europe/Istanbul').unix()}
			});
			livePromise.success(function (data) {
				live.games = data;
			});
			livePromise.error(function () {
				live.games = [];
			});
		}
	};
}]);

