'use strict';

// Configuring the Games module
angular.module('games').run(['Menus',
	function(Menus) {
		// Add the Games dropdown item
		Menus.addMenuItem('topbar', {
			title: 21,
			state: 'games.list',
			position: 1
		});

	}
]);
