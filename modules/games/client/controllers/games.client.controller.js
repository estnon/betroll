'use strict';


// Games controller
angular.module('games').controller('GamesController', ['$scope', '$stateParams', '$location', '$http', 'Authentication',
	'Games', 'progressBar', 'langs', 'couponOps',
	function($scope, $stateParams, $location, $http, Authentication, Games, progressBar, langs, couponOps) {

		$scope.authentication = Authentication;

		$scope.querySport = 'Football';
		$scope.queryCountry = undefined;
		$scope.queryStatus = undefined;
		$scope.queryDate = 0;
		$scope.weekdays = [langs.lang_strings[55], langs.lang_strings[56], langs.lang_strings[127], langs.lang_strings[51], langs.lang_strings[52], langs.lang_strings[53], langs.lang_strings[54]];
		$scope.dateList = [{
			index: 0,
			name: langs.lang_strings[49]
		}, {
			index: 1,
			name: langs.lang_strings[50]
		}];

		$scope.countryList = undefined;
		$scope.leagueList = undefined;
		$scope.queryLeague = undefined;

		$scope.setCountry = function(country) {
			$scope.queryDate = undefined;
			$scope.queryLeague = undefined;
			$scope.queryCountry = country;
			$scope.find();
		};
		$scope.setStatus = function(status) {
			$scope.queryStatus = status;
			if (status === langs.lang_strings[59]) $scope.setDay(undefined);
			$scope.find();
		};
		$scope.setLeague = function(league) {
			$scope.queryDate = undefined;
			$scope.queryLeague = league;
			$scope.find();
		};

		$scope.times = function() {
			if ($scope.queryDate !== undefined) {
				var now = new Date();
				var dd = now.getDate();
				var mm = now.getMonth();
				var yyyy = now.getFullYear();
				var today = new Date(yyyy, mm, dd, 0, 0, 0);
				var todayPlusDays = new Date(today.getTime() + 24 * 60 * 60 * 1000 * $scope.queryDate);
				$scope.todayPlusDaysMoment = moment.tz(todayPlusDays, $scope.authentication.user.timezone).format();
				$scope.startDayBR = moment($scope.todayPlusDaysMoment).tz('Europe/Istanbul').format().split('T')[0];
			} else {
				$scope.startDayBR = undefined;
			}
		};
		$scope.setDay = function(day) {

			$scope.queryDate = day;
			$scope.times();
			$scope.find();
		};
		$scope.setSport = function(sport) {
			$scope.queryCountry = undefined;
			$scope.queryDate = undefined;
			$scope.queryLeague = undefined;
			$scope.querySport = sport;

			$scope.find();
		};
		$scope.getCountryList = function() {
			progressBar.status = 1;

			var countryListPromise = $http.post('/api/games/countries', { sport: $scope.querySport });
			countryListPromise.success(function(result) {
				$scope.countryList = result;
				progressBar.status = 0;
			});
			countryListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[138] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};
		$scope.getLeagueList = function() {
			if ($scope.queryCountry === undefined) {
				$scope.leagueList = '';
				return;
			}
			progressBar.status = 1;
			var leagueListPromise = $http.post('/api/games/leagues/', { country: $scope.queryCountry,
			sport: $scope.querySport });
			leagueListPromise.success(function(result) {
				$scope.leagueList = result;
				progressBar.status = 0;
			});
			leagueListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[139] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};
		$scope.buildDayList = function() {
			var today = new Date();
			var next = '';
			for (var i = 2; i < 11; i++) {
				if (i > 6) next = langs.lang_strings[109] + ' ';
				$scope.dateList[i] = {
					index: i,
					name: next + $scope.weekdays[(today.getDay() + i) % 7]
				};
			}
		};
		// Create new Game
		$scope.create = function() {
			// Create new Game object
			var game = new Games({
				vnumber: this.vnumber,
				rb_id: this.rb_id,
				sport: this.sport,
				country: this.country,
				league: this.league,
				startTime: this.startTime,
				homeTeamName: this.homeTeamName,
				awayTeamName: this.awayTeamName,
				score: this.score,
				result: this.result,
				status: this.status,
				odds: this.odds
			});

			// Redirect after save
			game.$save(function(response) {
				$location.path('games/' + response._id);

				// Clear form fields
				$scope.vnumber = '';
				$scope.rb_id = '';
				$scope.sport = '';
				$scope.country = '';
				$scope.league = '';
				$scope.startTime = '';
				$scope.homeTeamName = '';
				$scope.awayTeamName = '';
				$scope.score = [];
				$scope.result = [];
				$scope.status = '';
				$scope.odds = [];

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Game
		$scope.remove = function(game) {
			if (game) {
				game.$remove();

				for (var i in $scope.games) {
					if ($scope.games[i] === game) {
						$scope.games.splice(i, 1);
					}
				}
			} else {
				$scope.game.$remove(function() {
					$location.path('games');
				});
			}
		};

		// Update existing Game
		$scope.update = function() {
			var game = $scope.game;

			game.$update(function() {
				couponOps.pop('success', 'Success','Kaydedildi');
				$location.path('games');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
				couponOps.pop('error', 'Error', $scope.error);
			});
		};

		$scope.scores = function() {
			// Create new Game object
			var game = new Games({
				vnumber: this.vnumber,
				startTime: this.startTime,
				homeTeamName: this.homeTeamName,
				awayTeamName: this.awayTeamName,
				seed: this.seed,
				flag: this.flag,
				score: this.score,
				result: this.result,
				status: this.status
			});

			var scoresPromise = $http.post('/api/games/scores/', game);
			scoresPromise.success(function(result) {
				// Clear form fields
				$scope.vnumber = '';
				$scope.startTime = '';
				$scope.homeTeamName = '';
				$scope.awayTeamName = '';
				$scope.score = [];
				$scope.seed = '';
				$scope.flag = '';
				$scope.status = '';
			});
			scoresPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[140] + ' ' + langs.lang_strings[136]);
			});

		};
		$scope.init = function() {

			$scope.times();
			$scope.buildDayList();
			$scope.find();


		};
		// Find a list of Games
		$scope.find = function() {

			if (!$scope.authentication.user) {
				$location.path('/');
			} else {

				progressBar.status = 1;

				$scope.games = Games.query({
					sport: $scope.querySport,
					country: $scope.queryCountry,
					startTime: $scope.startDayBR,
					league: $scope.queryLeague,
					status: $scope.queryStatus
				}, function success() {
					progressBar.status = 0;
				}, function error() {
					progressBar.status = 0;
				});
				$scope.isCollapsed = true;
				$scope.getCountryList();
				$scope.getLeagueList();
			}
		};


		// Find existing Game
		$scope.findOne = function() {

			$scope.game = Games.get({
				gameId: $stateParams.gameId
			}, function success() {
				progressBar.status = 0;
			}, function error() {
				progressBar.status = 0;
			});
			$scope.isCollapsed = true;
		};

		// Toggle the menu items
		$scope.isCollapsedFilters = false;
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsedFilters = !$scope.isCollapsedFilters;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsedFilters = false;
		});
	}
]);
