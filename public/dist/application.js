'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'betroll';

	var applicationModuleVendorDependencies = ['ngResource', 'ui.router', 'ui.bootstrap', 'ui.utils', 'angularFileUpload', 'ngSanitize', 'ngAside', 'toaster'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.html5Mode(true).hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {

	var now =moment();
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');


'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('favs');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('followers');
'use strict';

// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('games');

'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('lists');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('notifications');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('posts');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);
'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', '$window', 'Authentication', 'Menus', 'ticker', 'tickerVars', '$interval', 'progressBar', '$aside', 'langs', 'loadLangs',
	function($scope, $state, $window, Authentication, Menus, ticker, tickerVars, $interval, progressBar, $aside, langs, loadLangs) {
		// Expose view variables
		$scope.$state = $state;
		$scope.authentication = Authentication;
		$scope.tickerVars = tickerVars;
		$scope.progressBar = progressBar;
		$scope.langs = langs;
		$scope.loadLangs = loadLangs;
		$scope.jumptoTop = function(){
			$window.scrollTo(0, 0);
		};
		$interval(function () {
			ticker.check();
		}, 20000);


		// Get the topbar menu
		$scope.menu = Menus.getMenu('topbar');


		// Toggle the menu items
		$scope.isCollapsed = false;
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.asideState = {
			open: false
		};

		$scope.openAside = function(position, backdrop, partialURL) {
			$scope.asideState = {
				open: true,
				position: position
			};

			function postClose() {
				$scope.asideState.open = false;
			}

			$aside.open({
				templateUrl: partialURL,
				placement: position,
				size: 'lg',
				backdrop: backdrop,
				controller: function($scope, $modalInstance) {
					$scope.ok = function(e) {
						$modalInstance.close();
						e.stopPropagation();
					};
					$scope.cancel = function(e) {
						$modalInstance.dismiss();
						e.stopPropagation();
					};
				}
			}).result.then(postClose, postClose);
		};

	}
]);



'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$location', 'langs',
	function($scope, Authentication, $location, langs) {
		$scope.langs =langs;


		// This provides Authentication context.
		$scope.authentication = Authentication;
		if ($scope.authentication.user) {
			$location.path('posts');
		}
	}
]);


'use strict';

angular.module('core').controller('PageController', ['$scope', '$state', 'headerVars',
    function($scope, $state, headerVars) {
        // Expose view variables
        $scope.$state = $state;
        $scope.headerVars = headerVars;
    }
]);



'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

    function() {



        // Define a set of default roles
        this.defaultRoles = ['*'];

        // Define the menus object
        this.menus = {};

        // A private function for rendering decision 
        var shouldRender = function(user) {
            if (user) {
                if (!!~this.roles.indexOf('*')) {
                    return true;
                } else {
                    for (var userRoleIndex in user.roles) {
                        for (var roleIndex in this.roles) {
                            if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
                                return true;
                            }
                        }
                    }
                }
            } else {
                return this.isPublic;
            }

            return false;
        };

        // Validate menu existance
        this.validateMenuExistance = function(menuId) {
            if (menuId && menuId.length) {
                if (this.menus[menuId]) {
                    return true;
                } else {
                    throw new Error('Menu does not exists');
                }
            } else {
                throw new Error('MenuId was not provided');
            }

            return false;
        };

        // Get the menu object by menu id
        this.getMenu = function(menuId) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Return the menu object
            return this.menus[menuId];
        };

        // Add new menu object by menu id
        this.addMenu = function(menuId, options) {
            options = options || {};

            // Create the new menu
            this.menus[menuId] = {
                isPublic: ((options.isPublic === null || typeof options.isPublic === 'undefined') ? true : options.isPublic),
                roles: options.roles || this.defaultRoles,
                items: options.items || [],
                shouldRender: shouldRender
            };

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeMenu = function(menuId) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Return the menu object
            delete this.menus[menuId];
        };

        // Add menu item object
        this.addMenuItem = function(menuId, options) {
            options = options || {};

            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Push new menu item
            this.menus[menuId].items.push({
                title: options.title || '',
                state: options.state || '',
                type: options.type || 'item',
                class: options.class,
                isPublic: ((options.isPublic === null || typeof options.isPublic === 'undefined') ? this.menus[menuId].isPublic : options.isPublic),
                roles: ((options.roles === null || typeof options.roles === 'undefined') ? this.menus[menuId].roles : options.roles),
                position: options.position || 0,
                items: [],
                shouldRender: shouldRender
            });

            // Add submenu items
            if (options.items) {
                for (var i in options.items) {
                	this.addSubMenuItem(menuId, options.link, options.items[i]);
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        // Add submenu item object
        this.addSubMenuItem = function(menuId, parentItemState, options) {
            options = options || {};

            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Search for menu item
            for (var itemIndex in this.menus[menuId].items) {
                if (this.menus[menuId].items[itemIndex].state === parentItemState) {
                    // Push new submenu item
                    this.menus[menuId].items[itemIndex].items.push({
                        title: options.title || '',
                        state: options.state|| '',
                        isPublic: ((options.isPublic === null || typeof options.isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : options.isPublic),
                        roles: ((options.roles === null || typeof options.roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : options.roles),
                        position: options.position || 0,
                        shouldRender: shouldRender
                    });
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeMenuItem = function(menuId, menuItemURL) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Search for menu item to remove
            for (var itemIndex in this.menus[menuId].items) {
                if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
                    this.menus[menuId].items.splice(itemIndex, 1);
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeSubMenuItem = function(menuId, submenuItemURL) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Search for menu item to remove
            for (var itemIndex in this.menus[menuId].items) {
                for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
                    if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
                        this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
                    }
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        //Adding the topbar menu
        this.addMenu('topbar', {
            isPublic: false
        });
    }
]);

angular.module('core').service('progressBar', function () {
    return {
        status: 0
    };
});

angular.module('core').service('headerVars', function () {
    return {
        title: 'dsfdfdsfsd'
    };
});

'use strict';

//Setting up route
angular.module('favs').config(['$stateProvider',
	function($stateProvider) {
		// Favs state routing
		$stateProvider.
		state('favs', {
			abstract: true,
			url: '/favs',
			template: '<ui-view/>'
		}).
		state('favs.list', {
			url: '',
			templateUrl: 'modules/favs/views/list-favs.client.view.html'
		}).
		state('favs.create', {
			url: '/create',
			templateUrl: 'modules/favs/views/create-fav.client.view.html'
		}).
		state('favs.view', {
			url: '/:favId',
			templateUrl: 'modules/favs/views/view-fav.client.view.html'
		}).
		state('favs.edit', {
			url: '/:favId/edit',
			templateUrl: 'modules/favs/views/edit-fav.client.view.html'
		});
	}
]);
'use strict';

// Favs controller
angular.module('favs').controller('FavsController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Favs', 'couponOps', 'notifyOps', 'favArrs', 'progressBar', 'langs',
	function($scope, $http, $stateParams, $location, Authentication, Favs, couponOps, notifyOps, favArrs, progressBar, langs) {
		$scope.authentication = Authentication;
		$scope.couponOps = couponOps;
		$scope.favArrs = favArrs;

		$scope.addToFavs = function(postid, userid) {
			// Create new Notification object
			var fav = new Favs ({
				post: postid
			});
			progressBar.status = 1;
			// Redirect after save
			fav.$save(function(response) {
				favArrs.isFaved[postid] = true;
				notifyOps.create(postid, userid, 'Faved', 'fav');
				progressBar.status = 0;
			}, function(errorResponse) {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

		$scope.removeFromFavs = function(postid) {
			progressBar.status = 1;
			var fav = {	postid: postid };

			var userFavsPromise = $http.post('api/favs/remove/', fav);
			userFavsPromise.success(function(data) {
				favArrs.isFaved[postid] = false;
				progressBar.status = 0;
			});
			userFavsPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.hide = function(favid) {

			var fav = new Favs ({
				_id: favid
			});
			progressBar.status = 1;
			var hidePromise = $http.post('api/favs/hide/', fav);
			hidePromise.success(function(data) {
				//favArrs.isFaved[favid] = true; // or'dan true cikmasi icin

				progressBar.status = 0;
			});
			hidePromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

	}
]);

'use strict';

//Favs service used to communicate Favs REST endpoints
angular.module('favs').factory('Favs', ['$resource',
	function($resource) {
		return $resource('api/favs/:favId', { favId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('favs').factory('favArrs', function () {
	return {
		FavList: {
			postIds: [],
			user: ''
		},
		isFaved: {
			post: ''
		}
	};
});




'use strict';

//Setting up route
angular.module('followers').config(['$stateProvider',
	function($stateProvider) {
		// Followers state routing
		$stateProvider.
		state('followers', {
			abstract: true,
			url: '/followers',
			template: '<ui-view/>'
		}).
		state('followers.list', {
			url: '',
			templateUrl: 'modules/followers/views/list-followers.client.view.html'
		});
	}
]);

'use strict';

// Followers controller
angular.module('followers').controller('FollowersController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Followers', 'couponOps', 'notifyOps', 'currentUser', 'progressBar', 'followArrs', 'langs',
	function($scope, $stateParams, $location, $http, Authentication, Followers, couponOps, notifyOps, currentUser, progressBar, followArrs, langs) {
		$scope.authentication = Authentication;
		$scope.couponOps = couponOps;
		$scope.currentUser = currentUser;
		$scope.followArrs = followArrs;
		// Create new Follower
		$scope.create = function(userid) {
			// Create new Follower object
			var follower = new Followers({
				followed: userid
			});
			progressBar.status = 1;
			// Redirect after save
			follower.$save(function(response) {
				if (userid === currentUser.user.id) {
					$scope.isFollowing = true;
				} else {
					followArrs.isFollowed[userid] = true;
				}
				notifyOps.create(undefined, userid, 'New follower', 'follow');
				progressBar.status = 0;
			}, function(errorResponse) {
				$scope.isFollowing = false;
				couponOps.pop('error', langs.lang_strings[137], langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

		$scope.unfollow = function(userid) {
			progressBar.status = 1;
			var followingCurrentPromise = $http.get('api/followers/unfollow/' + userid);
			followingCurrentPromise.success(function(result) {
				if (userid === currentUser.user.id) {
					$scope.isFollowing = false;
				} else {
					followArrs.isFollowed[userid] = false;
				}
				notifyOps.create(undefined, userid, 'Unfollowed', 'unfollow');
				progressBar.status = 0;
			});
			followingCurrentPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};



		// Find a list of Followers
		$scope.find = function() {
			$scope.followers = Followers.query();
		};

		// Find existing Follower
		$scope.findOne = function() {
			$scope.follower = Followers.get({
				followerId: $stateParams.followerId
			});
		};
	}
]);

'use strict';

//Followers service used to communicate Followers REST endpoints
angular.module('followers').factory('Followers', ['$resource',
	function($resource) {
		return $resource('api/followers/:followerId', { followerId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('followers').factory('followArrs', function () {
	return {
		FollowList: {
			userIds: []
		},
		isFollowed: {
			followed: ''
		}
	};
});

'use strict';

// Configuring the Games module
angular.module('games').run(['Menus',
	function(Menus) {
		// Add the Games dropdown item
		Menus.addMenuItem('topbar', {
			title: 21,
			state: 'games.list',
			position: 1
		});

	}
]);

'use strict';

//Setting up route
angular.module('games').config(['$stateProvider',
	function($stateProvider) {
		// Games state routing
		$stateProvider.
		state('games', {
			abstract: true,
			url: '/games',
			template: '<ui-view/>'
		}).
		state('games.list', {
			url: '',
			templateUrl: 'modules/games/views/list-games.client.view.html'
		}).
		state('games.create', {
			url: '/create',
			templateUrl: 'modules/games/views/create-game.client.view.html'
		}).
		state('games.view', {
			url: '/:gameId',
			templateUrl: 'modules/games/views/view-game.client.view.html'
		}).
		state('games.edit', {
			url: '/:gameId/edit',
			templateUrl: 'modules/games/views/edit-game.client.view.html'
		});
	}
]);

'use strict';


// Games controller
angular.module('games').controller('GamesController', ['$scope', '$stateParams', '$location', '$http', 'Authentication',
	'Games', 'couponOps', 'dateOps', 'progressBar', 'langs',
	function($scope, $stateParams, $location, $http, Authentication, Games, couponOps, dateOps, progressBar, langs) {

		$scope.authentication = Authentication;
		$scope.langs = langs;
		$scope.dateOps = dateOps;
		$scope.couponOps = couponOps;
		$scope.querySport = 'Football';
		$scope.queryCountry = undefined;
		$scope.queryStatus = undefined;
		$scope.queryDate = 0;
		$scope.weekdays = [$scope.langs.lang_strings[55], $scope.langs.lang_strings[56], $scope.langs.lang_strings[127], $scope.langs.lang_strings[51], $scope.langs.lang_strings[52], $scope.langs.lang_strings[53], $scope.langs.lang_strings[54]];
		$scope.dateList = [{
			index: 0,
			name: $scope.langs.lang_strings[49]
		}, {
			index: 1,
			name: $scope.langs.lang_strings[50]
		}];
		$scope.countryList = undefined;
		$scope.leagueList = undefined;
		$scope.queryLeague = undefined;

		$scope.setCountry = function(country) {
			$scope.queryDate = undefined;
			$scope.queryLeague = undefined;
			$scope.queryCountry = country;
			$scope.find();
		};
		$scope.setStatus = function(status) {
			$scope.queryStatus = status;
			if (status === $scope.langs.lang_strings[59]) $scope.setDay(undefined);
			$scope.find();
		};
		$scope.setLeague = function(league) {
			$scope.queryDate = undefined;
			$scope.queryLeague = league;
			$scope.find();
		};
		$scope.times = function() {
			if ($scope.queryDate !== undefined) {
				var now = new Date();
				var dd = now.getDate();
				var mm = now.getMonth();
				var yyyy = now.getFullYear();
				var today = new Date(yyyy, mm, dd, 0, 0, 0);
				var todayPlusDays = new Date(today.getTime() + 24 * 60 * 60 * 1000 * $scope.queryDate);
				$scope.todayPlusDaysMoment = moment.tz(todayPlusDays, $scope.authentication.user.timezone).format();
				$scope.startDayBR = moment($scope.todayPlusDaysMoment).tz('Europe/Istanbul').format().split('T')[0];
			} else {
				$scope.startDayBR = undefined;
			}
		};
		$scope.setDay = function(day) {

			$scope.queryDate = day;
			$scope.times();
			$scope.find();
		};
		$scope.setSport = function(sport) {
			$scope.queryCountry = undefined;
			$scope.queryDate = undefined;
			$scope.queryLeague = undefined;
			$scope.querySport = sport;

			$scope.find();
		};
		$scope.getCountryList = function() {
			progressBar.status = 1;

			var countryListPromise = $http.post('/api/games/countries', { sport: $scope.querySport });
			countryListPromise.success(function(result) {
				$scope.countryList = result;
				progressBar.status = 0;
			});
			countryListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[138] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};
		$scope.getLeagueList = function() {
			if ($scope.queryCountry === undefined) {
				$scope.leagueList = '';
				return;
			}
			progressBar.status = 1;
			var leagueListPromise = $http.post('/api/games/leagues/', { country: $scope.queryCountry,
			sport: $scope.querySport });
			leagueListPromise.success(function(result) {
				$scope.leagueList = result;
				progressBar.status = 0;
			});
			leagueListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[139] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};
		$scope.buildDayList = function() {
			var today = new Date();
			var next = '';
			for (var i = 2; i < 11; i++) {
				if (i > 6) next = langs.lang_strings[109] + ' ';
				$scope.dateList[i] = {
					index: i,
					name: next + $scope.weekdays[(today.getDay() + i) % 7]
				};
			}
		};
		// Create new Game
		$scope.create = function() {
			// Create new Game object
			var game = new Games({
				vnumber: this.vnumber,
				rb_id: this.rb_id,
				sport: this.sport,
				country: this.country,
				league: this.league,
				startTime: this.startTime,
				homeTeamName: this.homeTeamName,
				awayTeamName: this.awayTeamName,
				score: this.score,
				result: this.result,
				status: this.status,
				odds: this.odds
			});

			// Redirect after save
			game.$save(function(response) {
				$location.path('games/' + response._id);

				// Clear form fields
				$scope.vnumber = '';
				$scope.rb_id = '';
				$scope.sport = '';
				$scope.country = '';
				$scope.league = '';
				$scope.startTime = '';
				$scope.homeTeamName = '';
				$scope.awayTeamName = '';
				$scope.score = [];
				$scope.result = [];
				$scope.status = '';
				$scope.odds = [];

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Game
		$scope.remove = function(game) {
			if (game) {
				game.$remove();

				for (var i in $scope.games) {
					if ($scope.games[i] === game) {
						$scope.games.splice(i, 1);
					}
				}
			} else {
				$scope.game.$remove(function() {
					$location.path('games');
				});
			}
		};

		// Update existing Game
		$scope.update = function() {
			var game = $scope.game;

			game.$update(function() {
				couponOps.pop('success', 'Success','Kaydedildi');
				$location.path('games');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
				couponOps.pop('error', 'Error', $scope.error);
			});
		};

		$scope.scores = function() {
			// Create new Game object
			var game = new Games({
				vnumber: this.vnumber,
				startTime: this.startTime,
				homeTeamName: this.homeTeamName,
				awayTeamName: this.awayTeamName,
				seed: this.seed,
				flag: this.flag,
				score: this.score,
				result: this.result,
				status: this.status
			});

			var scoresPromise = $http.post('/api/games/scores/', game);
			scoresPromise.success(function(result) {
				// Clear form fields
				$scope.vnumber = '';
				$scope.startTime = '';
				$scope.homeTeamName = '';
				$scope.awayTeamName = '';
				$scope.score = [];
				$scope.seed = '';
				$scope.flag = '';
				$scope.status = '';
			});
			scoresPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[140] + ' ' + langs.lang_strings[136]);
			});

		};
		$scope.init = function() {

			$scope.times();
			$scope.buildDayList();
			$scope.find();


		};
		// Find a list of Games
		$scope.find = function() {

			if (!$scope.authentication.user) {
				$location.path('/');
			} else {

				progressBar.status = 1;

				$scope.games = Games.query({
					sport: $scope.querySport,
					country: $scope.queryCountry,
					startTime: $scope.startDayBR,
					league: $scope.queryLeague,
					status: $scope.queryStatus
				}, function success() {
					progressBar.status = 0;
				}, function error() {
					progressBar.status = 0;
				});
				$scope.isCollapsed = true;
				$scope.getCountryList();
				$scope.getLeagueList();
			}
		};


		// Find existing Game
		$scope.findOne = function() {

			$scope.game = Games.get({
				gameId: $stateParams.gameId
			}, function success() {
				progressBar.status = 0;
			}, function error() {
				progressBar.status = 0;
			});
			$scope.isCollapsed = true;
		};

		// Toggle the menu items
		$scope.isCollapsedFilters = false;
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsedFilters = !$scope.isCollapsedFilters;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsedFilters = false;
		});
	}
]);

'use strict';

//Games service used to communicate Games REST endpoints
angular.module('games').factory('Games',  ['$resource',
	function($resource) {
		return $resource('api/games/:gameId', { gameId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);





'use strict';

// Configuring the Lists module
angular.module('lists').run(['Menus',
	function(Menus) {
		// Add the Lists dropdown item
		Menus.addMenuItem('topbar', {
			title: 23,
			state: 'lists.list',
			position: 3
		});
	}
]);

'use strict';

//Setting up route
angular.module('lists').config(['$stateProvider',
	function($stateProvider) {
		// Lists state routing
		$stateProvider.
		state('lists', {
			abstract: true,
			url: '/shared',
			template: '<ui-view/>'
		}).
		state('lists.list', {
			url: '',
			templateUrl: 'modules/lists/views/list-lists.client.view.html'
		}).
		state('lists.create', {
			url: '/create',
			templateUrl: 'modules/lists/views/create-list.client.view.html'
		}).
		state('lists.view', {
			url: '/:listId',
			templateUrl: 'modules/lists/views/view-list.client.view.html'
		}).
		state('lists.edit', {
			url: '/:listId/edit',
			templateUrl: 'modules/lists/views/edit-list.client.view.html'
		});
	}
]);

'use strict';

// Lists controller
angular.module('lists').controller('ListsController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Lists',  'couponOps', 'notifyOps', 'favArrs', 'listArrs', 'dateOps', 'tickerVars', 'progressBar', 'langs',
	function($scope, $http, $stateParams, $location, Authentication, Lists, couponOps, notifyOps, favArrs, listArrs, dateOps, tickerVars, progressBar, langs) {
		$scope.authentication = Authentication;
		$scope.dateOps = dateOps;
		$scope.user = Authentication.user;
		$scope.couponOps = couponOps;
		$scope.listArrs = listArrs;
		$scope.langs = langs;


		$scope.addToLists = function (postid, userid) {
			progressBar.status = 1;
			//lists of user
			var userListsPromise = $http.get('api/lists/add/' + postid);
			userListsPromise.success(function(data) {
				listArrs.isShared[postid] = true;
				notifyOps.create(postid, userid, 'Shared', 'share');
				progressBar.status = 0;
			});
			userListsPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.removeFromLists = function (postid) {
			progressBar.status = 1;
			//lists of user
			var userListsPromise = $http.get('api/lists/remove/' + postid);
			userListsPromise.success(function(data) {
				listArrs.isShared[postid] = false;
				progressBar.status = 0;
			});
			userListsPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.hide = function(listid) {

			var list = new Lists ({
				_id: listid
			});
			progressBar.status = 1;
			var hidePromise = $http.post('api/lists/hide/', list);
			hidePromise.success(function(data) {
				//favArrs.isFaved[favid] = false;
				progressBar.status = 0;
			});
			hidePromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};


		// Find a list of Lists
		$scope.find = function() {

			if (!$scope.user) {
				$location.path('/');
			} else {
				progressBar.status = 1;
				if (typeof $scope.posts !== 'undefined') {
					$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
				} else {
					$scope.lastSeen = Date.now();
				}

				var postdata = {
					lastseen: $scope.lastSeen
				};

				// get posts of followings
				var timelinePromise = $http.post('/api/lists/list/all', postdata);
				timelinePromise.success(function (timelinedata) {
					progressBar.status = 0;

					if (typeof $scope.posts !== 'undefined') {
						$scope.posts = $scope.posts.concat(timelinedata);

					} else {
						$scope.posts = timelinedata;
					}
					$scope.posts.$resolved = true;
					tickerVars.Shared.time=Date.now();
					tickerVars.Shared.latest = '';

					favArrs.FavList.user = $scope.user._id;
					favArrs.FavList.postIds = [];
					favArrs.FavList.postIds = $scope.posts.map(function (e) {
						var key;
						for (key in e) // push postids
							return e.post[key];
					});

					$scope.checkIfFaved();


					listArrs.ListList.user = $scope.user._id;
					listArrs.ListList.postIds = [];
					listArrs.ListList.postIds = favArrs.FavList.postIds;

					$scope.checkIfShared();


				});
				timelinePromise.error(function () {
					couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
					progressBar.status = 0;
				});
			}
		};

		// Find existing List
		$scope.findOne = function() {
			$scope.list = Lists.get({
				listId: $stateParams.listId
			});
		};

		$scope.checkIfFaved = function () {
			progressBar.status = 1;
			var favListPromise = $http.post('/api/favs/timeline', favArrs.FavList);
			favListPromise.success(function (favListData) {
				var tempList = [];
				for (var i = 0; i < favListData.length; i++) {
					tempList[favListData[i].post] = true;
				}
				favArrs.isFaved = tempList;
				progressBar.status = 0;
			});
			favListPromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[142] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};

		$scope.checkIfShared = function () {
			progressBar.status = 1;
			var listListPromise = $http.post('/api/lists/timeline', listArrs.ListList);
			listListPromise.success(function (listListData) {
				progressBar.status = 0;
				var tempList = [];
				for (var i = 0; i < listListData.length; i++) {
					tempList[listListData[i].post] = true;
				}
				listArrs.isShared = tempList;

			});
			listListPromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[143] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};
	}
]);

'use strict';

//Lists service used to communicate Lists REST endpoints
angular.module('lists').factory('Lists', ['$resource',
	function($resource) {
		return $resource('api/lists/:listId', {
			listId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('lists').factory('listArrs', function() {
	return {
		ListList: {
			postIds: [],
			user: ''
		},
		isShared: {
			post: ''
		}
	};
});

'use strict';

// Configuring the Notifications module
angular.module('notifications').run(['Menus',
	function(Menus) {
		// Add the Notifications dropdown item
		Menus.addMenuItem('topbar', {
			title: 24,
			state: 'notifications.list',
			position: 4
		});
	}
]);

'use strict';

//Setting up route
angular.module('notifications').config(['$stateProvider',
	function($stateProvider) {
		// Notifications state routing
		$stateProvider.
		state('notifications', {
			abstract: true,
			url: '/notifications',
			template: '<ui-view/>'
		}).
		state('notifications.list', {
			url: '',
			templateUrl: 'modules/notifications/views/list-notifications.client.view.html'
		}).
		state('notifications.create', {
			url: '/create',
			templateUrl: 'modules/notifications/views/create-notification.client.view.html'
		});
	}
]);

'use strict';

// Notifications controller
angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications', 'dateOps', 'tickerVars', 'progressBar', 'langs',
	function($scope, $stateParams, $location, Authentication, Notifications, dateOps, tickerVars, progressBar, langs) {
		$scope.authentication = Authentication;
		$scope.dateOps = dateOps;
		$scope.notifFilter = undefined;
		$scope.langs = langs;
		$scope.notifTypes = { 'follow' : 74, 'share' : 75, 'mention' : 76, 'unfollow' : 77, 'fav' : 78};
		$scope.setFilter = function(filter) {
			$scope.notifFilter = filter;
			switch(filter) {
				case 'mention':
					$scope.notifFilterName = langs.lang_strings[70];
					break;
				case 'follow':
					$scope.notifFilterName = langs.lang_strings[71];
					break;
				case 'share':
					$scope.notifFilterName = langs.lang_strings[72];
					break;
				case 'fav':
					$scope.notifFilterName = langs.lang_strings[73];
					break;
				default:
					$scope.notifFilterName = langs.lang_strings[69];
			}

		};
		// Find a list of Notifications
		$scope.find = function() {

			if (!$scope.authentication.user) {
				$location.path('/');
			} else {
				progressBar.status = 1;
				$scope.notifications = Notifications.query(function success() {
					progressBar.status = 0;
				}, function error() {
					progressBar.status = 0;
				});
				tickerVars.Notifications.time=Date.now();
				tickerVars.Notifications.latest = '';
				$scope.setFilter();
			}
		};

		// Find existing Notification
		$scope.findOne = function() {
			$scope.notification = Notifications.get({ 
				notificationId: $stateParams.notificationId
			});
		};
	}
]);

'use strict';

//Notifications service used to communicate Notifications REST endpoints
angular.module('notifications').factory('Notifications', ['$resource',
	function($resource) {
		return $resource('api/notifications/:notificationId', { notificationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('notifications').factory('notifyOps', ['Notifications', function (Notifications) {

	return {
		create:  function(postid, notifyUserId, notifText, notifType) {
		// Create new Notification object
		var notification = new Notifications ({
			post: postid,
			notifyUser : notifyUserId,
			text : notifText,
			type: notifType
		});

		// Redirect after save
		notification.$save(function(response) {
			//do nothing
		});
	}


	};
}]);

'use strict';

// Configuring the Posts module
angular.module('posts').run(['Menus',
	function(Menus) {
		// Add the Posts dropdown item
		Menus.addMenuItem('topbar', {
			title: 22,
			state: 'posts.list',
			position: 2
		});

	}
]);

'use strict';

//Setting up route
angular.module('posts').config(['$stateProvider',
	function($stateProvider) {
		// Posts state routing
		$stateProvider.
			state('posts', {
				abstract: true,
				url: '/posts',
				template: '<ui-view/>'
			}).
			state('posts.list', {
				url: '',
				templateUrl: 'modules/posts/views/list-posts.client.view.html'
			}).
			state('posts.latest', {
				url: '/latest',
				templateUrl: 'modules/posts/views/latest-post.client.view.html'
			}).
			state('posts.search', {
				url: '/search',
				templateUrl: 'modules/posts/views/search-post.client.view.html'
			}).
			state('posts.create', {
				url: '/create',
				templateUrl: 'modules/posts/views/create-post.client.view.html'
			}).
			state('posts.missing', {
				url: '/missing',
				templateUrl: 'modules/posts/views/post-not-found.client.view.html'
			}).
			state('posts.view', {
				url: '/:postId',
				templateUrl: 'modules/posts/views/view-post.post.client.view.html'
			}).
			state('posts.thread', {
				url: '/thread/:postId',
				templateUrl: 'modules/posts/views/view-post.thread.client.view.html'
			});
	}
]);

'use strict';

// Posts controller
angular.module('posts').controller('PostsController', ['$scope', '$sce', '$stateParams', '$http', '$location', '$anchorScroll', 'Authentication', 'Posts',
	'couponOps', 'notifyOps', 'favArrs', 'listArrs', 'FileUploader', '$window', '$timeout', 'dateOps', 'tickerVars', 'progressBar', '$rootElement', 'langs',
	function($scope, $sce, $stateParams, $http, $location, $anchorScroll, Authentication, Posts, couponOps, notifyOps, favArrs, listArrs, FileUploader, $window, $timeout, dateOps, tickerVars, progressBar, $rootElement, langs) {

		$scope.authentication = Authentication;
		$scope.user = Authentication.user;
		$scope.dateOps = dateOps;
		$scope.progressBar = progressBar;
		$scope.searchTerm ='latest posts';
		$scope.langs = langs;
		// Create file uploader instance
		$scope.uploader = new FileUploader({
			url: 'api/posts/image'
		});

		$scope.autoExpand = function(e) {
			var element = typeof e === 'object' ? e.target : document.getElementById(e);
			var scrollHeight = element.scrollHeight - 0;
			element.style.height = scrollHeight + 'px';
			if (element.value.length < 48) document.getElementById('TextArea').style.height = 60 + 'px';
		};

		function expand() {
			$scope.autoExpand('TextArea');
		}

		// Set file uploader image filter
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function(item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|'.indexOf(type) !== -1;
			}
		});

		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function(fileItem) {

			if ($window.FileReader) {
				progressBar.status = 1;
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function(fileReaderEvent) {
					$timeout(function() {
						$scope.imageURL = fileReaderEvent.target.result;
					}, 0);
					progressBar.status = 0;
				};

			}
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
			// Show success message
			$scope.success = true;

			$scope.finalize(response.imageURL);

			// Clear upload buttons
			$scope.cancelUpload();

			progressBar.status = 0;
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();
			// Show error message
			$scope.error = response.message;
			couponOps.pop('error', langs.lang_strings[135], $scope.error);
			progressBar.status = 0;
		};

		// ...
		$scope.uploadImage = function() {
			// Clear messages
			$scope.success = $scope.error = null;

			// Start upload
			progressBar.status = 1;
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function() {
			$scope.uploader.clearQueue();
			$scope.imageURL = '';
		};

		$scope.couponOps = couponOps;

		$scope.quoteIDs = [];
		$scope.couponBuild = [];

		// Create new Post

		$scope.create = function() {
			progressBar.status = 1;
			if (!this.postText) {
				couponOps.pop('error', 'Hey!', langs.lang_strings[144]);
				progressBar.status = 0;
				return;
			}

			if ($scope.uploader.queue.length) {
				$scope.uploadImage();

			} else {
				$scope.finalize();

			}



			for (var y = 0; y < couponOps.GlobalQuotes.quotes.length; y++) {
				$scope.quoteIDs.push(couponOps.GlobalQuotes.quotes[y].gameID);
			}


			for (var t = 0; t < couponOps.GlobalCoupon.coupon.length; t++) {
				$scope.couponBuild.push({
					pickedGame: couponOps.GlobalCoupon.coupon[t].gameID,
					betTypes: couponOps.GlobalCoupon.coupon[t].betType,
					betTypesShort: couponOps.GlobalCoupon.coupon[t].betTypeShort
				});
			}


		};


		$scope.finalize = function(finalImageURL) {
			var regexp = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/i;
			var Url1 = this.postText.match(regexp);

			// Create new Post object
			var post = new Posts({
				postText: this.postText,
				imageURL: finalImageURL,
				quotedGames: this.quoteIDs,
				coupon: this.couponBuild,
				totalOdds: couponOps.GlobalCoupon.totalOdds,
				postIds: couponOps.ReplyingTo.postIds,
				usernames: couponOps.ReplyingTo.usernames,
				userIds:couponOps.ReplyingTo.userIds
			});

			if (Url1) { post.embedURL = Url1[0]; }

			// after save
			post.$save(function(response) {
				progressBar.status = 1;
				$scope.quoteIDs = [];
				$scope.couponBuild = [];
				$scope.postText = '';
				$scope.imageURL = '';
				couponOps.GlobalCoupon.coupon = [];
				couponOps.GlobalCoupon.totalOdds = 1;
				couponOps.GlobalQuotes.quotes = [];

				//notif
				couponOps.ReplyingTo.userIds.forEach(function(value) {
					if (value !== $scope.user._id) notifyOps.create(response._id, value, 'Mentioned', 'mention');
				});


				couponOps.unReplyTo();

				document.getElementById('TextArea').style.height = 60 + 'px';


				progressBar.status = 0;
				$location.path('posts/' + response._id);

			}, function(errorResponse) {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
				progressBar.status = 0;
			});

		};


		// Remove existing Post
		$scope.remove = function( post ) {
			if ( post ) { post.$remove();

				for (var i in $scope.posts ) {
					if ($scope.posts [i] === post ) {
						$scope.posts.splice(i, 1);
					}
				}
			} else {
				$scope.post.$remove(function() {
					$location.path('posts');
				});
			}

		};


		// Find a list of Posts
		$scope.find = function() {

			if (!$scope.user) {
				$location.path('/');
			} else {
				progressBar.status = 1;
			if (typeof $scope.posts !== 'undefined') {
				$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
			} else {
				$scope.lastSeen = Date.now();
			}

			var postdata = {
				lastseen: $scope.lastSeen
			};

			var timelinePromise = $http.post('/api/posts/timeline', postdata);
			timelinePromise.success(function (timelinedata) {

				if (typeof $scope.posts !== 'undefined') {
					$scope.posts = $scope.posts.concat(timelinedata);
				} else {
					$scope.posts = timelinedata;
				}

				tickerVars.Timeline.time=Date.now();
				tickerVars.Timeline.latest = '';

				favArrs.FavList.user = $scope.user._id;
				favArrs.FavList.postIds = [];
				favArrs.FavList.postIds = $scope.posts.map(function (e) {
					var key;
					for (key in e) // push postids
						return e[key];
				});

				$scope.checkIfFaved();


				listArrs.ListList.user = $scope.user._id;
				listArrs.ListList.postIds = [];
				listArrs.ListList.postIds = favArrs.FavList.postIds;

				$scope.checkIfShared();
				progressBar.status = 0;

			});
			timelinePromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});
		}
		};

		$scope.search = function(more) {
			if ($scope.searchTerm.length <3 ){
				couponOps.pop('error', langs.lang_strings[145], langs.lang_strings[146]);
				return;
			}
			progressBar.status = 1;
			if (more === true) {
				$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
			} else {
				$scope.lastSeen = Date.now();
				$scope.posts = '';
			}

			var postdata = {
				searchterm: $scope.searchTerm,
				lastseen: $scope.lastSeen
			};

			var timelinePromise = $http.post('/api/posts/search', postdata);
			timelinePromise.success(function (timelinedata) {

				if (more === true) {
					$scope.posts = $scope.posts.concat(timelinedata);
				} else {
					$scope.posts = timelinedata;
				}
				//$scope.embedly(timelinedata);

				if (!timelinedata.length) { $scope.noResults = true;} else {$scope.noResults = false;}
				progressBar.status = 0;
			});
			timelinePromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[147] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

		// Find existing Post
		$scope.findThread = function() {
			progressBar.status = 1;
			$scope.activePost = $stateParams.postId;
			var threadPromise = $http.get('/api/posts/thread/' + $scope.activePost);
			threadPromise.success(function(data) {
				$scope.posts = data;
				//$scope.embedly(data);
				favArrs.FavList.user = $scope.user._id;
				favArrs.FavList.postIds = [];
				favArrs.FavList.postIds = $scope.posts.map(function(e) {
					var key;
					for (key in e) // push postids
						return e[key];
				});


				$scope.checkIfFaved();


				listArrs.ListList.user = $scope.user._id;
				listArrs.ListList.postIds = [];
				listArrs.ListList.postIds = favArrs.FavList.postIds;


				$scope.checkIfShared();

				$location.hash($scope.activePost);
				$anchorScroll();
				progressBar.status = 0;

			});
			threadPromise.error(function() {
				progressBar.status = 0;
				$location.path('posts/missing');
			});

		};


		$scope.checkIfFaved = function() {
			var favListPromise = $http.post('/api/favs/timeline', favArrs.FavList);
			favListPromise.success(function(favListData) {
				var tempList = [];
				for (var i = 0; i < favListData.length; i++) {
					tempList[favListData[i].post] = true;
				}
				favArrs.isFaved = tempList;

			});
			favListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[142] + ' ' + langs.lang_strings[136]);
			});

		};

		$scope.checkIfShared = function() {
			var listListPromise = $http.post('/api/lists/timeline', listArrs.ListList);
			listListPromise.success(function(listListData) {
				var tempList = [];
				for (var i = 0; i < listListData.length; i++) {
					tempList[listListData[i].post] = true;
				}
				listArrs.isShared = tempList;

			});
			listListPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[143] + ' ' + langs.lang_strings[136]);
			});

		};

		$scope.findOne = function() {
			progressBar.status = 1;
			$scope.post = Posts.get({
				postId: $stateParams.postId
			},
				undefined,
				function(){
					$scope.favCount();
					$scope.shareCount();

					favArrs.FavList.user = $scope.user._id;
					favArrs.FavList.postIds = [];
					favArrs.FavList.postIds.push($stateParams.postId);

					$scope.checkIfFaved();


					listArrs.ListList.user = $scope.user._id;
					listArrs.ListList.postIds = [];
					listArrs.ListList.postIds = favArrs.FavList.postIds;


					$scope.checkIfShared();
					progressBar.status = 0;
				},
				function() {
					progressBar.status = 0;
					$location.path('posts/missing');
				});


		};

		$scope.favCount = function() {
			//fav count
			var favCountCurrentPromise = $http.get('/api/favs/count/of/post/' + $stateParams.postId);
			favCountCurrentPromise.success(function(result) {
				$scope.favCountOfPost = result;
			});
			favCountCurrentPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
			});
		};

		$scope.shareCount = function() {
			//share count
			var shareCountCurrentPromise = $http.get('/api/lists/count/of/post/' + $stateParams.postId);
			shareCountCurrentPromise.success(function(result) {
				$scope.shareCountOfPost = result;
			});
			shareCountCurrentPromise.error(function() {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
			});
		};

		$scope.gameposts = function(more) {

			progressBar.status = 1;
			if (more === true) {
				$scope.lastSeen = $scope.posts[$scope.posts.length - 1].created;
			} else {
				$scope.lastSeen = Date.now();
				$scope.posts = '';
			}

			var postdata = {
				gameid: $stateParams.gameId,
				lastseen: $scope.lastSeen
			};

			var timelinePromise = $http.post('/api/posts/gameposts', postdata);
			timelinePromise.success(function (timelinedata) {

				if (more === true) {
					$scope.posts = $scope.posts.concat(timelinedata);
				} else {
					$scope.posts = timelinedata;
				}
				//$scope.embedly(timelinedata);

				if (!timelinedata.length) { $scope.noResults = true;} else {$scope.noResults = false;}
				progressBar.status = 0;
			});
			timelinePromise.error(function () {
				couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
				progressBar.status = 0;
			});
		};

	}
]);

'use strict';

//Posts service used to communicate Posts REST endpoints
angular.module('posts').factory('Posts', ['$resource',
	function($resource) {
		return $resource('api/posts/:postId', { postId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);


angular.module('posts').directive('fillHeight', ['$window', '$document', function ($window, $document) {
	return {
		restrict: 'A',
		scope: {
			footerElementId: '@',
			additionalPadding: '@'
		},
		link: function (scope, element, attrs) {

			function onWindowResize(){
				var footerElement = angular.element($document[0].getElementById(scope.footerElementId));
				var footerElementHeight;

				if (footerElement.length === 1) {
					footerElementHeight = footerElement[0].offsetHeight + getTopMarginAndBorderHeight(footerElement) + getBottomMarginAndBorderHeight(footerElement);
				} else {
					footerElementHeight = 0;
				}

				var elementOffsetTop = element[0].offsetTop;
				var elementBottomMarginAndBorderHeight = getBottomMarginAndBorderHeight(element);

				var additionalPadding = scope.additionalPadding || 0;

				var elementHeight = $window.innerHeight - elementOffsetTop - elementBottomMarginAndBorderHeight	- footerElementHeight - additionalPadding;

				//console.log(elementHeight);
				element.css('height', elementHeight + 'px');
			}

			function getTopMarginAndBorderHeight(element) {
				var footerTopMarginHeight = getCssNumeric(element, 'margin-top');
				var footerTopBorderHeight = getCssNumeric(element, 'border-top-width');
				return footerTopMarginHeight + footerTopBorderHeight;
			}

			function getBottomMarginAndBorderHeight(element) {
				var footerBottomMarginHeight = getCssNumeric(element, 'margin-bottom');
				var footerBottomBorderHeight = getCssNumeric(element, 'border-bottom-width');
				return footerBottomMarginHeight + footerBottomBorderHeight;
			}

			function getCssNumeric(element, propertyName) {
				return parseInt(element.css(propertyName), 10) || 0;
			}

			angular.element($window).on('resize', onWindowResize);
			onWindowResize();
		}
	};
}]);

angular.module('posts')
	.filter('highlightWords', function () {
		return function (text, search) {
			if (text && (search || angular.isNumber(search))) {
				text = text.toString();
				search = search.toString();

				angular.forEach(search.split(/\s+/), function(word) {

					if (['span', 'class', 'ui-match'].indexOf(word) < 0) {

						var pattern = new RegExp('\\b' + word + '\\b', 'gi');
						text = text.replace(pattern, '<strong class="highlight ui-match">$&</strong>');
					}
				});
			}
			return text;
		};
	});

angular.module('posts').directive('showEmbed', ['$http', '$sce', function($http, $sce)
{
	return {
		restrict: 'E',
		scope:
		{
			embedUrl: '@'
		},
		link: function(scope, element, attrs)
		{
			scope.$watch('embedUrl', function(value)
			{
				if (value)
				{
					var threadPromise = $http.get('http://api.embed.ly/1/oembed?url=' + attrs.embedUrl + '&key=314af94fce4149d384ed211ac1cce22b');
					threadPromise.success(function(data)
					{
						var embedWrapper = '';

						if (data.type === 'link') {
							embedWrapper += '<a href="' + data.url + '" target="_blank">';
						}

						embedWrapper += '<br /><div class="embedded">';

						if (data.title) {
							embedWrapper += '<strong class="text-info">' + data.title + '</strong>';
						}
						if (data.description) {
							embedWrapper += '<div class="text-muted">' + data.description + '</div>';
						}

						if (data.type !== 'link' && data.html) {
							embedWrapper += '<div class="responsive-object">' + data.html + '</div>';
						}
						if (data.type === 'photo') {
							embedWrapper += '<div class="responsive-object"><a href="' + data.url + '" target="_blank"><img class="img-thumbnail post-image" src="' + data.url + '"></a></div>';
						}

						embedWrapper += '</div>';

						if (data.type === 'link') {
							embedWrapper += '</a>';
						}

						element.html(embedWrapper);

					});
					threadPromise.error(function()
					{
						element.html = '';
					});
				}
			});
		}
	};
}]);

'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function ($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function ($q, $location, Authentication) {
				return {
					responseError: function (rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);

'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function ($stateProvider) {
		// Users state routing
		$stateProvider.
			state('settings', {
				abstract: true,
				url: '/settings',
				templateUrl: 'modules/users/views/settings/settings.client.view.html'
			}).
			state('settings.profile', {
				url: '/profile',
				templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
			}).
			state('settings.password', {
				url: '/password',
				templateUrl: 'modules/users/views/settings/change-password.client.view.html'
			}).
			state('settings.accounts', {
				url: '/accounts',
				templateUrl: 'modules/users/views/settings/manage-social-accounts.client.view.html'
			}).
			state('settings.picture', {
				url: '/picture',
				templateUrl: 'modules/users/views/settings/change-profile-picture.client.view.html'
			}).
			state('authentication', {
				abstract: true,
				url: '/authentication',
				templateUrl: 'modules/users/views/authentication/authentication.client.view.html'
			}).
			state('authentication.signup', {
				url: '/signup',
				templateUrl: 'modules/users/views/authentication/signup.client.view.html'
			}).
			state('authentication.signin', {
				url: '/signin',
				templateUrl: 'modules/users/views/authentication/signin.client.view.html'
			}).
			state('authentication.tos', {
				url: '/tos',
				templateUrl: 'modules/users/views/authentication/tos.client.view.html'
			}).
			state('authentication.privacy', {
				url: '/privacy',
				templateUrl: 'modules/users/views/authentication/privacy.client.view.html'
			}).
			state('password', {
				abstract: true,
				url: '/password',
				template: '<ui-view/>'
			}).
			state('password.forgot', {
				url: '/forgot',
				templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
			}).
			state('password.reset', {
				abstract: true,
				url: '/reset',
				template: '<ui-view/>'
			}).
			state('password.reset.invalid', {
				url: '/invalid',
				templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
			}).
			state('password.reset.success', {
				url: '/success',
				templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
			}).
			state('password.reset.form', {
				url: '/:token',
				templateUrl: 'modules/users/views/password/reset-password.client.view.html'
			}).
			state('users', {
				abstract: true,
				url: '/users',
				templateUrl: 'modules/users/views/view-profile.client.view.html'
			}).
			state('users.user', {
				url: '/:username',
				templateUrl: 'modules/users/views/view-profile.posts.client.view.html'
			}).
			state('users.favs', {
				url: '/:username/favs',
				templateUrl: 'modules/users/views/view-profile-favs.client.view.html'
			}).
			state('users.shares', {
				url: '/:username/shares',
				templateUrl: 'modules/users/views/view-profile-shares.client.view.html'
			}).
			state('users.following', {
				url: '/:username/following',
				templateUrl: 'modules/users/views/view-profile-following.client.view.html'
			}).
			state('users.followers', {
				url: '/:username/followers',
				templateUrl: 'modules/users/views/view-profile-followers.client.view.html'
			}).
			state('people', {
				url: '/people',
				templateUrl: 'modules/users/views/people.client.view.html'
			});
	}
]);

'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', 'ticker','tickerVars', 'progressBar', '$aside', 'langs', 'loadLangs',
	function($scope, $http, $location, Authentication, ticker, tickerVars, progressBar, $aside, langs, loadLangs) {
		$scope.authentication = Authentication;
		$scope.tickerVars = tickerVars;
		$scope.langs = langs;

		if ($scope.authentication.user) {
			loadLangs.load($scope.authentication.user.language);
		} else {
			loadLangs.load($scope.langs.current_lang || 'en');
		}


		$scope.openAside = function(position, backdrop, partialUrl) {
			$scope.asideState = {
				open: true,
				position: position
			};

			function postClose() {
				$scope.asideState.open = false;
			}

			$aside.open({
				templateUrl: partialUrl,
				placement: position,
				size: 'lg',
				backdrop: backdrop,
				controller: function($scope, $modalInstance) {
					$scope.ok = function(e) {
						$modalInstance.close();
						e.stopPropagation();
					};
					$scope.cancel = function(e) {
						$modalInstance.dismiss();
						e.stopPropagation();
					};
				}
			}).result.then(postClose, postClose);
		};





		$scope.tzdetect = {
			names: moment.tz.names(),
			matches: function(base){
				var results = [], now = Date.now(), makekey = function(id){
					return [0, 4, 8, -5*12, 4-5*12, 8-5*12, 4-2*12, 8-2*12].map(function(months){
						var m = moment(now + months*30*24*60*60*1000);
						if (id) m.tz(id);
						return m.format('DDHHmm');
					}).join(' ');
				}, lockey = makekey(base);
				$scope.tzdetect.names.forEach(function(id){
					if (makekey(id)===lockey) results.push(id);
				});
				return results;
			}
		};


		$scope.signup = function() {
			if (typeof $scope.credentials !== 'undefined') {
				progressBar.status = 1;
				$scope.credentials.timezone = $scope.tzdetect.matches()[0];
				$scope.credentials.language = langs.current_lang || 'en';
				$http.post('/api/auth/signup', $scope.credentials).success(function (response) {
					// If successful we assign the response to the global user model

					$scope.authentication.user = response;
					progressBar.status = 0;
					// And redirect to the index page
					$location.path('settings/profile');
				}).error(function (response) {
					$scope.error = response.message;

					progressBar.status = 0;
				});
			} else {
				$scope.error = 'Please fill out the sign up form.';
			}
		};

		$scope.signin = function() {
			progressBar.status = 1;
			$http.post('/api/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				loadLangs.load($scope.authentication.user.language);

				ticker.check();
				progressBar.status = 0;
				// And redirect to the index page
				$location.path('/posts');
			}).error(function(response) {
				$scope.error = response.message;

				progressBar.status = 0;
			});
		};



	}
]);

'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'progressBar', 'langs',
	function($scope, $stateParams, $http, $location, Authentication, progressBar, langs) {
		$scope.authentication = Authentication;
		$scope.progressBar = progressBar;
		$scope.langs = langs;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;
			progressBar.status = 1;
			$http.post('/api/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;
				progressBar.status = 0;
			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
				progressBar.status = 0;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;
			progressBar.status = 1;
			$http.post('/api/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;
				progressBar.status = 0;
				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
				progressBar.status = 0;
			});
		};
	}
]);

/**
 * Created by ugur on 20.02.2015.
 */
'use strict';

angular.module('users').controller('PeopleController', ['$scope', '$state', '$http', '$location', 'Users', 'Authentication', '$stateParams', 'dateOps', 'couponOps', 'progressBar', 'followArrs', 'langs',
    function($scope, $state, $http, $location, Users, Authentication, $stateParams, dateOps, couponOps, progressBar, followArrs, langs) {
        $scope.user = Authentication.user;
        $scope.dateOps = dateOps;
        $scope.searchTerm = 'newest users';
        $scope.langs = langs;

        $scope.search = function(more) {
            if ($scope.searchTerm.length <3 ){
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[145] + ' ' + langs.lang_strings[146]);
                return;
            }
            progressBar.status = 1;
            if (more === true) {
                $scope.lastSeen = $scope.people[$scope.people.length - 1].created;
            } else {
                $scope.lastSeen = Date.now();
                $scope.people = '';
            }
            var peoplePromise = $http.post('/api/users/search/', {searchterm : $scope.searchTerm, lastseen: $scope.lastSeen});
            peoplePromise.success(function(result) {

                if (more === true) {
                    $scope.people = $scope.people.concat(result);
                } else {
                    $scope.people = result;
                }
                followArrs.FollowList.userIds = [];
                followArrs.FollowList.userIds = $scope.people.map(function(e) {
                    var key;

                    for (key in e) // push postids
                        return e[key];
                });

                $scope.checkIfFollowed();
                if (!result.length) { $scope.noResults = true;} else {$scope.noResults = false;}
                progressBar.status = 0;
            });
            peoplePromise.error(function() {
                progressBar.status = 0;
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
            });
        };
        $scope.checkIfFollowed = function() {
            var followsPromise = $http.post('/api/followers/timeline/', followArrs.FollowList);
            followsPromise.success(function(listFollowData) {
                var tempList = [];
                for (var i = 0; i < listFollowData.length; i++) {
                    tempList[listFollowData[i].followed] = true;
                }
                followArrs.isFollowed = tempList;

            });
            followsPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[147] + ' ' + langs.lang_strings[136]);
            });

        };

    }
]);

'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'progressBar',
	function($scope, $http, $location, Users, Authentication, progressBar) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/api/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
				progressBar.status = 1;
				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
					progressBar.status = 0;
				}, function(response) {
					$scope.error = response.data.message;
					progressBar.status = 0;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;
			progressBar.status = 1;
			$http.post('/api/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
				progressBar.status = 0;
			}).error(function(response) {
				$scope.error = response.message;
				progressBar.status = 0;
			});
		};
	}
]);

'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'progressBar', 'langs',
	function($scope, $http, $location, Users, Authentication, progressBar, langs) {
		$scope.user = Authentication.user;

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;
			progressBar.status = 1;
			$http.post('/api/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.addAlert({
					type: 'success',
					msg: langs.lang_strings[179]
				});
				$scope.passwordDetails = null;
				progressBar.status = 0;
			}).error(function(response) {
				$scope.error = response.message;
				progressBar.status = 0;
			});
		};
	}
]);

'use strict';

angular.module('users').controller('ChangeProfilePictureController', ['$scope', '$timeout', '$window', 'Authentication', 'FileUploader', 'progressBar', 'langs',
	function ($scope, $timeout, $window, Authentication, FileUploader, progressBar, langs) {
		$scope.user = Authentication.user;
		$scope.imageURL = $scope.user.profileImageURL;

		// Create file uploader instance
		$scope.uploader = new FileUploader({
			url: 'api/users/picture'
		});

		// Set file uploader image filter
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|'.indexOf(type) !== -1;
			}
		});

		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function (fileItem) {
			if ($window.FileReader) {
				progressBar.status = 1;
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function (fileReaderEvent) {
					$timeout(function () {
						$scope.imageURL = fileReaderEvent.target.result;
					}, 0);
					progressBar.status = 0;
				};
			}
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
			// Show success message
			$scope.success = true;
			$scope.addAlert({
				type: 'success',
				msg: langs.lang_strings[108]
			});
			progressBar.status = 0;
			// Populate user object
			$scope.user = Authentication.user = response;

			// Clear upload buttons
			$scope.cancelUpload();
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();
			progressBar.status = 0;
			// Show error message
			$scope.error = response.message;
		};

		// Change user profile picture
		$scope.uploadProfilePicture = function () {
			// Clear messages
			$scope.success = $scope.error = null;

			// Start upload
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function () {
			$scope.uploader.clearQueue();
			$scope.imageURL = $scope.user.profileImageURL;
		};
	}
]);

'use strict';

angular.module('users').controller('EditProfileController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'progressBar', 'couponOps', 'loadLangs', 'langs',
	function($scope, $http, $location, Users, Authentication, progressBar, couponOps, loadLangs, langs) {
		$scope.user = Authentication.user;

		$scope.tzList = moment.tz.names();

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
				progressBar.status = 1;
				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
					loadLangs.load(Authentication.user.language);
					couponOps.pop('success', '', langs.lang_strings[107]);
					progressBar.status = 0;
				}, function(response) {
					$scope.error = response.data.message;
					couponOps.pop('error', langs.lang_strings[135], $scope.error);
					progressBar.status = 0;
				});
			} else {
				$scope.submitted = true;
			}
		};
	}
]);

'use strict';

angular.module('users').controller('SocialAccountsController', ['$scope', '$http', '$location', 'Users', 'Authentication',
	function($scope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// Check if there are additional accounts
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/api/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);

'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'langs',
	function($scope, $http, $location, Users, Authentication, langs) {
		$scope.user = Authentication.user;
		$scope.langs = langs;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');
	}
]);

/**
 * Created by ugur on 20.02.2015.
 */
'use strict';

angular.module('users').controller('ViewProfileController', ['$scope', '$state', '$http', '$location', 'Users', 'Authentication', '$stateParams', 'couponOps', 'favArrs', 'listArrs', 'followArrs', 'currentUser', 'ourUser', 'dateOps', 'progressBar', 'langs',
    function($scope, $state, $http, $location, Users, Authentication, $stateParams, couponOps, favArrs, listArrs, followArrs, currentUser, ourUser, dateOps, progressBar, langs) {
        $scope.user = Authentication.user;
        $scope.dateOps = dateOps;
        $scope.couponOps = couponOps;
        $scope.currentUser = currentUser;
        $scope.ourUser = ourUser;
        $scope.langs = langs;
        
        $scope.findPosts = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    if (typeof $scope.userPosts !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userPosts[$scope.userPosts.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }
                    //posts of user

                    var postData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    var userPostsPromise = $http.post('/api/posts/of/', postData);
                    userPostsPromise.success(function(data) {
                        if (typeof $scope.userPosts !== 'undefined' && loadmore === true) {
                            $scope.userPosts = $scope.userPosts.concat(data);
                        } else {
                            $scope.userPosts = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };


                        favArrs.FavList.user = $scope.user._id;
                        favArrs.FavList.postIds = [];
                        favArrs.FavList.postIds = $scope.userPosts.map(function(e) {
                            var key;
                            for (key in e) // push postids
                                return e[key];
                        });

                        $scope.checkIfFaved();

                        listArrs.ListList.user = $scope.user._id;
                        listArrs.ListList.postIds = [];
                        listArrs.ListList.postIds = favArrs.FavList.postIds;

                        $scope.checkIfShared();
                        progressBar.status = 0;

                    });
                    userPostsPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });
                });

        };

        $scope.findFavs = function(loadmore) {

            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    progressBar.status = 1;
                    if (typeof $scope.userFavs !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userFavs[$scope.userFavs.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var favData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    //favs of user
                    var userFavsPromise = $http.post('api/favs/list/of/', favData);
                    userFavsPromise.success(function(data) {
                        if (typeof $scope.userFavs !== 'undefined' && loadmore === true) {
                            $scope.userFavs = $scope.userFavs.concat(data);
                        } else {
                            $scope.userFavs = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };

                        favArrs.FavList.user = $scope.user._id;
                        favArrs.FavList.postIds = [];
                        favArrs.FavList.postIds = $scope.userFavs.map(function(e) {
                            var key;
                            for (key in e.post) // push postids
                                return e.post[key];
                        });

                        $scope.checkIfFaved();


                        listArrs.ListList.user = $scope.user._id;
                        listArrs.ListList.postIds = [];
                        listArrs.ListList.postIds = favArrs.FavList.postIds;


                        $scope.checkIfShared();
                        progressBar.status = 0;

                    });
                    userFavsPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                    });

                });
        };

        $scope.findShares = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;
            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {


                    if (typeof $scope.userShares !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userShares[$scope.userShares.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var shareData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    //shares of user
                    var userSharesPromise = $http.post('api/lists/list/of/', shareData);
                    userSharesPromise.success(function(data) {
                        if (typeof $scope.userShares !== 'undefined' && loadmore === true) {
                            $scope.userShares = $scope.userShares.concat(data);
                        } else {
                            $scope.userShares = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };

                        favArrs.FavList.user = $scope.user._id;
                        favArrs.FavList.postIds = [];
                        favArrs.FavList.postIds = $scope.userShares.map(function(e) {
                            var key;
                            for (key in e.post) // push postids
                                return e.post[key];
                        });

                        $scope.checkIfFaved();


                        listArrs.ListList.user = $scope.user._id;
                        listArrs.ListList.postIds = [];
                        listArrs.ListList.postIds = favArrs.FavList.postIds;


                        $scope.checkIfShared();
                        progressBar.status = 0;
                    });
                    userSharesPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });

                });
        };


        $scope.findFollowing = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    if (typeof $scope.userFollowing !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userFollowing[$scope.userFollowing.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var followingData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };


                    //followings of user
                    var followingsPromise = $http.post('api/followers/following/list/of/', followingData);
                    followingsPromise.success(function(data) {
                        if (typeof $scope.userFollowing !== 'undefined' && loadmore === true) {
                            $scope.userFollowing = $scope.userFollowing.concat(data);
                        } else {
                            $scope.userFollowing = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };


                        followArrs.FollowList.userIds = [];
                        followArrs.FollowList.userIds = $scope.userFollowing.map(function(e) {
                            var key;
                            for (key in e) // push postids
                                return e.followed._id;
                        });
                        $scope.checkIfFollowed();
                        progressBar.status = 0;
                    });
                    followingsPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[141] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });

                });

        };

        $scope.findFollowers = function(loadmore) {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    if (typeof $scope.userFollowers !== 'undefined' && loadmore === true) {
                        $scope.lastSeen = $scope.userFollowers[$scope.userFollowers.length - 1].created;
                    } else {
                        $scope.lastSeen = Date.now();
                    }

                    var followersData = {
                        userid: $scope.ourUser._id,
                        lastseen: $scope.lastSeen
                    };

                    //followers of user

                    var followersPromise = $http.post('api/followers/list/of/', followersData);
                    followersPromise.success(function(data) {
                        if (typeof $scope.userFollowers !== 'undefined' && loadmore === true) {
                            $scope.userFollowers = $scope.userFollowers.concat(data);
                        } else {
                            $scope.userFollowers = data;
                        }

                        $scope.currentUser.user = {
                            id: ourUser._id
                        };
                        followArrs.FollowList.userIds = [];
                        followArrs.FollowList.userIds = $scope.userFollowers.map(function(e) {
                            var key;
                            for (key in e) // push postids
                                return e.user._id;
                        });
                        $scope.checkIfFollowed();

                        progressBar.status = 0;
                    });
                    followersPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[148] + ' ' + langs.lang_strings[136]);
                        progressBar.status = 0;
                    });

                });
        };

        $scope.findUser = function() {
            progressBar.status = 1;
            $scope.state = $state.current.name;

            $scope.getOurUser = Users.get({
                username: $stateParams.username
            }).$promise.then(function(ourUser) {

                    $scope.ourUser = ourUser;

                    if ($scope.user) {
                        //is followed by user
                        var followingCurrentPromise = $http.get('api/followers/of/' + ourUser._id);
                        followingCurrentPromise.success(function(result) {
                            $scope.isFollowing = result;
                        });
                        followingCurrentPromise.error(function() {
                            couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                        });

                        //is following our user
                        var followingUsPromise = $http.get('api/followers/following/us/' + ourUser._id);
                        followingUsPromise.success(function(result) {
                            $scope.isFollowingUs = result;
                        });
                        followingUsPromise.error(function() {
                            couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                        });
                    }

                    //post count
                    var postCountCurrentPromise = $http.get('/api/posts/count/' + ourUser._id);
                    postCountCurrentPromise.success(function(result) {
                        $scope.postCount = result;
                    });
                    postCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //fav count
                    var favCountCurrentPromise = $http.get('/api/favs/count/' + ourUser._id);
                    favCountCurrentPromise.success(function(result) {
                        $scope.favCount = result;
                    });
                    favCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //share count
                    var shareCountCurrentPromise = $http.get('/api/lists/count/' + ourUser._id);
                    shareCountCurrentPromise.success(function(result) {
                        $scope.shareCount = result;
                    });
                    shareCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //follower count
                    var followersCountCurrentPromise = $http.get('/api/followers/followers/count/' + ourUser._id);
                    followersCountCurrentPromise.success(function(result) {
                        $scope.followersCount = result;
                    });
                    followersCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });

                    //following count
                    var followingCountCurrentPromise = $http.get('/api/followers/following/count/' + ourUser._id);
                    followingCountCurrentPromise.success(function(result) {
                        $scope.followingCount = result;
                    });
                    followingCountCurrentPromise.error(function() {
                        couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[136]);
                    });
                    progressBar.status = 0;
                });


        };


        $scope.checkIfFaved = function() {
            var favListPromise = $http.post('/api/favs/timeline', favArrs.FavList);
            favListPromise.success(function(favListData) {
                var tempList = [];
                for (var i = 0; i < favListData.length; i++) {
                    tempList[favListData[i].post] = true;
                }
                favArrs.isFaved = tempList;

            });
            favListPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[142] + ' ' + langs.lang_strings[136]);
            });

        };

        $scope.checkIfShared = function() {
            var listListPromise = $http.post('/api/lists/timeline', listArrs.ListList);
            listListPromise.success(function(listListData) {
                var tempList = [];
                for (var i = 0; i < listListData.length; i++) {
                    tempList[listListData[i].post] = true;
                }
                listArrs.isShared = tempList;

            });
            listListPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[143] + ' ' + langs.lang_strings[136]);
            });

        };

        $scope.checkIfFollowed = function() {
            var followsPromise = $http.post('/api/followers/timeline/', followArrs.FollowList);
            followsPromise.success(function(listFollowData) {
                var tempList = [];
                for (var i = 0; i < listFollowData.length; i++) {
                    tempList[listFollowData[i].followed] = true;
                }
                followArrs.isFollowed = tempList;

            });
            followsPromise.error(function() {
                couponOps.pop('error', langs.lang_strings[135], langs.lang_strings[148] + ' ' + langs.lang_strings[136]);
            });

        };

    }
]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [

	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);

angular.module('users').factory('dateOps', ['Authentication', function (authentication) {

	return {
		convertDate: function (date) {

			var newMoment = moment.tz(date, 'Europe/Istanbul');
			if (authentication.user) {
				return moment(newMoment).locale(authentication.user.language).tz(authentication.user.timezone).format('DD MMM YYYY HH:mm');
			} else {
				return newMoment.locale(authentication.user.language).format('DD MMM YYYY HH:mm');
			}
		},
		convertDateRelative: function (date) {

			var newMoment = moment.tz(date, 'Europe/Istanbul');
			if (authentication.user) {
				return moment(newMoment).locale(authentication.user.language).tz(authentication.user.timezone).fromNow();
			} else {
				return newMoment.locale(authentication.user.language).fromNow();
			}
		}
	};
}]);


angular.module('posts').factory('couponOps', ['toaster', 'langs', function (toaster, langs){

	return {
		GlobalCoupon : {
			coupon: [],
			totalOdds: 1
		},
		GlobalQuotes : {
			quotes: []
		},
		ReplyingTo: {
			postIds: [],
			userIds: [],
			usernames: []
		},
		replyTo: function (postid, postids, username, userid, usernames, userids) {
			this.unReplyTo();
			var usernamesPrep = usernames.slice();
			usernamesPrep.push(username);
			var useridsPrep = userids.slice();
			useridsPrep.push(userid);

			this.ReplyingTo.usernames = usernamesPrep.filter(function(elem, pos) {
				return usernamesPrep.indexOf(elem) === pos;
			});
			this.ReplyingTo.userIds = useridsPrep.filter(function(elem, pos) {
				return useridsPrep.indexOf(elem) === pos;
			});
			if (typeof postids !== 'undefined') {
				this.ReplyingTo.postIds = postids.slice();
				this.ReplyingTo.postIds.push(postid);
			}
			this.pop('success', langs.lang_strings[178], langs.lang_strings[177]);
			document.getElementById('TextArea').focus();

		},
		unReplyTo: function () {
			this.ReplyingTo.userIds = [];
			this.ReplyingTo.usernames = [];
			this.ReplyingTo.postIds = [];
		},
		addFormField : function (gameID, dateTime, teamNames, betType, betTypeShort, odd) {

			for (var i = 0; i < this.GlobalCoupon.coupon.length; i++) {
				if (this.GlobalCoupon.coupon[i].gameID === gameID) {
					this.pop('warning', this.GlobalCoupon.coupon[i].teamNames, langs.lang_strings[175]);
					return;
				}
			}

			this.GlobalCoupon.coupon.push({
				'gameID': gameID,
				'dateTime': dateTime,
				'teamNames': teamNames,
				'betType': betType,
				'betTypeShort': betTypeShort,
				'odd': odd
			});

			this.GlobalCoupon.totalOdds = 1;
			for (var y = 0; y < this.GlobalCoupon.coupon.length; y++) {
				this.GlobalCoupon.totalOdds = this.GlobalCoupon.totalOdds * this.GlobalCoupon.coupon[y].odd;
			}
			this.pop('success', teamNames, langs.lang_strings[176]);
		}, // addFormField end

		removeFormField : function(sira ) {
			this.GlobalCoupon.totalOdds = this.GlobalCoupon.totalOdds / this.GlobalCoupon.coupon[sira].odd ;
			this.GlobalCoupon.coupon.splice(sira,1);
		},

		removeQuoteField : function(sira) {
			this.GlobalQuotes.quotes.splice(sira,1);
		},

		addQuoteField : function (gameID, countryLeague, dateTime, teamNames, odd1, odd2, odd3) {

			for (var z = 0; z < this.GlobalQuotes.quotes.length; z++) {
				if (this.GlobalQuotes.quotes[z].gameID === gameID) {
					this.pop('warning', this.GlobalQuotes.quotes[z].teamNames, langs.lang_strings[173]);
					return;
				}
			}

			this.GlobalQuotes.quotes.push({
				'gameID': gameID,
				'countryLeague': countryLeague,
				'dateTime': dateTime,
				'teamNames': teamNames,
				'odd1': odd1,
				'odd2': odd2,
				'odd3': odd3
			});
			this.pop('success', teamNames, langs.lang_strings[174]);
		},	// addQuoteField end
		pop : function(toasttype, toasttitle, toasttext){
			toaster.pop(toasttype, toasttitle, toasttext);
		}

	};

}]);



angular.module('users').factory('ticker', ['$http', 'tickerVars', 'Authentication', function($http, tickerVars, authentication  ) {
	return {
		check: function () {
			if (authentication.user) {
				var postsTime = moment.tz(tickerVars.Timeline.time || authentication.user.lastTick, authentication.user.timezone);
				var listsTime = moment.tz(tickerVars.Shared.time || authentication.user.lastTick, authentication.user.timezone);
				var notificationsTime = moment.tz(tickerVars.Notifications.time || authentication.user.lastTick, authentication.user.timezone);
				var tickTime = moment.tz(Date.now(), authentication.user.timezone);

				var postsPromise = $http.post('api/posts/new/', {lastseen: postsTime});
				postsPromise.success(function (data) {
					tickerVars.Timeline.latest = data;
				});

				var listsPromise = $http.post('api/lists/new/', {lastseen: listsTime});
				listsPromise.success(function (data) {
					tickerVars.Shared.latest = data;
				});

				//console.log(notificationsTime);
				var notificationsPromise = $http.post('api/notifications/new/', {lastseen: notificationsTime});
				notificationsPromise.success(function (data) {
					tickerVars.Notifications.latest = data;
				});

				var lastTickPromise = $http.post('api/users/tick/', {tick: tickTime});
				lastTickPromise.success(function (data) {
					console.log('All good.');
				});
			}
		}
	};
}]);

angular.module('users').factory('tickerVars', [function() {
	return {
		Timeline: {
			latest: '',
			time: ''
		},
		Shared: {
			latest: '',
			time: ''
		},
		Notifications: {
			latest: '',
			time: ''
		}
	};
}]);

angular.module('users').service('langs',  function () {
	return {
		lang_strings : {},
		current_lang : 'en'
	};
});

angular.module('users').service('loadLangs', ['$http', 'langs', function ($http, langs) {
	return {
		load : function (lang){
			var langsPromise = $http.post('/api/auth/langs', {'dil' : lang || langs.current_lang});
			langsPromise.success(function (langdata) {
				langs.current_lang = lang;
				langs.lang_strings =  langdata;
			});
		}
	};
}]);

'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('api/users/:username', {
			username: '@username'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('users').factory('currentUser', function() {
	return {
		user: {
			id: ''
		}
	};
});


angular.module('users').factory('ourUser', function() {
	return {
		ourUser: ''
	};
});
