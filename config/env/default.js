'use strict';


module.exports = {
	app: {
		title: 'betroll',
		description: 'Betting Connection! Betroll is a sports social network with a focus on betting. Upcoming and live games, scores, bettors and sports lovers from all around the world are inside. Sign up, pick your games, discuss with others, build your bet slips and see what others do. Most importantly, make new friends and celebrate games.',
		keywords: 'betting, sports, football, soccer, basketball, tennis, odds, social networking',
		googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'UA-61841309-1'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'BrollllobR',
	sessionCollection: 'sessions'
};
