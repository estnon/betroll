'use strict';

module.exports = {
	port: 443,
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/betroll',
	client: {
		lib: {
			css: [
				'http://maxcdn.bootstrapcdn.com/bootswatch/3.3.2/flatly/bootstrap.min.css',
				'public/lib/angular-aside/dist/css/angular-aside.min.css',
				'public/lib/angularjs-toaster/toaster.min.css'
			],
			js: [
				'public/lib/moment/min/moment.min.js',
				'public/lib/moment-timezone/builds/moment-timezone-with-data-2010-2020.min.js',
				'public/lib/moment/locale/tr.js',
				'public/lib/moment/locale/ru.js',
				'public/lib/moment/locale/de.js',
				'public/lib/angular/angular.min.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'public/lib/angular-file-upload/angular-file-upload.min.js',
				'public/lib/angular-sanitize/angular-sanitize.min.js',
				'public/lib/angular-aside/dist/js/angular-aside.min.js',
				'public/lib/angularjs-toaster/toaster.min.js'
			]
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: 'https://localhost:443/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: 'https://localhost:443/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: 'https://localhost:443/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: 'https://localhost:443/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: 'https://localhost:443/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'Betroll <info@betroll.net>',
		options: {
			host: 'smtp.office365.com',
			port: '587',
			auth: { user: 'info@betroll.net', pass: 'Br365808Br-' },
			secureConnection: 'false',
			tls: { ciphers: 'SSLv3' }
		}
	}
};
