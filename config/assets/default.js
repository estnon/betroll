'use strict';
/*
 'public/lib/bootstrap/dist/css/bootstrap.css',
 'public/lib/bootstrap/dist/css/bootstrap-theme.css'
 'http://maxcdn.bootstrapcdn.com/bootswatch/3.3.2/flatly/bootstrap.min.css'
 'https://maxcdn.bootstrapcdn.com/bootswatch/3.3.2/flatly/bootstrap.min.css',
*/
module.exports = {
	client: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootflat/bootstrap-flat.css',
				'public/lib/bootflat/bootstrap-flat-extras.css',
				'public/lib/angular-aside/dist/css/angular-aside.css',
				'public/lib/angularjs-toaster/toaster.css',
				'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'
			],
			js: [
				'public/lib/moment/min/moment.min.js',
				'public/lib/moment-timezone/builds/moment-timezone-with-data-2010-2020.min.js',
				'public/lib/moment/locale/tr.js',
				'public/lib/moment/locale/ru.js',
				'public/lib/moment/locale/de.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/angular-file-upload/angular-file-upload.js',
				'public/lib/angular-sanitize/angular-sanitize.js',
				'public/lib/angular-touch/angular-touch.min.js',
				'public/lib/angular-aside/dist/js/angular-aside.js',
				'public/lib/angularjs-toaster/toaster.js'
			],
			tests: ['public/lib/angular-mocks/angular-mocks.js']
		},
		css: [
			'modules/*/client/css/*.css'
		],
		less: [
			'modules/*/client/less/*.less'
		],
		sass: [
			'modules/*/client/scss/*.scss'
		],
		js: [
			'modules/core/client/app/config.js',
			'modules/core/client/app/init.js',
			'modules/*/client/*.js',
			'modules/*/client/**/*.js'
		],
		views: ['modules/*/client/views/**/*.html']
	},
	server: {
		allJS: ['gruntfile.js', 'server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
		models: 'modules/*/server/models/**/*.js',
		routes: ['modules/*[!core]/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
		config: 'modules/*/server/config/*.js',
		policies: 'modules/*/server/policies/*.js',
		views: 'modules/*/server/views/*.html'
	}
};
