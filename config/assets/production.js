'use strict';
//'http://maxcdn.bootstrapcdn.com/bootswatch/3.3.2/flatly/bootstrap.min.css',
module.exports = {
	client: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/lib/bootflat/bootstrap-flat.min.css',
				'public/lib/bootflat/bootstrap-flat-extras.min.css',
				'public/lib/angular-aside/dist/css/angular-aside.min.css',
				'public/lib/angularjs-toaster/toaster.min.css',
				'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'
			],
			js: [
				'public/lib/moment/min/moment.min.js',
				'public/lib/moment-timezone/builds/moment-timezone-with-data-2010-2020.min.js',
				'public/lib/moment/locale/tr.js',
				'public/lib/moment/locale/ru.js',
				'public/lib/moment/locale/de.js',
				'public/lib/angular/angular.min.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'public/lib/angular-file-upload/angular-file-upload.min.js',
				'public/lib/angular-sanitize/angular-sanitize.min.js',
				'public/lib/angular-touch/angular-touch.min.js',
				'public/lib/angular-aside/dist/js/angular-aside.min.js',
				'public/lib/angularjs-toaster/toaster.min.js'
			]
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	}
};
